---
title: Club Banners
---

<!-- This is the same line as in _includes/scripts but its hard to scope assign -->
{% assign images = site.static_files | where_exp:'image',"image.path contains 'assets/banners'" %}

The club website has a system of randomly selected banner images. There are currently
{{images.size}} of these banner images.

Please contact [the maintainers](mailto:blog@bendigouniathsclub.org.au) if you
have any photos that you might like to contribute, or any other comments,
suggestions or issues with any of these images.  Short wide images work best.
1280x300 pixels is a recommendation to aim at.  Try and choose nice descriptive
filenames.

{% for image in images %}
<p>
<img src="{{site.baseurl}}{{image.path}}"/>
<small>{{image.basename}}</small>
</p>
{% endfor %}
