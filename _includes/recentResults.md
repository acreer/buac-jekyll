

{% assign numLinks = include.show |default 3 %}

{% assign siteYear = site.time | date: '%Y' %}
{% assign recentResultsGroup = site.raceData| where: "buacYear",siteYear | where: "buacType","results" | reverse | group_by: "buacSlug"  %}

{% if recentResultsGroup.size > 0 %}
<nav class="nav-buttons">
<ul>
 <li><a href="{% link results.md %}">Results</a></li>
   {% for raceGroup in recentResultsGroup limit:numLinks %}
     {% assign race = raceGroup.items.last %}
    <li> <a href="{{site.baseurl}}{{race.url}}">{{ race.title }}</a>&nbsp;</li>
   {% endfor %}
   <li><a href="{% link aggregates.md %}">Aggregates</a></li>
</ul>
</nav>
{% endif %}

