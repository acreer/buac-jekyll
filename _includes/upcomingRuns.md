
{% assign numLinks = include.show |default 3 %}

{% assign nextRuns = site.runs| where_exp:"run","run.date >site.time" | where_exp:"run","run.isCurrent" %}

{% if nextRuns.size > 0 %}
<nav class="nav-buttons">
<ul>
 <li><a href="{% link runs.md %}#nextRun">Coming Up:</a></li>
   {% for run in nextRuns limit:numLinks %}
    <li><a href="{{site.baseurl}}{{run.url}}">{{run.title}}</a></li>
   {% endfor %}
</ul>
</nav>
{% endif %}

