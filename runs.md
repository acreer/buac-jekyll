---
title: Covid Virtual Series
sidebar:
  nav: admin
---

{% include upcomingRuns.md show="3" %}

<p/>


The 2019 [Covid Virtual Series]({% link covid-spring.md %}) races in no particular order are:

### Series 2

* [ College Classic, Kennington ]({% link _runs/college-classic.md %})
* [ Sandhurst Slog, Kangaroo Flat ]({% link _runs/sandhurst-slog.md %})
* [ Pearce's Road Rally, Mandurang ]({% link _runs/pearces-road-rally.md %})
* [ Kangaroo Flat Falter, Kangaroo Flat ]({% link _runs/kangaroo-flat-falter.md %})
* [ Hornet's Hideaway, Crusoe ]({% link _runs/hornets-hideaway.md %})
* BONUS ROUND: [ Pioneer Heartbreak ]({% link _runs/guys-hill-heartbreak.md %})

### Series 1
* [Notley’s Noodle (Virtual)]({% link _runs/notleys-noodle.md %})
* [Rifle Range Rattle (Virtual)]({% link _runs/rifle-range-rattle.md %})
* [Mandurang Meander (Virtual)]({% link _runs/mandurang-meander.md %})
* [Rocky Rises  (Virtual)]({% link _runs/rocky-rises.md %})
* [Crusoe Crusade (Virtual)]({% link _runs/crusoe-crusade.md %})


{% assign currentRunsByWeek = site.runs
  | where: "isCurrent",true
  | group_by_exp: "run","run.date | date: '%s' | plus: 86400 | date: '%Y-%U'"
%}

{% assign today = site.time | date: '%s' %}
{% assign foundNext = false %}

{% for wkGrp in currentRunsByWeek %}

 <div style="border:1px solid yellow;margin:5px;padding:0 5px;" >
   <h4 style="margin:5px 0"> Week of {{ wkGrp.items.first.date | date:'%a %b %-d %Y'  }} </h4>
    {% if wkGrp.items.size > 1 %}
    {% endif %}
  {% for run in wkGrp.items %}
    {% assign fromToday = run.date | date: '%s' | minus: today %}
  <div class="{% include raceTypeClass type=run.type %}"
    {% if fromToday > 0 and fromToday < 604800  and  foundNext != true %}
        id="nextRun"
       {% assign foundNext = true %}
    {% endif %}
    
   >
    {% if fromToday > 0 and fromToday < -604800 %}
        
    {% endif %}
       {{run.date | date: "%a %b %-d %Y %l:%M %P "}}
        <a href="{{site.baseurl}}{{run.url}}">{{run.title}}</a>
        <br/>
        Location: <a target="_new" href="{{run.location.href}}">{{run.location.text}}</a>
        {% if run.distances %}
        <br/>
        Distances/Program: {{run.distances}}
        {% endif %}
        <br/>
        Course Marker:
            {% if run.setter[0] %}
              {% for setter in run.setter %}
                {% include linkOrText.html link=setter %}
                {% case forloop.rindex  %}
                    {% when 2 %}
                        &amp;
                    {% when 1 %}
                    {% else %}
                        ,
                {% endcase %}
              {% endfor %}
            {% else %}
              {% include linkOrText.html link=run.setter %}
            {% endif %}
   </div>
    {% endfor %}
   </div>
{% endfor %}

<!--  BRING BACK AFTER COVID
<div>
<h4>Key</h4>
 <div class="{%  include raceTypeClass type="club-run" %}">
  <a href="#key">Club Run</a> Buac club handicap runs. 
 </div>
 <div class="{%  include raceTypeClass type="ab-run" %}">
  <a href="#key">Athletics Bendigo</a>
    Runs organised  by or for <a href="https://athleticsbendigo.org.au/">Athletics
    Bendigo</a> in or around bendigo.  Includes club invitations and long running Bendigo wide events.
 </div>
 <div class="{%  include raceTypeClass type="AV" %}">
   <a href="#key">Athletic Victorian</a> Runs with a state focus organised by
   <a href="http://athsvic.org.au/">Athletics Victoria</a>. The winter XCR program
    requires more travel, but also provides a great experiance.
  </div>
  <div class="{%  include raceTypeClass type="social" %}"><a href="#key">Other Club Event</a>
    Dinners, family days, meetings and more.
  </div>
  <div class="{% include raceTypeClass type="other" %}"><a href="#key">Other Runs</a>
    Events that various club members might participate in. 
  </div>

</div>

You can <a href="{{site.programpdf}}">download printable PDF program</a>

<a href="{% link resultsHistory.md %}">Previous Winter Cross Country Results</a>

The BUAC Club Winter Cross Country program is active over the cooler part of
the year. An Athletics Bendigo [summer track program]({% link
summerTrack.md %}) operates over the summer months.

-->

There are so many runs and so few weekends. Each year difficult decisions are
made to prefer one race over anothere. Here is a list of runs that are
currently resting

{%
 assign resting = site.runs |
   where: "type","Club Run" |
   where: "isCurrent",false
%}

<nav class="race-data">
<ul>
{% for run in resting %}
<li><a href="{{site.baseurl}}{{run.url}}">{{run.title}}</a></li>
{% endfor %}
</ul>
</nav>

<!-- MATE THIS IS COOL This comment helped something display properly -->
