---
layout: single
buacType: handicaps
---


{% assign siteYear = site.time | date: '%Y' %}

<h2>{{siteYear}} Winter Season Handicaps</h2>

{% assign byYear = site.raceData |  where: "buacYear",siteYear | where: "buacType",page.buacType | group_by: "buacYear" %}

{% if byYear.size == 0 %}

<p>No winter season handicaps for {{ siteYear }} yet</p>

{% endif %}

{% for yrGrp in byYear %}
  
  {% assign byRaceNum = yrGrp.items |  group_by: "buacRaceNum" |reverse %}

  <table>
    <thead>
      <tr>
        <td>Race</td>
        <td>Links</td>
      </tr>
    </thead>
    <tbody>
      {% for raceNumGrp in byRaceNum %}
      <tr>
        <td>
          {{ raceNumGrp.items.first.title  }} 
        </td>
        <td>
          {% assign sortedPosts = raceNumGrp.items | sort: "buacLength"  %}
          {% for post in sortedPosts %}
          <a href="{{site.baseurl}}{{ post.url }}">{{ post.buacLength | capitalize }}</a>
          {% endfor %}
        </td>
      </tr>
      {% endfor %}
    </tbody>
  </table>
  
{% endfor %}

{% assign typeYears = site.raceData |  where: "buacType",page.buacType | group_by: "buacYear" | reverse %}

<nav class="nav-across">
<ul>
 <li>History:</li>
  {% for yr in typeYears  %}
   {% if yr.name != siteYear %}
    <li><a href="{% link handicapsHistory.md %}#{{yr.name}}">{{yr.name}}</a></li>
   {% endif %}
  {% endfor %}
</ul>
</nav>


<!-- END -->
