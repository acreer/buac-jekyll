---
title: Winter Season Handicap History
layout: single
---

Online winter season handicap information over the years. There is 
[results history]({% link resultsHistory.md %}) available also.

{% assign byYear = site.raceData | where: "buacType","handicaps" | group_by: "buacYear" %}
{% assign byYearReverse = byYear | reverse %}


<nav class="nav-across">
 <ul>
  <li><small>Jump to year:</small></li>
  {% for yr in byYearReverse  %}
   <li><a href="#{{yr.name}}"><small>{{yr.name}}</small></a></li>
  {% endfor %}
 </ul>
</nav>

{% for yrGrp in byYear reversed %}
 <table id="{{yrGrp.name}}">
  <thead>
   <tr>
    <th style="width:50%">{{ yrGrp.items.first.date| date: '%Y' }} Race</th>
    <th>Handicaps</th>
   </tr>
  </thead>
  <tbody>
   {% assign byRaceNum = yrGrp.items |  group_by: "buacRaceNum" %}
   {% for raceNumGrp in byRaceNum %}
     <tr>
       <td>
        {{ raceNumGrp.items.first.title  }} 
       </td>
       <td>
        {% assign sortedPosts = raceNumGrp.items | sort: "buacLength"  %}
        {% for post in sortedPosts %}
          <a href="{{site.baseurl}}{{ post.url }}">{{ post.buacLength | capitalize }}</a>
        {% endfor %}
       </td>
      </tr>
   {% endfor %}
  </tbody>
 </table>
{% endfor %}

<nav class="nav-across">
 <ul>
  <li><small>Jump to year:</small></li>
  {% for yr in byYearReverse  %}
   <li><a href="#{{yr.name}}"><small>{{yr.name}}</small></a></li>
  {% endfor %}
 </ul>
</nav>

<!-- END -->

