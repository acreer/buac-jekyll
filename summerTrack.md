---
title: Summer Track Season
sidebar:
  nav: admin
---

Contact David Heislers and Ross Douglas for further information on the BUAC
track and field team and provide email link phone number.

Over the summer months Athletics Bendigo presents a range of Athletics.

2019/20 Summer track info as provided by Athletics Bendigo

* [Season at a glance]({%link  assets/files/2019/2019_20_Bendigo_Season-at-a-Glance_Ver1.2.pdf %})
* [Summer Handbook DRAFT]({%link  assets/files/2019/2019_20_Book_Prelim.pdf %})
* [Thursday Nights]({%link  assets/files/2019/AB-Thursday-evening-track-2019-20-colour--FINAL.pdf %})
* [Tuesday Nights]({%link  assets/files/2019/AB-Tuesday-track-program-2019b.pdf %})

[]({%link  assets/files/2019/AVSL_Logo.gif %})
[]({%link  assets/files/2019/Welcome_AVSL.gif %})
[]({%link  assets/files/2019/flack_advisory.gif %})

### AV Shield

The [AV Shield](https://www.athleticsbendigo.org.au/calendar) is a full program
of track and field athletics on Saturday Afternoons.

The Club is an [Athletics Victoria](http://athsvic.org.au/) affiliated club and has
a track and field team that competes at every track meet.

[Uniform]({%link uniform.md %}) for the AV Shield is usually
availble at the Track on Saturdays.

The Club has end of season awards for track and field athletes in the following
categories - throws, jumps, sprints and middle-long distance.  Based on a
points system.

### Tuesday Nights
The [The AL Parker Electrical Tuesday Track Run
Series](https://www.athleticsbendigo.org.au/tuesday-track-run-series)  is is
run during the track season from October to March. A run in a relaxed
atmosphere for all abilities at the La Trobe University Bendigo Athletics
Complex

- Jack Davey 5000m Bendigo Championship
- Hilson Builders Melbourne Cup Handicap
- Leigh Purtill 10,000m Bendigo Championship
- 4x1500m, 4x800m relays

### Thursday nights
The [Flack Advisory Distance
Series](https://www.athleticsbendigo.org.au/flack-advisory-distance-series) is
handicap event run over a number or races (series) between 800 metres and 1600
metres.  Small cash prizes and trophies for winners and placegetters.


### Photos
![Middle Distance Ladies]({%link assets/files/summerTrack/MiddleDistanceLadies.jpg %}){: .align-center}
![Middle Distance Men]({%link assets/files/summerTrack/MiddleDistanceMen.jpg %}){: .align-center}
![Javelin]({%link assets/files/summerTrack/Javelin.jpg %}){: .align-center}
![2017 10k Champs]({%link assets/files/summerTrack/2017_10k_Champions.jpg %}){: .align-center}
