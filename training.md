---
title: Group runs in Bendigo

sidebar:
  nav: admin
---

#  Monday
D.I.Y

# Tuesday

## Roosters Run

Roosters Run
6am at Nolan St Carpark, Lake Weeroona
Distances: 4-6km Loop
All levels catered for. 

## Winter
Trot Around The Lake [facebook](https://www.facebook.com/groups/260799314257961/)

Time : 5.50pm for a 6pm start.  
Place : Nolan Street Car Park, Lake Weeroona  
What : 3km & 1.5km free, timed efforts.  

## Summer

[Summer Track]({% link summerTrack.md %})

# Wednesday
Gavin Fielder’s Interval Sessions

Time : 6pm  
Place : Rowing Club Car Park (Railway side), Lake Weerona  
What : Interval Sessions  

# Thursday

## Roosters Run

Roosters Run
6am at Nolan St Carpark, Lake Weeroona
Distances: 4-6km Loop
All levels catered for. 

## Thursday Night Trail

Time : 6pm  
Place : Starts at the Carpark oppsite Federation Terrace on Edwards road.  
What : 1 hour at a gentle pace. All welcome. No one gets left behind.  



# Friday
D.I.Y.

# Saturday

Winter
: [Club Runs](http://www.bendigouniathsclub.org.au/ac/runs.html)

Summer
: [Summer Track]({% link summerTrack.md %})

# Sunday

## Bendigo Sunday Bunch Run

Time : Departs 7am SHARP  
Place : John Bomford Centre Carpark at Kennington Reservoir (Cnr Condon st & Crook St)  
What : 15km - 20km of easy jogging through hills and / or flat. All welcome.  
 Two Groups (but feel free to start a new one!)
  * approx. 4:30 min/k.
  * approx. 5:00 - 5:15 min/k.
