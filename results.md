---
layout: single
buacType: results
---

{% assign siteYear = site.time | date: '%Y' %}

<h2>{{siteYear}} Winter Season Results </h2>

{% assign byyear = site.raceData |  where: "buacYear",siteYear | where: "buacType",page.buacType | group_by: "buacYear" %}

{% if byyear.size == 0 %}

<p>No winter season results for {{ siteYear }} yet</p>
{% endif %}

{% for yrGrp in byyear %}

{% assign byRaceNum = yrGrp.items |  group_by: "date" | reverse %}

  <table>
    <thead>
      <tr>
        <td>Race</td>
        <td>Links</td>
      </tr>
    </thead>
    <tbody>
      {% for raceNumGrp in byRaceNum %}
      <tr>
        <td>
          {{ raceNumGrp.items.first.title  }} 
        </td>
        <td>
          {% assign sortedPosts = raceNumGrp.items | sort: "buacLength"  %}
          {% for post in sortedPosts %}
          <a href="/ac{{ post.url }}">{{ post.buacLength | capitalize }}</a>
          {% endfor %}
        </td>
      </tr>
      {% endfor %}
    </tbody>
  </table>

{% endfor %}

{% assign typeYears = site.raceData |  where: "buacType",page.buacType | group_by: "buacYear" | reverse %}

<nav class="nav-across">
<ul>
 <li> <a href="{% link aggregates.md %}">Aggregates</a> </li>
 <li>History:</li>
  {% for yr in typeYears  %}
   {% if yr.name != siteYear %}
    <li><a href="{% link resultsHistory.md %}#{{yr.name}}">{{yr.name}}</a></li>
   {% endif %}
  {% endfor %}
</ul>
</nav>

{% include upcomingRuns.md show="3" %}

<!-- END -->
