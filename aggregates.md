---
title: Club Championship and Speed Championship
layout: single
---

{% assign byYear = site.aggregates | group_by: 'buacYear' %}

{% for yrGroup in byYear reversed%}
{% assign clubLong = yrGroup.items |where: "buacType","club" | where: "buacLength","long" %}
{% assign clubMedium = yrGroup.items |where: "buacType","club" | where: "buacLength","medium" %}
{% assign clubShort = yrGroup.items |where: "buacType","club" | where: "buacLength","short" %}
{% assign speedLong = yrGroup.items |where: "buacType","speed" | where: "buacLength","long" %}
{% assign speedMedium = yrGroup.items |where: "buacType","speed" | where: "buacLength","medium" %}
{% assign speedShort = yrGroup.items |where: "buacType","speed" | where: "buacLength","short" %}

<table>
  <tbody>
      <tr>
      <td rowspan="2">{{yrGroup.name}}</td>
        <th>Club Champion</th>
        <td><a href="{{site.baseurl}}{{clubLong.first.url}}">Long</a></td>
        <td><a href="{{site.baseurl}}{{clubMedium.first.url}}">Medium</a></td>
        <td><a href="{{site.baseurl}}{{clubShort.first.url}}">Short</a></td>
      </tr>
      <tr>
        <th>Speed Champion</th>
        <td><a href="{{site.baseurl}}{{speedLong.first.url}}">Long</a></td>
        <td><a href="{{site.baseurl}}{{speedMedium.first.url}}">Medium</a></td>
        <td><a href="{{site.baseurl}}{{speedShort.first.url}}">Short</a></td>
      </tr>
  </tbody>
</table>
{% endfor %}
<!--- Can bring back original
<table>
 <tbody>
  <tr>
  <th> Club Champion</th>
   <td><a href="{% link _aggregates/2017/club-long.md%}">Long</a></td>
   <td><a href="{% link _aggregates/2017/club-medium.md%}">Medium</a></td>
   <td><a href="{% link _aggregates/2017/club-short.md%}">Short</a></td>
 </tr>
 <tr>
  <th>Speed Champion</th>
   <td><a href="{% link _aggregates/2017/speed-long.md %}">Long</a></td>
   <td><a href="{% link _aggregates/2017/speed-medium.md %}">Medium</a></td>
   <td><a href="{% link _aggregates/2017/speed-short.md %}">Short</a></td>
  </tr>
 </tbody>
</table>
-->

[Past Champions]({% link aggregatesHistory.md %})

### Explanation

Handicapped racing is designed to ensure that every runner has a chance to win.
This means that runners are allocated a handicap according to their ability.
The faster runners give a start to the slower runners – with the aim to have
everyone finishing close together. So whether you can run a 3 min km or a 7min
km you have a an equal chance of crossing the line first. Which also means that
just because you’re a slower runner you don’t come last.

Points are allocated for the handicap race (Club Champion) and the fastest runner (Speed Champion).
Have a look at how the [Scoring System]({% link aggregatesScoring.md %}) works.
