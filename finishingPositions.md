---
title: Finishing Positions
layout: single
---

### 2017 Club Run Finishing Positions

||Race Order|5|Crusoe Crusade|10|Pearces Road Rally|
|1|Mandurang Meander|6|Ham Street Hustle|11|Kangaroo Flat Falter|
|2|Landry Lope|7|Piepers Plunder|12|Trotting Terrace Trundle|
|3|Rifle Range Rattle|8|Reservoir Ramble|13|Notleys|
|4|Sandhurst Slog|9|Junortoun Jog|||


|Name|Surname|1|2|3|4|5|6|7|8|9|10|11|12|13|Races|


{% assign byYear = site.aggregates | group_by: "buacYear" %}

