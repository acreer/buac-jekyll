---
title: Past Club Championship and Speed Championship
layout: single
---

### Past Club Champions

| Year|Long|Medium|Short|
|---|---|---|
|2017|Darren Rowe|Max Rowe|April Wainright|
|2016|Ben McDermid|Beatrice Lonsdale|Geoffrey Robinson|
|2015|Troy Davis|Josephine Robinson|Billy Meade|
|2014|John Robinson|Tyler Wilkie|Lacey Wilkie|
|2013|Ingrid Douglas|Tullie Rowe|Luke Feuerherdt|
|2012|Gavin Fielder|Sebastian Rowe|Flyod Cartner|
|2011|Shane Rushan|Imogen Douglas|Braydon Roberts|
|2010|Tracy Wilson|Steffany Roberts|Steffany Roberts|
|2009|Mick McCarthy|Rielly Walsh|Imogen Douglass|

### Past Speed Champions Female

|Year|Long|Medium|Short|
|---|---|---|
|2017|Sarah Jalim|Tully Rowe|Ebony Woodward|
|2016|Sarah Jalim|Bridie Blake-Burrows|Abbey Cartner|
|2015|Rebecca Wilkinson|Els Viester|Ingrid Douglas|
|2014|Sarah Jalim|||
|2013|Rebecca Tweed|||
|2012|Rebecca Tweed|||
|2011|Rebecca Tweed|Steffany Roberts|Steffany Roberts|
|2010|Erica Wilkinson|Steffany Roberts|Steffany Roberts|
|2009|Ingrid Douglass|Leila Bieleny|Imogen Douglass|


### Past Speed Champions Male

|Year|Long|Medium|Short|
|---|---|---|
|2017|Michael Bieleny|Floyd Cartner|William Robinson|
|2016|Rik McCaig|Will McCaig|Lachlan Feuerherdt|
|2015|David Meade|Adam Fleming|Billy Meade|
|2014|Michael Bieleny|||
|2013|Richard Gleisner|||
|2012|Ben McDermid|||
|2011|Michael Bieleny|Scott Roberts|Braydon Roberts|
|2010|Andrew Buchanan|Bailey Evans|Bailey Evans|
|2009|Andrew Buchanan|Jake Russell|Dane Heiden|


