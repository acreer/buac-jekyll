---
title: Wandin Park Yarra Ranges- 8km Athletics Victoria XCR’17 – Sat 13th May 2017
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/WandinGroupPhoto13May17.jpg
author: Mel Douglass
---

Congratulations to  our BUAC athletes and local athletes who represented the Bendigo Region at Wandin Park for the Athletics Victoria XCR’17 – 8km event on Saturday 13th May 2017.

Special mention to Andy Buchanan who took out the 8km event in a time 26:05:68, winning by a comfortable 25 seconds, and Nigel Preston also made the podium being 3rd placed Country Victorian Athlete in the 8km event in 29:53:43.

Our Veterans were also in fine form with Craig Green finishing 8th in his age group (45-49) time of32:58.  Charles Chambers 3rd in his age group (60+) time of 37:57:92, and David Heislers in the 50+ age group with a time of 40:05.

Fantastic results.

For further details and reading, Craig Green has prepared a full report for the Bendigo Region, available on the link below.

There are some links to Athletics Victoria too.

 

[https://www.athleticsbendigo.org.au/single-post/2017/05/13/Bendigo-and-Buchanan-Blitz-at-Aths-Vic-X-Country](https://www.athleticsbendigo.org.au/single-post/2017/05/13/Bendigo-and-Buchanan-Blitz-at-Aths-Vic-X-Country)

[http://athsvic.org.au/generalnews/frontpage/xcr17-wandin-park-cross-country-wrap/](http://athsvic.org.au/generalnews/frontpage/xcr17-wandin-park-cross-country-wrap/)

[https://www.facebook.com/athsvic/videos/10154797217762979/](https://www.facebook.com/athsvic/videos/10154797217762979/)

#unipride

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/WandinGroupPhoto13May17.jpg?resize=300%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/WandinGroupPhoto13May17.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Wandin-Course13May2017.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Wandin-Course13May2017.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Andy-B-Wandin-Park-13May17.jpg?resize=300%2C200)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Andy-B-Wandin-Park-13May17.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/NigelPrestonWandin-13May2017.jpg?resize=300%2C200)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/NigelPrestonWandin-13May2017.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Andy-B2-Wandin-13May2017.jpg?resize=300%2C200)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Andy-B2-Wandin-13May2017.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Wandin-Podium-AndyB13May2017.jpg?resize=300%2C200)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Wandin-Podium-AndyB13May2017.jpg)

 



<!-- END -->
