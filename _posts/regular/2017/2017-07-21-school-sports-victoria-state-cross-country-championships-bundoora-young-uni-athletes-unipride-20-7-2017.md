---
title: 'School Sports Victoria State Cross Country Championships Bundoora – young
  Uni athletes #unipride 20/7/2017'
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/TullieSchoolsxc.jpg
author: Mel Douglass
---

A huge Congratulations to our young, up and coming UNI XC runners who participated at the School Sports Victoria State championships in Bundoora yesterday.  Well done everyone, great running.

For a State Championship the course it has measured well short of the 3km it was suppose to be, measured incorrectly not by a few metres but by a considerable distance.  According to our experts the course was actually 2500m, rather than 3km.

A big thank you to Justine Rowe for providing the results and the photos.   If anyone has been accidentally missed, please let me know and I can edit the results.

**Boys 15yo**  
Will McCaig 2nd 8.00 2500m  
Jake Meade 19th 8.42 2500m…  
**Girls 12/13 P**  
Charlotte Murphy 66th 10.44 2500m  
**Boys 12/13yo**  
Toby McCaig 76th 9.54 2500m  
**Girls 14yo**  
Beatrice Lonsdale 51st 10.52 2500m  
Jasmine Santly 61st 11.10 2500m  
Jazlin Fear 64th 11.15 2500m  
**Girls 9/10**  
Abby Cartner 72nd 8.52 2000m  
**Boys 11**  
Max Rowe 43rd 9.40 2500m  
**Girls 16yo**  
Tullie Rowe 3rd 8.55 2500m  
Bridie Blake-Burrows 17th 10.09 2500m

Further reading and official results can be found via the Athletics Bendigo Facebook page [https://www.facebook.com/AthleticsBendigo/ ](https://www.facebook.com/AthleticsBendigo/)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/SchoolsXC2n.jpg?resize=300%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/SchoolsXC2n.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/WilMcCo.jpg?resize=300%2C169)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/WilMcCo.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/MaxSchool-XCn.jpg?resize=300%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/MaxSchool-XCn.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/WilMco.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/WilMco.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/TullieSchoolsxc.jpg?resize=300%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/TullieSchoolsxc.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/schoolsxc1.jpg?resize=168%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/schoolsxc1.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Schools-XC.jpg?resize=168%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Schools-XC.jpg)

 

 



<!-- END -->
