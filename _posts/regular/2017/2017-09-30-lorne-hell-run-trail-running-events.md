---
title: Lorne Hell Run – Trail running events
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail2.jpg
author: Mel Douglass
---

Congratulations to our BUAC trail runners who ran at Lorne on the weekend.  Fantastic results.  Love all the podium finishes too.

[Danny Burgess](https://www.facebook.com/danny.burgess.733?fref=gs&dti=32542525756&hc_location=group) 1st place in the marathon 4:35ish, [Andrea Smith](https://www.facebook.com/profile.php?id=1242648842&fref=gs&dti=32542525756&hc_location=group) 2nd place in the half marathon 2:13:26, [Rhianna Wik-gamble](https://www.facebook.com/Rhianna16?fref=gs&dti=32542525756&hc_location=group) 2:11:31, [David Cripps](https://www.facebook.com/Crippy05?fref=gs&dti=32542525756&hc_location=group) cripps 2:04:01, [Leah K Cripps](https://www.facebook.com/leah.cripps?fref=gs&dti=32542525756&hc_location=group) 2:44:23 (1st place in the 63km relay)/

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/trail7.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/trail7.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail6.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail6.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail4.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail4.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail3.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail3.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail2.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/Trail2.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/trail1.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/10/trail1.jpg)

 



<!-- END -->
