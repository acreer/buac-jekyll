---
title: Congratulations to BUAC’s Russ Jenkins – 1st 50+ Veteran – Noosa Half Marathon
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/RussNoosa-2017-n.jpg
author: Mel Douglass
---

A big congratulations to our veteran uni speedster [Russ Jenkins](https://www.facebook.com/russ.jenkins.963?fref=mentions) (The Banker) on his super effort in this mornings Run Noosa Half Marathon. Russ was first in his age group and 16th overall in a time of 80mins 34seconds (average pace 3:49 p/km). This event was part of the Noosa Ultimate Sports Festival. Well done Russ.

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Russ-Noosa2017n.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Russ-Noosa2017n.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/RussNoosa-2017-n.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/RussNoosa-2017-n.jpg)



<!-- END -->
