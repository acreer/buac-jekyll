---
title: Victorian Country Championships – Australia Day Long Weekend 2017
author: Mel Douglass
---

Congratulations to our BUAC athletes at the Vic Country Championships.  Fantastic results.

Though we were small in numbers, Uni athletes had an outstanding 100% podium strike rate at the Vic Country Champs over the Australia Day long weekend. Results were at Casey Fields:  
– Anthony Messerle 2nd Open Javelin, 2nd Discus  
– Wil McCaig 2nd U16 3000m, 2nd 1500m  
– Craig Green 2nd 40+ 5000m, 3rd 1500m  
– David Heislers 2nd 50+ 3000m, 1st 1500m, 1st 800m

Though the competition was generally down on previous years our athletes ran/threw quality efforts – including at least 2 PBs. We did miss regulars such as Mike Belieny and Matthew Heislers (injured or regaining fitness from such), but hopefully next year we can pull in some more of our Saturday afternoon T&F regulars including from our growing band of promising juniors.

#unipride

 

All official results can be found on this link below:

[https://media.wix.com/ugd/2daf1f\_d03988dc5c8842e9ab9383f058955af1.pdf](https://media.wix.com/ugd/2daf1f_d03988dc5c8842e9ab9383f058955af1.pdf)



<!-- END -->
