---
title: Sandown Relays – Athletics Victoria 9/7/2017
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017n.jpg
author: Mel Douglass
---

Congratulations to our UNI AV runners and others representing the Bendigo Regional at the AV Sandown Relays.

The Division 5 Bendigo Region Team of Andy Buchanan (UNI), Nigel Preston (UNI), Ben Stolz (Harriers) and Kye Jenkyn (UNI) certainly put Bendigo on the map, being the number 1 winning mens team on the day and actually lapping the team that placed 3rd and finishing 4:42 ahead of the team in second place.  Andy Buchanan had the fastest men’s time of the day with 18:01 for the 6.2km relay leg and the rest of the team kept the cracking pace going.

Results 4 x 6.2km relay- 1st placed mens team – Andy Buchanan (UNI) 18.01, Nigel Preston (UNI) 19:52, Kye Jenkyn (UNI) 20:11, Ben Stolz (Harriers) 21:57.

The Division 7 Mens Team of Adam Parker (SB), Daniel Plowright (SB), Craig Green (UNI) and Rossi Evans (Harriers) kept the winnings coming placing first in their event and would have placed 3rd overall.

Results 4 x 6.2km Relay Adam Parker (SB) 21.32, Daniel Plowright (SB)22:46, Craig Green (UNI) 22:23, Rossi “Evo” Evans (Harriers) 23:02

The 40+ Men’s Team in a tough division performed credibly in 9th of the 12 team field. Greg Hilson (SB) battled illness but still ran not to let the team down 25:13. David Heislers (UNI) showing his best form for the season with a 25:31 run and radio star Leigh Browell (SB) 26:49.

The Under 16 Boys team of Wil McCaig (UNI), Ned Buckell (Harriers) and Matt Buckell (Harriers) scored a silver medal in their event.

Results 3 x 3.1km Relay – Wil McCaig (UNI) 9:58, Ned Buckell (Harriers) 10:29, Matt Buckell (Harriers) 10:37.

Congratulations to all.  #unipride

A full report from Craig Green is available for further reading on the Athletics Bendigo website and facebook page.

[https://www.athleticsbendigo.org.au/single-post/2017/07/08/Two-wins-and-two-seconds-as-Bendigo-shines-at-XCR-Sandown-Relays](https://www.athleticsbendigo.org.au/single-post/2017/07/08/Two-wins-and-two-seconds-as-Bendigo-shines-at-XCR-Sandown-Relays)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017n.jpg?resize=300%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017n.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017WilMc.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017WilMc.jpg)



<!-- END -->
