---
title: Athletics Bendigo – Richard Kitt Memorial 1500m Handicap – Bendigo International
  Madison 11/3/17
author: Mel Douglass
---

Congratulations to Uni’s [<u><span style="color: #0066cc">David Heislers</span></u>](https://www.facebook.com/david.heislers) and [<u><span style="color: #0066cc">Craig Green</span></u>](https://www.facebook.com/craig.green.5205) on making the podium in last nights Flack Advisory Richard Kitt Memorial Race 1500m Handicap at Tom Flood Sports Centre. David Heislers had a great race to second place in a time of 5:08, Craig Green hot on his heels down the finishing straight, showed superior form to finish 3rd in a cracking time 4mins 44 sec. Congratulations also and a well deserved win to Bendigo Harriers athlete, Isaac Everett. Isaac is coached …by Craig Green and has showed continous improvement in his first season of track running to take our the prestigious 1500m Athletics Bendigo event.

BUAC was well represented in this event with 8 Uni runners in total taking to the track, a big well done to all our uni runners out there. Craig Green David Heislers[<u><span style="color: #0066cc">Michael Bieleny</span></u>](https://www.facebook.com/michael.bieleny) Wil McCaig, [<u><span style="color: #0066cc">Melissa Douglas</span></u>](https://www.facebook.com/mel.pearsedouglas) [<u><span style="color: #0066cc">Ross Douglas</span></u>](https://www.facebook.com/ross.douglas.35) Bridie Blake Burrows, Charles Chambers.

A big thank you to the BUAC cheer squads and other cheers squads strategically placed around the outside of the velodrome. You certainly make running around out there in front of a big crowd a memorable event. [<u><span style="color: #0066cc"><span class="_58cl _5afz">#</span><span class="_58cm">unipride</span></span></u>](https://www.facebook.com/hashtag/unipride?source=feed_text&story_id=10155273791760757)

Also a big thank you to all the officials, sponsors, athletes and race commentators who do a fabulous job and make these type of events happen in Bendigo.

Stayed tuned for more information on the Victorian Athletic Leagues invitation backmarkers mile at Bendigo Madison tonight. Should be a great event to watch.

For further reading and results see the following links below.

[<u><span style="color: #0066cc">https://www.facebook.com/AthleticsBendigo/?fref=ts</span></u>](https://www.facebook.com/AthleticsBendigo/?fref=ts)

[<u><span style="color: #0066cc">http://www.bendigoadvertiser.com.au/…/everett-charges-to-k…/</span></u>](http://www.bendigoadvertiser.com.au/story/4523519/everett-charges-to-kitt-memorial-triumph/)

[<u><span style="color: #0066cc">https://www.athleticsbendigo.org.au/…/The-complete-form-gui…</span></u>](https://www.athleticsbendigo.org.au/single-post/2017/03/10/The-complete-form-guide-to-the-Memorial-Events)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/03/Richard-Kitt1500m-race-2017.jpg?resize=300%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/03/Richard-Kitt1500m-race-2017.jpg)



<!-- END -->
