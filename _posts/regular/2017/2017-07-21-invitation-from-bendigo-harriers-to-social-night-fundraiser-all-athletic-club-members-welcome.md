---
title: Invitation from Bendigo Harriers to Social Night Fundraiser – all athletic
  club members welcome
author: Mel Douglass
---

The Bendigo Harriers social night and fundraiser for Joseph Baldwin and Liam Richardson looks set to be a great night, with over 70 members (so far) attending from the 4 different amateur athletics clubs in Bendigo. It is not too late to come along, RSVP to Eric Baker of Bendigo Harriers. Details below and attached flyer:

Bendigo Harriers AC have extended an invite to UNI members for a social night out and fundraiser to celebrate the selection of Joe Baldwin and Liam Richardson who will represent Australia in upcoming World Games, details are below and file attached from Eric Baker:

Bendigo Harriers would like to invite all Athletes to a social event on Sunday 23rd July 2017, commencing at 5pm at the Wellington Hotel, White Hill.

Dinner meals will be available and able to be purchased from at the venue.

The night is get together for all athletes and families all clubs in the Bendigo Region, and also to celebrate the selection of Joe Baldwin and Liam Richardson who will represent Australia in upcoming World Games.

There will be guest speakers and raffles and auctions on the night with any funds raised going to assist Joe and Liam.

Please put this in your calendar and share with anybody you would to  invite. Click on the link below for the flyer from Eric Baker.

[Harrierssocial event](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Harrierssocial-event.pptx)



<!-- END -->
