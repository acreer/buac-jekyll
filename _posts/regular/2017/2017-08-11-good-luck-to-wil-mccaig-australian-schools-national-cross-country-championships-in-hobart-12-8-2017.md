---
title: Good luck to Wil McCaig – Australian Schools National Cross Country Championships
  in Hobart 12/8/2017
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8747-e1498352599525.jpg
author: Mel Douglass
---

Good luck to BUAC’s  [Wil McCaig](https://www.facebook.com/wil.mccaig?fref=mentions) who is running down in Hobart tomorrow at the Australian Schools National Cross Country Champs!

There will apparently be a live stream on the School Sport Australia page, his race kicks off at 1:50pm

[http://www.schoolsport.edu.au/home-2/sports-information/cross-country/](http://www.schoolsport.edu.au/home-2/sports-information/cross-country/)

Will post an update when results become available. [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017WilMc.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Sandown2017WilMc.jpg)



<!-- END -->
