---
title: 'New BUAC long sleeve training tops and crop tops #catfishdesigns'
author: Mel Douglass
---

So this stuff just landed… And it’s spot on…

Did you choose White (cool & understated) or Yellow (loud & proud)

The new long sleeve training tops $50 each & crop tops $45 each can be collected from our team for those who have pre-ordered.   We will be looking to order more if you missed out or need to change size.

Ross will also order more winter jumpers shortly too.

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-uniform-1.jpg?resize=300%2C292)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-uniform-1.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/UniTops2017.jpg?resize=180%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/UniTops2017.jpg)

 



<!-- END -->
