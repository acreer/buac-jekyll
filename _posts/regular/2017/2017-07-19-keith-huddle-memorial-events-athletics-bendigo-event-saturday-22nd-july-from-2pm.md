---
title: Keith Huddle Memorial events – Athletics Bendigo event Saturday 22nd July from
  2pm
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Keith-Huddle.jpg
author: Mel Douglass
---

This Saturday there is no Uni club run however, as advertised previously, Athletics Bendigo / Bendigo Harriers will host the Keith Huddle Memorial events this weekend.  This is a combined club event and entry fees are payable on the day.  MacDonald Cup points count in this event so giddy up!  Uni is narrowly leading the MacDonald Cup points from Bendigo Harriers.

The run is held at the Quarry Hill oval/reserve (Ken Wust Oval).  For new members the events consist of laps of the grassed park area and are a lot different to our usual bushland trails. Good day for spectating with nearly the whole course viewable from the pavilion and rego area. Lots of prizes, sashes, ribbons & race categories.

As this is an AB event there is no need to worry about leg tags or afternoon tea.

Entry fees are:

$8 for 6km

$5 for 500m, 1k & 3k

$20 for a family.

There is no online rego do it on the day.

Times and details are:

2pm – U10 500 metres

2.05pm – 1K with U12 & Open

2.15pm  – 3k with U15, U18 & Open

2.40pm – 6k with U20, Open, 40+, 50+ & 60+

Please wear your BUAC singlet meant to be in it to get an award in the 3k & 6k events.

The 3km & 6km events are definitely for inter-club McDonald Cup points so please get along it’s easy to get to and all over pretty quickly with no packing up.

Read about it below.  [https://www.athleticsbendigo.org.au/keith-huddle-memorial](https://l.facebook.com/l.php?u=https%3A%2F%2Fwww.athleticsbendigo.org.au%2Fkeith-huddle-memorial&h=ATNONOF3QjXztNa53MLcR8ue-HKAdm35eyVfyOvG867JQlMEStquz7_6FtN8AxsigXIeVDYhzLfZ87lkPtyfetWO4kTi-dLACAbrpd978fGbR1EqxE_03S8XYr_l-cliBDIg7eSeVmKGuaPhSeubjQ-4yBWfnx5aAX5ovWJ32xtNMSJjZEh9rmvhoQaB8F1yS6jRD0OXq-n61-inudjNVTvg4YU8pYVtgzKI5VE5Uxmok0Mgkpgk2ibe8peuhy9xD9pVv1fQ9zi759KhNNbgpRkhcqxGyefv3382y8uNkQ)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Keith-Huddle.jpg?resize=232%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Keith-Huddle.jpg)



<!-- END -->
