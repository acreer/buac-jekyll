---
title: Athletics Victoria Bundoora XC Wrap up
author: Andrew Creer
---

Sounds like a great day. Here are all the Bendigo Regions Runners

Andy is State Champ (He is in OUR club…) Go US (Wink)

### Open and Masters Male

- 1 Andrew BUCHANAN 31:01
- 57 Nigel PRESTON  35:02
- 120 Ben STOLZ 37:55
- 141 Daniel PLOWRIGHT 38:51
- 147 Craig GREEN 39:01.687 (7th 45-49)
- 226 Rossi EVANS  41:38.973 (16 50-54)
- 302 David HEISLERS  (33 50-54)  45:33
- 343 Leigh BROWELL (45 40-44) 47:52

### U20 Results

- 8 Kye JENKYN  00:28:18.377

### Female Open

- 66 Alice WILKINSON   44:35


<!-- END -->
