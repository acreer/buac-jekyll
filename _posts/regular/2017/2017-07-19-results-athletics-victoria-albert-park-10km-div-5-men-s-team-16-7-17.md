---
title: Results – Athletics Victoria, Albert Park 10km – Div 5 Men’s team 16/7/17
author: Mel Douglass
---

Congratulations to the Division 5 Men’s team for the Bendigo Region,  finishing 2nd at Albert Park 10km  – Nigel Preston (Uni), Rossi Evans (Harriers), Ben Stolz (Harriers) and Daniel Plowright (SB).

The Division 5 men also took to the 10km Albert Park circuit,  unfortunately they encountered a very strong St Stephens team to finish second for the time in the individual events.

Nigel Preston (Bendigo University) ran an excellent race to run the 10km in a time of 33:29.74 to finish 8th Country Victorian.  Nigel had also raced the day before at the Uni Club run at Pearces Road on a course with some very steep hills.

Ross Evans (Bendigo Harriers) is quickly returning to his best and ran 37:21.00 to be 8th in the 50-54 age group. He was just 7 seconds ahead of another Bendigonian in Peter Cowell running for Vic Masters.

Ben Stolz (Bendigo Harriers) was narrowly ahead of the above runners running 37:17.25. Daniel Plowright (South Bendigo) finishing in a time of 37:56.54.

Full report of all the Bendigo Region runners has been prepared by Craig Green on the link below.

[https://www.athleticsbendigo.org.au/single-post/2017/07/16/Stunning-performance-for-Hayes-the-highlight-at-Albert-Park](https://www.athleticsbendigo.org.au/single-post/2017/07/16/Stunning-performance-for-Hayes-the-highlight-at-Albert-Park)



<!-- END -->
