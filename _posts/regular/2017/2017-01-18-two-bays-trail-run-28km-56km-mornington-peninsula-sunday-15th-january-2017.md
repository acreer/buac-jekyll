---
title: Two Bays Trail Run 28km & 56km – Mornington Peninsula – Sunday 15th January
  2017
author: Mel Douglass
---

Congratulations to all our BUAC runners at the Two Bays Trail Run in the 28km and 56km events.  Situated on the Mornington Peninsula, the Two Bays Trail goes from Dromana to Port Phillip Bay, to Bushrangers Bay and on to Cape Schanck. Lots of brilliant results.  #unipride

BUAC results  
56km Sarah Jalim.4.58.07.3rd female.   John Robinson.5.09.12.  Craig Feuerherdt.5.28.50.  Maree Platt.7.04.23.

28km – Trinity Sanderson.3.03.32.  Beth Lavery.3.28.04.  Aida Escall.3.53.09.  Riss Leung.3.27.23.  Laurie Leung.3.27.24.  Sue Taylor.3.39.06   Andrea Smith.3.01.21.  Phillip Shambrook.2.57.15  .Louise Shambrook.3.07.16.  Rhianna Wik-Gamble.3.17.23.Leah Cripps.3.37.23.Sue Condon.3.48.29.

Ross McPhee <!-- react-text: 190 -->4.40.19 1st 40-49 male  
28k Brian Watson [<!-- react-text: 195 -->](https://www.facebook.com/brian.watson.503092?hc_location=ufi)2.20.24 Andrew Creer 2.21.42 Jim Russell <!-- react-text: 198 -->2.49.37

Sounds like a beautiful course, with some good elevation.  Will be adding this one to the bucket list at some stage.

If there are any more results that have been missed, or photos you would like added feel free to send them to me.

More information and results can be found at the following link [http://www.twobaystrailrun.com/index.html](http://www.twobaystrailrun.com/index.html)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/Two-Bays17.jpg?resize=165%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/Two-Bays17.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/Two-bays20017.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/Two-bays20017.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/two-bays.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/two-bays.jpg) [![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/two-bays2.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/two-bays2.jpg)



<!-- END -->
