---
title: Winter Race Day Numbers 2017
author: Darren Rowe
---

A great summary of the numbers at races where the club timing system counted club runners.

Data is a fine thing!

|Race                         |L  |M  |S  |    TOTAL|
|-----------------------------|---|---|---|---------|
|01_Mandurang Meander         |43 |29 |31 |    103  |
|02_Landry Lope               |39 |37 |24 |    100  |
|03_Rifle Range Rattle        |34 |24 |22 |    80   |
|04_Sandhurst Slog            |38 |39 |29 |    106  |
|05_Crusoe Crusade            |40 |28 |29 |    97   |
|06_Ham St Hustle             |31 |29 |22 |    82   |
|07_Piepers Plunder           |32 |25 |25 |    82   |
|BUAC Invitation              |54 |57 |47 |    158  |
|08_Reservoir Ramble          |32 |28 |19 |    79   |
|09_Junortoun Jog             |32 |27 |28 |    87   |
|99_Combined_Run              |14 |12 |11 |    37   |
|10_Pearces Rd Rally          |29 |29 |33 |    91   |
|11_Kangaroo Flat Falter      |22 |20 |24 |    66   |
|12_Trotting Terrace Trundle  |30 |18 |27 |    75   |
|13_Notleys                   |22 |19 |21 |    62   |
|BUAC Half Marathon           |   |   |   |    103  |
|BUAC Mystery Run             |29 |29 |28 |    86   |

    
    (13 Core Points scoring races)
    Total unique members who ran at least one race: 195
    Male: 105 Female: 90
    
    Total members who ran LONG race: 80
    Male: 52 Female: 28
    Total members who ran MEDIUM race: 97
    Male: 51 Female: 46
    Total members who ran SHORT race: 71
    Male: 33 Female: 38



