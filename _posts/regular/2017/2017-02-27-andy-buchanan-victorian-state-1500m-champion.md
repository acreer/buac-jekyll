---
title: Andy Buchanan – Victorian State 1500m Champion
author: Andrew Creer
---

Congratulations to Andy Buchanan.

You really ought to watch this race. The finish is amazing.



<iframe src="https://www.youtube.com/embed/RA8P5n3BKmA?rel=0" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Consider the amount of hard work and dedication to be able to do that.Hats off. And only 5 seconds faster than the Rio Mens Olympic Final….



<!-- END -->
