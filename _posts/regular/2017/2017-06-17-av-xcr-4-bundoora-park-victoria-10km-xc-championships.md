---
title: AV XCR 4 – Bundoora Park – Victoria 10km XC Championships
author: Mel Douglass
---

Good luck to our UNI Athletics Victoria runners competing at Bundoora Park today. [Andy Buchanan](https://www.facebook.com/andy.buchanan.5205?fref=mentions), [Kye Jenkyn](https://www.facebook.com/kye.jenkyn.1?fref=mentions),  [Nigel Preston](https://www.facebook.com/nigepresto?fref=mentions),  [Craig Green](https://www.facebook.com/craig.green.5205?fref=mentions),  [David Heislers](https://www.facebook.com/david.heislers?fref=mentions) and  [Matthew Heislers](https://www.facebook.com/matthew.heislers?fref=mentions).

Today’s event is 10km and serves as a national selection race for the Australian Cross Country Championships. Under 20’s race over 8km. It is also the Open Age Victorian Championships too which Andy B will be hopefully come home with back to back wins for the State Crown!

Good luck also to all the UNI students racing at Bundoora for the Vic All Schools State Finals too. Racing for the school kids is over 3km, 4km 6km depending on their age group. [#unipride](https://www.facebook.com/hashtag/unipride?source=feed_text&story_id=10155596019250757)



<!-- END -->
