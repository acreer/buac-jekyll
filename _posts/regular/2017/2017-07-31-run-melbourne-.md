---
title: Run Melbourne.
author: Gavin Fiedler
---

BUAC runners hit the streets of Melbourne for the annual Run Melbourne event.Russ Jenkins continued his outstanding form to win his age group in the 10K.

Results.

Half Marathon.  
Stephen Van Rees.1.19.51.  
Kathleen Pleasants.2.22.17.

10K.  
Russ Jenkins.36.53.  
Lou Bray.58.45.

Well done BUACers.



<!-- END -->
