---
title: National Cross Country Team Trials – Article from 31 January 2017
author: Mel Douglass
---

Well done to BUAC’s Andy Buchanan on his efforts at the National Cross-Country team trials in Canberra. Further reading  to story in the Bendigo Advertiser is in the link below.

 

[http://www.bendigoadvertiser.com.au/story/4437439/andys-gallant-run-at-trials/?cs=81](http://www.bendigoadvertiser.com.au/story/4437439/andys-gallant-run-at-trials/?cs=81)



<!-- END -->
