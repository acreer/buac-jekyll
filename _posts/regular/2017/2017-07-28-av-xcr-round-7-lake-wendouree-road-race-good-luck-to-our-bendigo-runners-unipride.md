---
title: 'AV XCR – Round 7 Lake Wendouree Road Race – good luck to our Bendigo Runners
  #unipride'
author: Mel Douglass
---

Good luck to our UNI AV runners and Bendigo Regional runners competing at Lake Wendouree tomorrow .  A very strong  UNI contingent representing the Bendigo Region – Andy Buchanan, Mike Bieleny, Nigel Preston, Craig Green, Leon Russell-Keely, Kye Jenkyn, Darren Hartland, Will McCaig and Jack Hickman.   Have a great day, go get em!

Open age run 15km road race and Juniors run 6km road race.  The traditional stomping ground of XCR legend Steve Moneghetti, Lake Wendouree once again provides the backdrop for this fast-paced event.

Will post an update when results and photos are available.

The rest of us will be at Kangaroo Flat Falter for another club run.  Good luck to all.

#unipride

#gogetem

 



<!-- END -->
