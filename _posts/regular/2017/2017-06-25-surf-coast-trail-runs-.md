---
title: Surf Coast Trail Runs.
author: Gavin Fiedler
---

BUACs bitumen is boring brigade headed to the SURF COAST for the SURF COAST TRAIL RUNS on Saturday.  
The weather was great this year but just to add a little more difficulty there was a High Tide that gave runners a good soaking and plenty of soft sand.  
Results.  
43K.  
Phil Shambrook.4.30.05.2nd in category.Danny Burgess.4.51.43.Andrea Smith.4.43.23.Jeremy Draper.5.35.44.Rhianna Wik-Gamble.5.14.52.  
21K.  
Aida Escall.3.19.06.Lou Bray.2.51.19.Sue Taylor.2.47.34.Riss Leung.2.45.32.Trinity Sanderson.2.28.17. Well done all.



<!-- END -->
