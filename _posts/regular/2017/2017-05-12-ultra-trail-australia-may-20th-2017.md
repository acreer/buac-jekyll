---
title: Ultra-Trail Australia – May 20th, 2017
author: John Robinson
---

Ultra-Trail Australia is an annual event run each year in the Blue Mountains out of Katoomba, NSW. Previously it went by the name of Northface 100. It attracts numerous runners from around Australia. International runners feature prominently, with UTA being part of the Ultra-Trail World Tour. It is fast becoming, if not, one of the premium ultra distance trail runs in the southern hemisphere. It boasts several events, the premier being the 100km endurance run with elevation gain of around 4,400 metres. There is also a 50km and 22km event.

This year Simon Austin and Ross McPhee are participating in the 100km, their first and third attempts respectively. Ross is hoping to better his 33rd overall finish in 2016, 7th in his age group. Brian Watson and Craig Feuerherdt have also entered in the 50km beast that has 2,400m gain. The guys have been putting in a huge number of hours of training over several months, many of their longer runs held down at Mt Alexander and Mt Macedon where elevation is a little easier to achieve.

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/UTA2017_prep_small.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/UTA2017_prep_small.jpg)

You can follow their progress on the day with live tracking at: [http://www.uta.livetrail.net/](http://www.uta.livetrail.net/ "http://www.uta.livetrail.net/")

We wish the four runners all the best and hope the weather is favourable!



<!-- END -->
