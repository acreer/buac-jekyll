---
title: Good luck to our AV BUAC runners at Cruden Farm – AV XCR 17 – Round 3
author: Mel Douglass
---

Good luck to our BUAC cross country runners and other Bendigo Region runners competing today at Cruden Farm – Athletics Victoria XCR Round 17.

Leon Keely, [Craig Green](https://www.facebook.com/craig.green.5205?fref=mentions), [David Heislers](https://www.facebook.com/david.heislers?fref=mentions) and Charles Chambers will be part of a Division 5 Team in the 16km Event.

Kye Jenkyn will race in the 6km Under 20 event.

The Men’s team comes with a very inform Leon Keely. Leon recently ran in China in a stairwell race up a skyscraper and finishing second in this big Asian event. He also was amongst the fastest at Jells Park.

Go Team Bendigo and [#unipride](https://www.facebook.com/hashtag/unipride?source=feed_text&story_id=10155520757430757)

We await the results update.

Further details on the Athletics Bendigo Facebook Page.

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Cruden-Farm-AV27May2017.jpg?resize=300%2C169)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Cruden-Farm-AV27May2017.jpg)



<!-- END -->
