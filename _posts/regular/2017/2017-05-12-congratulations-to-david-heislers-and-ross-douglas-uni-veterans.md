---
title: Congratulations to David Heislers and Ross Douglas – Uni Veterans
author: Mel Douglass
---

Congratulations to BUAC’s [David Heislers](https://www.facebook.com/david.heislers). Officially the Athletics Bendigo 50+ mens long jump champion over the summer track and field season. Well done David.

Congratulations also to President Ross Douglas, Officially the Athletics Bendigo 50+ 200m and 100m sprint champion over the summer track season.  Well done Ross.  I have never seen you move so fast in that 200m event! (-;

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/David-Hn.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/David-Hn.jpg)[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/received_10211887497047543.jpeg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/received_10211887497047543.jpeg)[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Ross.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Ross.jpg)[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/DAvid-HH.jpg?resize=180%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/DAvid-HH.jpg)

 



<!-- END -->
