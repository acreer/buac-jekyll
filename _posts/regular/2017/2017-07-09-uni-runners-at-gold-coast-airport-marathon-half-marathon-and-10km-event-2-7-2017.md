---
title: Uni runners at Gold Coast Airport Marathon, Half Marathon and 10km event 2/7/2017
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Andy-B-GC2017.jpg
author: Mel Douglass
---

Congratulations to all our UNI runners who went to the warm and sunny Gold Coast last weekend for the Gold Coast Airport Marathon and associated events.  Heading up there at this time of year certainly breaks a Victorian winter nicely.  Daily temperatures for the weekend varied between 20-24 degrees.

UNI had over half a dozen members running with the chief highlights from a speed perspective being Andy Buchanan 13th among the elite, and in the half and Russ Jenkins knocking out a sub 3 hour marathon winning his age group (55-59 male) and the National Masters Marathon Championship.

There is a lot of interest in what Andy does and in particular his early speed in the race which was on track with Australian record pace hence have got him to make some comments for the newsletter:-

_My plan was always to go with the leaders, I had my km splits turned off on my Garmin so wasn’t aware that the pace was so fast. Going through 10k in 29:42, I was still feeling fresh and unaware the impact a few 2:53-2:55km splits would have later on in the race. The eventual winner, a Japanese runner, and Liam Adams broke away from me just before the 10km mark. It wasn’t until the 14km mark that I started to pay for the few quicker km’s early on. The remaining 7km was a long way, made longer with a toilet stop at 18km. All runners paid the price of the quick early pace and ran a slower second half. While I didn’t run the time I wanted, too often I get comfortable sitting back in the pack so I am happy with my first attempt at a half and know there is a lot of room for improvement. I will look to do another but probably won’t be until next year. The goal for Gold Coast Half was just to experience racing 21km, as it is quite a jump from 10km. Looking back at how hard 21km was to race it certainly hasn’t made me want to race the full distance just yet and gives me even more respect for those who do the ‘real distance’. I’m pretty lucky to have parents who make it look easy, with Dad running another sub 3:30 (3:29:42) and Mum coming through in 3:43:35._

Congratulations Andy, super effort considering you had a stop at 18km mark.

Well done to all. #unipride

**BUAC Gold Coast Results**  
**10km event** Mel Douglas 49.16, Julie Marr 53.40.

**Half Marathon 21.1km** Andy Buchanan.1.06.11, Peter King 1.25.03. David Howes 1.58.18, Andrew Evely 1.46.33, Julie Evely 2.15.24.  
Cath Pethybridge 2.16.59, Sam Walker 1.43.49 Stacey Walker 2.15.26.

**Marathon 42.195km** Russ Jenkins.2.55.05.1st in his age group, Alan Buchanan 3.29.42, Jenny Buchanan 3.43.35, Maddy Evely 4.06.16, Jeff Pethybridge 3.22.21.

Further reading about the Gold Coast Marathon can be found on the Athletics Bendigo website, prepared by Craig Green. https://www.athleticsbendigo.org.au/single-post/2017/07/09/Quality-Queensland-results-for-local-athletes

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Andy-B-GC2017.jpg?resize=300%2C293)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Andy-B-GC2017.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/GCMarathon.jpg?resize=300%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/GCMarathon.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/GC-Marathono.jpg?resize=300%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/GC-Marathono.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/AndyGC2017o.jpg?resize=300%2C200)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/AndyGC2017o.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/CGMarathon_n.jpg?resize=300%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/CGMarathon_n.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/GC2017.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/GC2017.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/MelGC.jpg?resize=200%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/MelGC.jpg)



<!-- END -->
