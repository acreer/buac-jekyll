---
title: Mid Year Social Night Out / Inaugural Track Season Awards Saturday 24th June,
  Foundry Arms Hotel
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/BUAC_Mid_Year_Night_out_2017.jpg
author: Mel Douglass
---

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/BUAC_Mid_Year_Night_out_2017.jpg?resize=212%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/BUAC_Mid_Year_Night_out_2017.jpg)

Please come along and join us!

More readable PDF version  click here [BUAC Mid Year Night out 2017](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/BUAC-Mid-Year-Night-out-2017.pdf)



<!-- END -->
