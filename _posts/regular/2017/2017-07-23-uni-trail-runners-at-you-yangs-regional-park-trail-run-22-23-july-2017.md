---
title: Uni Trail Runners at You Yangs Regional Park Trail Run – 22-23 July 2017
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/AndreaYLT.jpg
author: Mel Douglass
---

Uni Trail Runners at You Yangs Trail Run events .

Race 3  – Trails Plus Mountain Series   22-23 July 2017

Congratulations to all our Uni Trail Runners who ran at the You Yangs Regional Park this weekend.  The You Yangs course is described on the website “as a beautiful run through varying terrain, from flat wooded bike trails to open rocky mountain trails. There are some great views from Flinders Peak across Port Philip Bay”.

Super effort everyone.  Phil, Louise and Andrea also ran at the Athletics Bendigo Keith Huddle Memorial event the day before.

Results listed below:

21.1Km  
David Cripps 2.01.11, Rhianna Deane 2.24.43.

30Km  
Phil Shambrook 3.04.02 Andrea Smith 3.20.04 Louise Shambrook 3.24.13 Rhianna Wik-Gamble 3.34.17.Leah Cripps 3.50.12

42.2Km  
Danny Burgess.4.44.26.

Great running everyone, well done.

Further reading and results can be found at [http://www.trailsplus.com.au/you-yangs/](http://www.trailsplus.com.au/you-yangs/)

#unipride

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/DavidC.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/DavidC.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/YLTR17.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/YLTR17.jpg)[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/UniYYTrail.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/UniYYTrail.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/AndreaYLT.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/AndreaYLT.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/LeahC.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/LeahC.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/PhilSYL.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/PhilSYL.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/YYMedaln.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/YYMedaln.jpg) [![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/YYTrail.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/YYTrail.jpg)

 



<!-- END -->
