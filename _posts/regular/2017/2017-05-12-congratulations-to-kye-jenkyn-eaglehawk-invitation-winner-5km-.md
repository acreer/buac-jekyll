---
title: Congratulations to Kye Jenkyn – Eaglehawk Invitation Winner 5km.
author: Mel Douglass
---

Winners are grinners! Congratulations to one of our newest BUAC members [Kye Jenkyn](https://www.facebook.com/kye.jenkyn.1) on taking out the Eaglehawk Athletics Club 5000m Invitation Cross Country today in the Borough.  Kye ran a cracking time of 16 mins 36 seconds on what I would call a true cross country course.  Super effort Kye. I am not sure of all the official results but a big well done to all our BUAC runners, and all competitors. There was certainly lots of [#unipride](https://www.facebook.com/hashtag/unipride?source=feed_text&story_id=10155453092410757) on show with many PB’s and placings in the age groups. More to come on that with Gav’s results round-up for the media. Also a big thank you to the [Eaglehawk Athletics-Club](https://www.facebook.com/Eaglehawk.YMCA.AC) for putting on another fine event this year.

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/KyeJ.jpg?resize=180%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/KyeJ.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Kye-Jn.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Kye-Jn.jpg)



<!-- END -->
