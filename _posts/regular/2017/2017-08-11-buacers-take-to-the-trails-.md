---
title: BUACers Take To The Trails.
author: Gavin Fiedler
---

Sunday August 6th saw BUAC members hitting the Trails at Dunkeld in the Southern Grampians and Silvan in the Dandenong Ranges.  
The Lonsdale and Rowe Families tackled The Peaks and Trails runs at Dunkeld.  
David,Beatrice,Phoebe and Lewis Lonsdale raced the event for the second time.This year David upped the ante to the 26k run.  
Darren,Tullie and Max Rowe got their first taste of Trail Running to add to the ever growing list of Trail Runners at BUAC.  
The Storm that went through overnight made conditions boggy in places and slippery.  
Mean while two of BUACs experienced Trail Runners Lisa and Rebecca Wilkinson took on the Silvan Trail run.The conditions were cold, wet and slippery.  
Trail running is booming at the moment.If you are interested, their are plenty of people around the club to point you in the right direction.Just ask around.  
Results.Peaks and Trails.  
26K.  
David Lonsdale.2.57.00.  
11K.  
Darren Rowe.1.09.10.  
6.4K.  
Tullie Rowe.27.44.1st Female.  
Max Rowe.27.52.1st in Age Group.  
Beatrice Lonsdale.33.45. 2nd in A/G.  
Phoebe Lonsdale.34.42. 2nd in A/G.  
Lewis Lonsdale.36.20.  
Silvan.  
21K.  
Rebecca Wilkinson.2.11.17.  
Lisa Wilkinson.2.23.27.  
Well done BUACers.



<!-- END -->
