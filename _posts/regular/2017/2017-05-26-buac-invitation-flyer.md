---
title: BUAC Invitation Flyer
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-Invite-2-Image.jpg
author: Andrew Creer
---

Print out this [flyer](http://bendigouniathsclub.org.au/ac/assets/files/2017UniInviteFlyer.pdf) and pin it up at work.



<!-- END -->
