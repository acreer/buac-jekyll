---
title: 3km – St Arnaud School Sports Victoria (SSV) XC 6/6/17
author: Mel Douglass
---

Congratulations to the talented UNI runners who represented their school at St Arnaud SSV XC Loddon Mallee Regional Finals 2017.   Fantastic results all round.  3km XC

- Wil McCaig has continued his winning form with a 1st place in the 15 year old boys 3km event 10:27
- Jack Meade 8th 11.08
- Tullie Rowe 2nd in under 16 girls 11:58
- Beatrice Lonsdale 2nd U14 (no time available)
- Max Rowe 2nd 11 year old boys 12.06
- Charlotte Murphy 3rd 12-13 year old girls 13:43
- Bridie Blake- Burrows 3rd under 16 girls 12:25
- Jazlin Fear 8th (no time available)
- Jasmine Santley 4th (no time available)
- Toby McCaig 12-13 year old boys 12.22 (team place qualified for State titles at Bundoora)
- Emerson Sim 11 year old girls 13:50

Well done to all.



<!-- END -->
