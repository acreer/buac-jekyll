---
title: BUAC XC Invitation 7.5km Event wrap up 3/6/17
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4598.jpg
author: Mel Douglass
---

Runners were greeted with perfect conditions for the Annual UNI Invitation events.  A great day was had by most and our committee thanks everyone that has participated and turned up to support our event.  A big thank you to all our volunteers and helpers who made the day run smoothly.  The afternoon tea and coffee van finished the day off perfectly!  A special mention and thank you to the Walsh Family for donating out major raffle prize – a GPS sports watch, and the Lee Family for donating Arbonne sports products and goodies in second prize.

In the 7.5km slog up One Tree Hill Road and back through Pioneer Track / Kairns Road, line honours went to UNI’s Nigel Preston in the Open Age Men’s category in a time of 26.07.09, and Bendigo Harriers Matilda Moore in the Open Age Women’s category in a time of 30:08.01

Age group winners in the 7.5km and place getters are as follows:

Open Male 7.5km – 1st place Nigel Preston 26.07.9 (UNI), 2nd Peter Rice 27:20.4 (Harriers), 3rd Ben Stolz 27:36:8 (Harriers)

Open Female 7.5km – 1st Matilda Moore 30.08.1  (Harriers), 2nd Sarah Jalim 30:26:5 (UNI),  3rdTullie Rowe 32:45:1 (UNI)

40+ Male 7.5km – 1st Craig Feuerherdt 28:54:5 (UNI), 2nd Ian Wellard 32:54:1 (HARRIERS), 3rd Danny Burgess 33:16:2 (UNI)

40+ Female 7.5km – 1st Kylie Kaye 36:26:1 (HARRIERS), 2ndTrinity Sanderson 37:47:0 (UNI), 3rd Alison Cartner 38:19:8 (UNI)

50+ Male – 1st Peter Cowell 28:18:5 (HARRIERS), 2nd “Iron” Michael Bieleny 28:20:0 (UNI), 3rd Rossi “Evo” Evans 30:26:5 (Harriers)

50+ Female – 1st Jill Wilkie 33:02:3 (Harriers), 2nd Leanne Healey 35:21:00 (Harriers) 3rd Wendy Stibbe 40:23:2(Harriers),

60+ Male 1st Charles Chambers 33:06:5 (UNI), 2nd Philip Shambrooke 35:52:8 (UNI), 3rd Hunter Gill 36:42:4 (Harriers)

link to results [http://bendigouniathsclub.org.au/ac/raceData/2017/2017-06-03-2017-buac-invite-long-results.html](http://bendigouniathsclub.org.au/ac/raceData/2017/2017-06-03-2017-buac-invite-long-results.html)

More photos will become available from AJ Taylor images and others who took photos at our event.

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4592.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4592.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4594.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4594.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4595.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4595.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4596.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4596.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4598.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4598.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4599.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4599.jpg)

 

 



<!-- END -->
