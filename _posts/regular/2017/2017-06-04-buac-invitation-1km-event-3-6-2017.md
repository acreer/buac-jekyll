---
title: BUAC Invitation – 1km event 3/6/2017
author: Mel Douglass
---

This year UNI introduced an open aged male and female category in this event for the first time and it was well received by entrants in both  open age categories.  Next year we will put the faster runners at the front to save adults tripping over small children in what turned out to be a rather fast paced start to event!

Sash eligibility was for the children, however there were large ribbons for all age group winners both male and female.

UNI’s Abbey Cartner was the sash recipient, being the first girl (under 10’s) over the line in a time of 4:05:5, and Kai Pearce of South Bendigo scored the sash for the fastest boy (under 10’s) in a time of 3:56:8.

The Under 6 age group was a UNI dominated event with Belle Howes 5:54:7 in 1st in the girls, April Wainwright 6:11:8 2nd and Ruby Douglas thrilled to score a ribbon in 3rd 6:29:5.

In the under 6 boys, where UNI domination continued we had Liam Carter in 1st 6:18:6, Samuel Preston in 2nd 6:23:5 and Owen Draper in 3rd 8:05:1

The under 8 boys event had Jorden Matthews (Harriers) in 1st place 4:14:1. UNI’s Jonty McDermid in 2nd 4:42:4, Cale Hinton 4:46:8 in 3rd place.

The under 8 girls was a UNI duo domination and  only two entrants, UNI’s Tully Cripps in 1st place 5:10:6, and Jemma Carter (UNI) 2nd in 6:03

In the under 10 boys event there was a rather large field of entrants, with Kai Pearce (South Bendigo) 3:56:8 1st, Lewis Lonsdale (UNI) in 2nd 4:03:4, and Avery McDermid (UNI) in 3rd 4:12:12

In the under 10 girls event, as mentioned about UNI’s Abbey Cartner in 1st place 4:05:8, South Bendigo’s Chelsea Tickell in 2nd 4:13:2 and UNI’s Lucy McCaig in 3rd 4:17:8

Harriers Sprinter David Morrissey was the fastest male on the day, being on the open aged category in a cracking time 3:11:06, with UNI super-veteran Craig Green not far away, after running in the 3km event in 3:21:9, and Eaglehawk Sprinter Dave Chisholm in 3:30:3

The Open Aged Women had 5 Entrants which is great.  South Bendigo’s Hannah Cotter was first female of the line in 3:59:01, and managed to hold off UNI Veteran Mel Douglas 4:01 who had also ran in the 3km event before, and in 3rd place was Georgie Lee (EAGLEHAWK) 4:49.

Anyway, enough of me and that is my wrap.  Ciao!

Link to results below.

http://bendigouniathsclub.org.au/ac/raceData/2017/2017-06-03-2017-buac-invite-short-results.html

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4581.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4581.jpg)

 

 



<!-- END -->
