---
title: 'AV XCR Results Cruden Farm – 16km and 6km 27/5/2017 #unipride'
author: Mel Douglass
---

A beautiful day of XCR racing at a stunningly good venue – Cruden Farm – yesterday. Compared to last year it was dry underfoot, that is apart from one unavoidable bog! I recommend if you just want to sample one XCR event then this is the one to do. Conditions vary from undulating and occassionally puggy grassy hills to sandy forest.

A number of Uni athletes were out in force. Of our athletes results in the 16km Open Mens were Leon Russell-Keely (9th, 50.28), Craig Green (132…, 61.53), David Heislers (249, 70.14) and Charles Chambers (251, 70.24). In the U20 6km Men Kye Jenkyn (9, 19.37) ran very impressively. In the same event Matthew Heislers stuck it out to finish 24th in 22.27.

Of the other Bendigo region runners 3 of our Junior girls ran placings (Taryn Furletti 1st U16, Yasmin Hayes 2nd U16 and, Matilda Moore 3rd U18) that would have to be an unprecented result. Other Bgo region runners that hit the trail were Matt Hook, Ben Stolz, Alice Wilkinson Anne Buckley and Peter Cowell. Well done to all for flying the Bendigo flag! And especially tothose under the banner of UniPride!!!

Thanks to David Heislers for the update and Alice Wilkinson for the photos.

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/David-H-Cruden-Farm-1.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/David-H-Cruden-Farm-1.jpg) [![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/David-H-Cruden-Farm.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/David-H-Cruden-Farm.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Cruden-Farm.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Cruden-Farm.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Charles-Cuden-Farm.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/Charles-Cuden-Farm.jpg)

 



<!-- END -->
