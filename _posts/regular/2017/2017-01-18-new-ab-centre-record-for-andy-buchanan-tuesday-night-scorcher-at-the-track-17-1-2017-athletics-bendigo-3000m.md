---
title: New AB Centre Record for Andy Buchanan – Tuesday night scorcher at the Track
  – 17/1/2017 – Athletics Bendigo 3000m
author: Mel Douglass
---

Congratulations to BUAC’s [<u><span style="color: #0066cc">Andy Buchanan</span></u>](https://www.facebook.com/andy.buchanan.5205) on setting a new Athletics Bendigo Centre record in the Open Mens 3000m at the track tonight, with a time of 8:20.25. The previous record, set in 2015 by Brady Threlfall was 8:33.48. Great run Andy. Well done also to the large field of runners who turned up and braved the heat in tonights 3000m race.   It was warm out there  with a temperature of around 37 degrees.  Also, a big thank you to all the dedicated officials, time keepers, helpers and those who had the garden hose going out on track to cool us down.

For further reading and details, a full report is available on the Athletics Bendigo FB page and Website thanks again to Craig Green for his brilliant work.   [https://www.athleticsbendigo.org.au/single-post/2017/01/18/Buchanan-beats-the-heat](https://www.athleticsbendigo.org.au/single-post/2017/01/18/Buchanan-beats-the-heat)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/AndyB17Jan2017.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/AndyB17Jan2017.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/AndyB1.jpg?resize=180%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/AndyB1.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/Results17Jan2017.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/01/Results17Jan2017.jpg)



<!-- END -->
