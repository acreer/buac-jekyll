---
title: 2017 Bendigo University Athletics Club Cross Country Invitation including the
  Tracy Wilson Memorial 3km and a 1km event
author: Mel Douglass
---

The BUAC Annual Cross Country Invitation is fast approaching and will be held on Saturday 3rd June 2017.  This also includes the Tracy Wilson Memorial 3km, and a 1km race.  All welcome.  
Best to arrive early so the scheduled 2pm start runs close to on time.  Registration takes place in the La Trobe University  Bendigo Student Union, off Edwards Road, Kennington.

BUAC members please remember to bring double the amount of afternoon tea to cater for our extra guests.

A big thank you to the Walsh Family for donating our major raffle prize, which will be a Garmin running watch.  Ross has organised raffle tickets so we can get started on selling these this weekend.

Should be a great day. You can also tag your photos on the day to Instagram [#unipride](https://www.facebook.com/hashtag/unipride?source=feed_text&story_id=10155471439375757)

Please refer to the attached PDF file outlining all the relevant details.

The following links will take you to the online registration page. Rego window closes midnight the night before the invite and will not take online entries after midnight.

…

[http://my1.raceresult.com/72352/registration?lang=en](http://my1.raceresult.com/72352/registration?lang=en)

[http://my1.raceresult.com/72352/participants…](https://l.facebook.com/l.php?u=http%3A%2F%2Fmy1.raceresult.com%2F72352%2Fparticipants%3Flang%3Den%230_41317A&h=ATNHhozblN0_ENGbCtjkRCMfwzJwfIAbrloOWlEBqyuf0-ws5HXCKroGNol6k1TcTMPuQ8iH7Ut4pveYMs06q3TShlbfqMlpzaQBmTS7AKi8tFMPXSxRJp8W8Aupxix2SKrfU4Wy2BWGNXD9uw&enc=AZO00esQ2Unj7FBzwIN2Oq4rak1sMi3zluQdQ-M8mK49LhY-Z08J-PWP7RGOhwFsW6Snak66zkff-4eHseT5WFqiSyHtz0bXednrw9YnW1zD84NbhNgcAmwcN-shwwxRP_M8d5xyg39zWBsa_l0rQ7fk_EQ39_Dtsp0SexLW6CsBMZSG4TWLyFUKwyKKGmm08mw&s=1)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-Invite-2-Image.jpg?resize=217%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-Invite-2-Image.jpg)

From the PA to the President!



<!-- END -->
