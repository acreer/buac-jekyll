---
title: Mount Macedon Trail Run – BUAC trail runners 4/6/17
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Macedon-trail.jpg
author: Mel Douglass
---

Congratulations to all our super UNI endurance / trail runners who ran at Mount Macedon trail run today.   Bec Wilkinson made the podium, again and we expected nothing less, great work Bec.

Travis Edwards and Dan O’Bree were our two UNI runners who tackled the 50km event.  Well done gentlemen.

Lining up for this event for the second year in a row were our UNI trail runners Philip Shambrook, Andrea Smith, Rhianna Wik – Gamble and Leah Cripps in the 30km event.  Super effort.

Well done also to UNI’s Sue Taylor in the 10km event.

The temperature was one degrees in Bendigo so we can only imagine how cold things were at Mount Macedon?

Trails Plus Mount Macedon Trail Runs.  
BUAC Trail Runners  
50K Travis Edwards 8h00m09s, Dan O’Bree 8.00.12.  
30K – Rebecca Wilkinson, 2nd female.3.17.09.  Phil Shambrook.3.33.54.Andrea Smith.3.46.48.Rhianna Wik-Gamble.3.56.21.Leah Cripps.4.33.05.

10K  Sue Taylor.1.21.16..

Well done to all.  Rest up and recover.

#unipride

 

For more info head to [http://www.trailsplus.com.au/](http://www.trailsplus.com.au/)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Madecon-trail-group.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Madecon-trail-group.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/BecMadedon.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/BecMadedon.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Macedon-trail.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Macedon-trail.jpg)



<!-- END -->
