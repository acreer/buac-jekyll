---
title: Flack Advisory Distance Series Race 8 1000m handicap – Thursday 2nd Feb 2017
author: Mel Douglass
---

Jake Hilson has taken out Race 8 of the Flack Advisory Distance Series over 1000 metres, running a great time of 2.39 minutes!  
Second place went to Michael Preece who clocked the fastest time of 2.38 minutes, with BUAC’s Craig Green in third place.  
Gabrielle Rusbridge was the fastest of the girls with a time of 3.27 minutes.  
Racing returns again next Thursday night, which happens to be one of the Nitro Athletics nights in Melbourne!  
Results; 1000 metres…  
Jake Hilson 2.39  
Michael Preece 2.38 F/T  
Craig Green 3.07  
Adam Parker 2.55  
Isaac Everett 2.55  
John Justice 4.05  
Aaron Norton 3.22  
Connor Findlay 3.39  
Gabrielle Rusbridge 3.27 F/F  
Rick Ermel 3.36  
Jacob Nolan 2.50  
Melissa Douglas 3.45  
Tim Sullivan 3.49  
George Flack 4.24  
Chris Panayi 3.43  
Ross Douglas 3.44

 

Many thanks to Greg Hilson for providing lots of great photos.

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/DistRace8.jpg?resize=300%2C199)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/DistRace8.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/CraigRace8.jpg?resize=300%2C199)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/CraigRace8.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/RossRace8.jpg?resize=300%2C199)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/RossRace8.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Race8.jpg?resize=300%2C200)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Race8.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Dist-8.jpg?resize=300%2C199)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Dist-8.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/RossRace8o.jpg?resize=300%2C199)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/RossRace8o.jpg)[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Race8o.jpg?resize=300%2C199)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Race8o.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Dist-Series8.jpg?resize=300%2C199)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/02/Dist-Series8.jpg)



<!-- END -->
