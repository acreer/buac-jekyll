---
title: Track and Field Award Winners 2016/2017 Season – Foundry Arms Hotel Social
  Night out 24/6/2017
author: Mel Douglass
---

The BUAC Mid Year Night and Inaugural Track Awards was held at the Foundry Arms Hotel last night.  We filled the Stirling Room at the Foundry with 60 members coming along.  A great night to socialise with our fellow athletes and their families, and recognise the efforts of our summer track and field athletes, and our canteen volunteer Lynley McDonald.   Lynley’s help in the canteen over the summer track season has been greatly appreciated and makes it possible for our athletes to concentrate on their events, rather than playing tag team in the canteen when UNI is rostered on for duty.

With a record number of UNI athletes now participating in track and field events, this is the first year UNI has had track and field awards. Some basic awards were put together based on statistics, results and points tally’s provided by Athletics Bendigo. We can always add to these or improve things next year or add age group awards. Thanks to Craig Green for providing all the data to support the awards.

The 2016/2017 season – UNI has added 5 new records to the Athletics Bendigo record book, which is just outstanding.    This would also be the first year we have had two UNI runners take out the titles in the Bendigo 10km track championships. To add to the success our UNI track and field athletes have competed far and wide across the country in Country Championships, Master Games, AV Shield Finals, AV Track and Field Titles, Australian Titles. Andy Buchanan also ventured over to America to compete against some of the worlds elite.  We have #unipride being flown everywhere!

Our awards winners for the 2016/2017 Track and Field Season are:

**Athletics Bendigo Records – Awards**

16/2/17 UNI Mens 4 x 800m Relay team, 50+ AB Residential & Centre Record 10:14:76 – Ross Douglas, Michael Bieleney, David Heislers and Jim Russell.

17/12/16 Mens Open 10,000 AB Residential Record 29:17:03 (Landy Field) – Andy Buchanan

19/11/16 Mens Open 1500m AB Centre Record 3:53:83 (Bendigo) – Andy Buchanan

17/1/17 Mens Open 30000m AB Centre Record 8:20:25 (Bendigo) – Andy Buchanan

28/3/17 Under 15 Male 3000m AB Residential Record 9.04.80 (Homebush) – Jack Hickman

**Individual Awards**

2016/17 Track Season – Long Distance Award – Craig Green & Melissa Douglas

2016/17 Track Season – Jumps Award – Hamish Wild & Maya van Es

2016/17 Track Season – Throws Award – Anthony Messerle & Maya van Es

2016/17 Track Season – Sprints Award – Ross Douglas & Jorja Van Den Berg

 

**Special Recognition Awards**

2016/2017 Track Season – National Recognition Award – Andy Buchanan, Jack Hickman & Wil McCaig

2016/2017 BUAC Masters Athlete of the Year – Craig Green

2016/2017 Track Season Presidents Awards – Volunteer Recognition – Lynley McDonald

 

**Point scores for Individual Awards 2016/2017 Season**

Craig Green 2840 points (long distance)

Melissa Douglas 2552 points (long distance)

Ross Douglas 2099 points (sprints)

Georgia Van Den Berg 1970 points (sprints)

Hamish Wild 1437 points (jumps)

Maya Van Es 1423 points (jumps)

Anthony Messerle 2129 point (throws)

Maya Van Es 543 points (throws)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/FullSizeR-e1498352511794-200x300.jpg?resize=200%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/FullSizeR-e1498352511794.jpg) [![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8743-e1498352555379-225x300.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8743-e1498352555379.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8747-e1498352599525-225x300.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8747-e1498352599525.jpg) [![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8748-e1498352643104-225x300.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8748-e1498352643104.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8758.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8758.jpg) [![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8759-e1498352710444-225x300.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8759-e1498352710444.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Lynley-e1498352778428-226x300.jpg?resize=226%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Lynley-e1498352778428.jpg) [![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Maya-e1498352823650-199x300.jpg?resize=199%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Maya-e1498352823650.jpg) [![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8752-e1498352903915-225x300.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/IMG_8752-e1498352903915.jpg)

 



<!-- END -->
