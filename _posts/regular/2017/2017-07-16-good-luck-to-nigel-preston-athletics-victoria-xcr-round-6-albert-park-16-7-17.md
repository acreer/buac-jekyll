---
title: Good luck to Nigel Preston – Athletics Victoria XCR Round 6 – Albert Park 16/7/17
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/NigelPrestonWandin-13May2017.jpg
author: Mel Douglass
---

Good luck to Uni’s [Nigel Preston](https://www.facebook.com/nigepresto?fref=mentions), running AV at Albert Park Lake today in the 10km road race.  A nice flat and fast course.  Good luck also to others representing the Bendigo Region. Go Bendigo and Go Nigel! Will post results when they become available later. [#unipride](https://www.facebook.com/hashtag/unipride?source=feed_text)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/NigelPrestonWandin-13May2017.jpg?resize=300%2C200)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/NigelPrestonWandin-13May2017.jpg)



<!-- END -->
