---
title: Mid Year Social Night Out / Track Season Awards – Foundry Arms Hotel 24/6/2017
author: Mel Douglass
---

Just a reminder – next Saturday 24th June 2017 is our Mid Year Social Night out at the Foundry Arms Hotel Stirling Room. This is a child friendly venue so bring the family along for a great night. Meals are counter meals from the menu and drinks are from the bar, so you just pay as you go. Easy peasy and no having to split bills etc.

The evening will also now be including the 2016/2017 BUAC Inaugural Track Season awards to celebrate our increased and successful UNI representation on the running track at local, state, masters, interstate events and international events if your name happens to be [Andy Buchanan](https://www.facebook.com/andy.buchanan.5205?fref=mentions). Awards will be for distance running, sprints, jumps and throws, which UNI had both male and female representation in a range of events and age groups. As they say, Jamaica now has a bob sled team (-;

Will aim to keep the formalities to a minimum so we can enjoy the social aspect of the evening. Should be a good night so if you can confirm your attendance, that would be great.

The Foundry Arms Complex is located at 2 Old High Street, Golden Square for those who have not been there before. 

Time : from 6.30pm

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/20170216_174335.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/20170216_174335.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/AndyB1.jpg?resize=180%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/AndyB1.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161115_200522-e1479522460486-180x300.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161115_200522-e1479522460486.jpg)



<!-- END -->
