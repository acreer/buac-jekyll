---
title: BUAC XC Invitation – Tracy Wilson Memorial 3000m 3/6/2017
author: Mel Douglass
---

Congratulations to UNI’s Wil McCaig and South Bendigo’s Yazmin Hayes who took out the Tracy Wilson Memorial 3000m Cross Country Event.    Both showing their usual good form to take home the sash for each category.  Wil completed the 3000m mostly off road course in a time off 10:26:7 with Ben Powell 10:30:2 (Harriers) and Riley Osborne 10:33:6 (Harriers) putting the pressure on the pace and was not far behind.   In the Junior Female event, Yazmin finished in a time of 11:17:2. with a clear win from her nearest rival Zahli Drummond(Harriers) 12:20:2.

Results for age group placing are:

3km – Open Male – 1st Ben Powell 10:30:2 (Harriers), 2nd Craig Green 11:12:2 (UNI), 3rd David Morrissey 11:32:4 (Harriers)

Open Female – 1st Madison Hooke 13:13:7 (Harriers), 2nd Melissa Douglas 13:40 (UNI), Chloe Green 16:05 (UNI)

Under 16 Male – Wil McCaig 10:26:7 (UNI), 2nd Riley Osborne 10:33:6, Matt Buckell 10:47:0 (Harriers)

Under 16 Female – 1st Yazmin Hayes 11:17:2 (South Bendigo), 2nd Zahli Drummond 12:20:2 (Harriers), 3rd Bridie Blake-Burrows 12:54:1 (UNI)

Under 12 Male – 1st Logan Tickell 11:43:6 (South Bendigo), 2nd Oscar Fox 12:52:00 (South Bendigo), 3rd Max Rowe 13:03:3 (UNI)

Under 12 Female – 1st – Charlotte Murphy 14:19:3 (UNI), 2nd Kayla McManus 15:44:2 (UNI), 3rd Charlotte Dashwood 16:05:2

full results for the Tracy Wilson Memorial are at [http://bendigouniathsclub.org.au/ac/raceData/2017/2017-06-03-2017-buac-invite-medium-results.html](http://bendigouniathsclub.org.au/ac/raceData/2017/2017-06-03-2017-buac-invite-medium-results.html)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4602.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4602.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4603.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4603.jpg)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4607.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/100_4607.jpg)

 



<!-- END -->
