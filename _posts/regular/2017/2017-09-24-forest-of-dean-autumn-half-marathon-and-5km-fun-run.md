---
title: Forest of Dean Autumn Half Marathon and 5km Fun Run
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/09/JeremyDraperphoto.jpg
author: Mel Douglass
---

Congratulations to Jeremy Draper and daughter Bell Draper on their efforts in the Forest of Dean Autumn events. (Gloucestershire England, just near the Welsh border).  Jeremy ran a new personal best in the half marathon in a time of 2:04:44, and daughter Bell ran in the 5km fun run in a time of 30:10.  Fantastic effort.

“Bella and I are now ‘international’ runners, having done the Forest of Dean Autumn Half and 5km! ![](https://i1.wp.com/www.facebook.com/images/emoji.php/v9/f57/1/16/1f609.png?resize=16%2C16&ssl=1);-)”

Bella Draper – 5km 30:10

Jeremy Draper – Half 2:04:44 (pb)

[ Further information about this event is on the this link.  Looks like rego’s are open for next year too!](http://www.forestofdean-halfmarathon.co.uk/index.php)

[http://www.forestofdean-halfmarathon.co.uk/index.php](http://www.forestofdean-halfmarathon.co.uk/index.php)

 

 

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/09/JeremyDraperphoto.jpg?resize=255%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/09/JeremyDraperphoto.jpg)



<!-- END -->
