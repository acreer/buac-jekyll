---
title: Online Pre-registrations for BUAC XC Invitation close at Midnight tonight 2/6/17
author: Mel Douglass
---

Just a friendly reminder, if you haven’t done so already, please remember to do your online pre-registration for the Bendigo University Athletic Club XC Invitation events being held tomorrow.

Online entry means you also get to save a few dollars on the entry cost and it closes at midnight tonight.  Please go to the website [http://bendigouniathsclub.org.au/ac/](http://bendigouniathsclub.org.au/ac/)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/13173233_10154981867542195_2390929141930253133_o.jpg?resize=300%2C217)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/13173233_10154981867542195_2390929141930253133_o.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-Invite-2-Image.jpg?resize=217%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/05/BUAC-Invite-2-Image.jpg)

 

 

 



<!-- END -->
