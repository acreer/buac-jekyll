---
title: Congratulations to UNI’s Russ Jenkins – Australian Masters Athletics Marathon
  Champion (55-59 male) age group
author: Mel Douglass
---

Congratulations to [Russ Jenkins](https://www.facebook.com/russ.jenkins.963?fref=mentions) “The Banker” who has claimed some new titles after his age group win this morning at sunny Gold Coast. In the 55-59 male age group, IAAF Oceania Area Marathon Champion, Queensland Marathon Champion, Australian Masters Athletics Marathon Champion & Australian Open Running Club Marathon Champion. Looks like having personal water bottle assistants out on course at Broadbeach and Burleigh Heads worked well. ![](https://i1.wp.com/www.facebook.com/images/emoji.php/v9/f57/1/16/1f609.png?resize=16%2C16&ssl=1)😉 Fantastic result 2:54:57 (4:08 p/km). Russ said he would be at the Burleigh turn around at about 8:22am to collect water bottle number 2 and arrived pretty much right on that time, so a well paced event.

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Banker-GCn.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/07/Banker-GCn.jpg)



<!-- END -->
