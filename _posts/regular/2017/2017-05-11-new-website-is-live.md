---
title: New Website is LIVE
author: Andrew Creer
---

Hello BUAC

If you are looking for results you wont find them here anymore.

John Robinson and I have launched a new version of the website.

Its mobile friendly and quick, and where I plan to put results and handicaps in the future.

Just go [http://bendigouniathsclub.org.au/ ](http://bendigouniathsclub.org.au/)The Old site is still there

here if you need it [http://bendigouniathsclub.org.au/index.old.html](http://bendigouniathsclub.org.au/index.old.html)

Thanks John and Andrew



<!-- END -->
