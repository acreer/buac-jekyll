---
title: UNI’s Andy Buchanan – Victorian State 10km XCR Champion, again in 2017.   17/6/17
header:
  teaser: http://bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/AndyB10km.jpg
author: Mel Douglass
---

Congratulations to UNI’s Andy Buchanan on winning back to back Victorian State 10km XCR Athletics Victoria Titles at Bundoora Park on Saturday. Winning by a comfortable 120m ahead of the nearest rival.  Bundoora Park is famous for its gruelling slopes.  This event was also a selection race for the Australian Cross Country Titles, and was held in conjunction with the Victorian All Schools Cross Country Championships.  The Juniors raced over 4, 6 & 8 km.   This year would have to be UNI’s biggest AV representation at AV XCR events which is just great to see.

UNI has been well represented with Andy Buchanan, Nigel Preston forming part of men’s division 5 team for the Bendigo Region, with Bendigo Harriers Ben Stolz and South Bendigo’s Daniel Plowright.

UNI’s Craig Green and David Heislers formed part of a mens division 7 team for the Bendigo Region with Bendigo Harriers Rossi Evans and South Bendigo’s Leigh Browell.

**Div 5 Team Results**

Andrew Buchanan (UNI) 1st State 10km XCR Champion 31:01

Nigel Preston (UNI) 10km 35:02

Ben Stolz (Harriers) 10km 37:55

Daniel Plowright (SB) 10km 38:51

**Div 7 Team Results**

Craig Green (UNI) 7th (45-49 age group) 39:01

Rossi Evans (Harriers) 10km 41:39

David Heislers  (UNI) 10km 45:33

Leigh Browell (SB) 10km 47:52

**Under 20  Mens 8km Event**

UNI’s Kye Jenkyn had a cracking race in the Under 20’s 8km event to finish 8th in the AV Event, 15th Overall in a time of 28:18.

**Victorian All Schools State Final**

Congratulations to our UNI juniors who competed in the Victorian State All Schools Event:

Wil McCaig (UNI) 14th placing. under 16 male – 4km time – 13:33

Toby McCaig (UNI) 79th, under 14 male – 3km time – 12:22

Bridie Blake – Burrows (UNI) 30th under 16 girls – 4km 16:30

Jazlin Fear (UNI) – under 15 girls 4km – 18:55

Congratulations to all our UNI runner and Bendigo Regional Runners, so pleasing to see Bendigo Region doing so well at these AV events.  #unipride

Craig Green has prepared a full report and results for the all the Bendigo Region Runners below and links to the post race interview with Andy Buchanan and highlights of the 10km race.  There are also links to the Victorian All Schools Results.   See link below:

[https://www.athleticsbendigo.org.au/single-post/2017/06/17/Buchanan-and-Furletti-victorious-at-Bundoora](https://www.athleticsbendigo.org.au/single-post/2017/06/17/Buchanan-and-Furletti-victorious-at-Bundoora)

[![](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/AndyB10km.jpg?resize=168%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/AndyB10km.jpg)

[![](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Andy-B-10km-Bundoora.jpg?resize=300%2C271)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Andy-B-10km-Bundoora.jpg)

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Nigelo.jpg?resize=300%2C200)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2017/06/Nigelo.jpg)



<!-- END -->
