---
title: Congratulations to Andy Buchanan – new AB 1500m Open Mens Centre Record holder
  19/11/16
author: Mel Douglass
---

Congratulations to BUAC’s [<u><span style="color: #0066cc">Andy Buchanan</span></u>](https://www.facebook.com/andy.buchanan.5205) on breaking the Open Mens 1500 m AB Centre Record last Saturday  at the track, a record that has stood since 1985.   Andy you make running 1500m look so easy. Great run.  For those who missed the article this week in the Bendigo Advertiser, you can read all about it by clicking on the link below. #unipride

[http://www.bendigoadvertiser.com.au/story/4304303/buchanan-sizzles-to-break-bendigo-1500m-record/](http://www.bendigoadvertiser.com.au/story/4304303/buchanan-sizzles-to-break-bendigo-1500m-record/)



<!-- END -->
