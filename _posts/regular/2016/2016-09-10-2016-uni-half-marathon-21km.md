---
title: 2016 Uni Half Marathon – 21km
---

### Complete 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Complete.csv”][/table]

### Female 40+ 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Female\_40plus.csv”][/table]

### Female 40+ 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Female\_60plus.csv”][/table]

### Female Open 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Female\_Open.csv”][/table]

### Male 40+ 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Male\_40plus.csv”][/table]

### Male 50+ 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Male\_50plus.csv”][/table]

### Male Open 21km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_21km\_Male\_Open.csv”][/table]



<!-- END -->