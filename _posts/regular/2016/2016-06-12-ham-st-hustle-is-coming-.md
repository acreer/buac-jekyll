---
title: Ham St Hustle is coming!
---

This run tends to polarise people.

I love it but some do not.

On the positive side, there are stunning views, nice bush and quite alot of down hill. Also as far as the long runs go it is one of the shorter offerings at only 6.5k.

And I just drove around the course and did not even need the car in four wheel drive.

**Check out this segment on Strava: [Ham Street Hustle](https://www.strava.com/segments/9441186)**

There is ONLY 49m of elevation gain, and the finish is  down hill.



<!-- END -->