---
title: BUAC at AV XCR’16 10km & Victorian All Schools XC championships 18 June 2016
---

Congratulations to all our BUAC and local competitors at the Victorian All Schools Cross Country Championships and also serving as the selection trial for the Australian Cross Country Championships, Bundoora Park hosts round 5 of the XCR’16 series with 10km, 8km, 6km, 4km and 3km races from Open and Overage down to Under 14.

2016 Victorian All Schools Cross Country Champs  
Under 14 Girls  
20th Tullie Rowe Bendigo South East 3km 11:58.9  
Under 16 Girls…  
12th Bridie Blake-Burrows Bendigo South East 4km 15:39.7  
Under 15 Boys  
28th Wil McCaig Bendigo South East 4km 14:28.9  
Under 20 Men  
6th Matthew Heislers Girton Grammar 8km 26:56.7  
Under 16 Boys  
16th Luke Padgham Bendigo South East 4km 13:29.8

XCR’16 Bundoora Park Rd 5

Open Men  
1st Andrew Buchanan Melbourne University  
10km 30:34.4  
265th David Heislers Bendigo Region 10km 44:12.7  
177th Darren Hartland Bendigo Region 10km 40:09.6  
211th Craig Green Bendigo Region 10km 41:36.7  
303rd James Russell Bendigo Region 10km 46:11.9

[![13445368_1046109088790642_8029630299553057962_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13445368_1046109088790642_8029630299553057962_n.jpg?resize=225%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13445368_1046109088790642_8029630299553057962_n.jpg)

Provisional results.

[All-Schools-Results-2016](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/All-Schools-Results-2016.pdf) [XCR16-Rd-5-Results](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/XCR16-Rd-5-Results.pdf)

[XCR16-Rd-5-Results](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/XCR16-Rd-5-Results.pdf)

http://athsvic.org.au/generalnews/xcr16-round-5-bundoora-park/

http://athsvic.org.au/wp-content/uploads/XCR16-Rd-5-Results.pdf

http://athsvic.org.au/wp-content/uploads/All-Schools-Results-2016.pdf



<!-- END -->