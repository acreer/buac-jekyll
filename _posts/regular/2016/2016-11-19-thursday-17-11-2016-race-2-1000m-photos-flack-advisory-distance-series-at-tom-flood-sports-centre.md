---
title: Thursday 17/11/2016 Race 2 1000m Photos – Flack Advisory Distance Series at
  Tom Flood Sports Centre
author: Mel Douglass
---

[![20161117_191404](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191404.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191404.jpg) [![20161117_191357](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191357.jpg?resize=300%2C180)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191357.jpg) [![20161117_191327](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191327-e1479522750199-180x300.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191327-e1479522750199.jpg)

[![20161117_191332](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191332-e1479522845318-180x300.jpg?resize=180%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191332-e1479522845318.jpg) [![20161117_191259](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191259-e1479522613931-300x180.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191259-e1479522613931.jpg) [![20161117_191242](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191242.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/20161117_191242.jpg)



<!-- END -->
