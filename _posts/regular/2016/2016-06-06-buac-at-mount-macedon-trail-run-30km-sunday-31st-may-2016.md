---
title: BUAC at  – Mount Macedon Trail Run 30km – Sunday 31st May 2016
---

Congratulations to our BUAC team of runners who competed at Mt Macedon.  In freezing conditions, a team of BUAC runners mad their way to Mount Macedon for Race 2 of the Trail Plus Mountain Trails Series for the 30km mountain goat run.

Rebecca Wilkinson and Els Viester had extremely strong runs and managed to secure a spot on the podium in 2nd and 3rd place.  Rebecca’s super-mum Lisa Wilkinson was not too far away in 7th place effectively securing the win for BUAC in the Womens 30km Teams competition, along with Andrea Smith and Leah Cripps.   Well done to our mens team too, great effort from David Sebastian Serven, Danny Burgess and Philip Shambrook.  #Unipride all round.

On a course with plenty of elevation (1110m in total according to Strava!), it was fantastic to see  newer runners taking on such an event for the first time.   It is expected that the BUAC team will grow in this event next year as trail running has become a unique sport of its own.  These results also highlight the diverse range of runners our club has over different distances and types of events they participate in.

Rebecca Wilkinson 3.06.16.2nd placed female, Els Viester 3.13.13 3rd female, David Sebastian Serven 3.25.46,Lisa Wilkinson 3.26.48,Danny Burgess 3.42.18,Philip Shambrook,4.00.12,Rhianna Wik-Gamble4.14.51,Andrea Smith 4.24.21,Leah Cripps 4.40.52.

**Macedon 30k Women’s Team Competition Results.**

(copied from the Macedon Trail Run Facebook Page)

Congratulations to the Bendigo University Athletics Club for a VERY strong performance in the Women’s 30k Race, comfortably taking out 1st Place in the Teams Competition.. with solid performances by the Teams from Dandenong Trail Runners (2nd) and Surf Coast Trail Runners (3rd).

Well done to everyone! And hope to see the Bendigo University Athletics Club returning in 2017 to defend their title _😉_

Rebecca Wilkinson 30km Bendigo University Athletics Club 3:06:16  
Els Viester 30km Bendigo University Athletics Club 3:13:13  
Lisa Wilkinson 30km Bendigo University Athletics Club 3:26:48  
Leah Cripps 30km Bendigo University Athletics Club 4:40:52  
Andrea Smith 30km Bendigo university athletics club 4:24:21

TOP 3 TOTAL = 9:46:17

Cheryl Martin 30km DTR 3:17:19  
Melinda McLeod 30km DTR 4:08:54  
Sharee George 30km DTR 4:28:08

TOP 3 TOTAL = 11:54:21

Emma-jane Dowdell 30km SCTR 3:50:16  
Rhonda Gale 30km SCTR 4:27:15  
Sandra Henry 30km SCTR 4:27:18  
Catherine Dickson 30km SCTR 4:28:20

TOP 3 TOTAL = 12:44:49

http://www.trailsplus.com.au/macedon/

http://www.bendigoadvertiser.com.au/story/3930263/mountain-challenge-for-runners/?cs=80

[![13342041_1728083454121268_1024930310_n](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13342041_1728083454121268_1024930310_n.jpg?resize=300%2C200)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13342041_1728083454121268_1024930310_n.jpg) [![13383675_1728066620789618_560738406_o](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13383675_1728066620789618_560738406_o.jpg?resize=300%2C200)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13383675_1728066620789618_560738406_o.jpg) [![13319834_10206675710031726_7133880106660911496_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13319834_10206675710031726_7133880106660911496_n.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13319834_10206675710031726_7133880106660911496_n.jpg) [![13327637_10206675709831721_4463678573255781982_n](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13327637_10206675709831721_4463678573255781982_n.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13327637_10206675709831721_4463678573255781982_n.jpg)

[![13323808_1338810816134185_408026586821690532_o](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13323808_1338810816134185_408026586821690532_o.jpg?resize=300%2C200)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13323808_1338810816134185_408026586821690532_o.jpg) [![13335547_10206675710911748_9140953680947784025_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13335547_10206675710911748_9140953680947784025_n.jpg?resize=300%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13335547_10206675710911748_9140953680947784025_n.jpg)[![13323795_10206734680625954_5687507497661821315_o](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13323795_10206734680625954_5687507497661821315_o.jpg?resize=300%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/06/13323795_10206734680625954_5687507497661821315_o.jpg)



<!-- END -->