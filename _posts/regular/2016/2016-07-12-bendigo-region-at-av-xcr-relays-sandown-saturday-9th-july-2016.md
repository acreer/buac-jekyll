---
title: Bendigo Region at AV XCR Relays Sandown – Saturday 9th July 2016
---

BUAC runners were well represented amoung the Bendigo Region team that made the trip to Sandown on Saturday morning for the  Athletics Victoria XCR Sandown Relays. 

 

For the middle of winter in Melbourne’s wind tunnel, conditions were near perfect for fast running on the well known 3.1km racing car circuit. The Open, Vets and Under 20 running 6.2km and the juniors 3.1km.

Fantastic effort and congratulations to all the Bendigo Region runners, and of course those from BUAC – Melissa Douglas, Ingrid Douglass, Darren Hartland, Craig Green,  David Heislers, Jim Russell and Matthew Heislers.  This is a large representation from our club for an AV event, having runners form part of the teams in the 40+ mens, 50+ mens, under 20 mens and open womens categories.  With  Ingrid Douglass and myself venturing onto the AV circuit for the very first time, I must say the experience was awesome.    Fast, but still awesome.

Thanks to Jim Russell for being the bus driver and Luke Matthews for helping with navigation.

A comprehensive race report has been prepared thanks to Craig Green and is available on the Athletics Bendigo website link below.  Thanks Craig.  http://www.athleticsbendigo.org.au/single-post/2016/07/11/Six-teams-do-Bendigo-proud

** RESULTS**

 **Div 5 Men (4 x 6.2km’s) – 1:34.07 3rd place.**

22.39 Ben Stolz (Bendigo Harriers)

22.56 Matt Hooke (Bendigo Harriers)

24.00 Lachlan Carr (Bendigo Harriers)

24.32 Luke Matthews (Bendigo Harriers)

 

**Div 5 Women (3 x 6.2km’s) – 1:23.50 5th place.**

27.30 Melissa Douglas (Bendigo University)

29.35 Yvette Tuohey (Bendigo Harriers)

26.45 Ingrid Douglass (Bendigo University)

 

**40+ Men (3 x 6.2km’s) – 1:12.10 4th place.**

22.38 Darren Hartland (Bendigo University)

22.57 Craig Green (Bendigo University)

26.35 Leigh Browell (South Bendigo)

 

**50+ Men (3 x 6.2km’s) – 1:18.23 9th place.**

26.07 David Heislers (Bendigo University)

25.38 Jim Russell (Bendigo University)

26.38 Eric Baker (Bendigo Harriers)

 

**Under 20 Men (3 x 6.2km’s) – 1:12.32 7th place.**

25.04 Isaac Everett (Bendigo Harriers)

24.02 Rhys Lias (South Bendigo)

23.26 Matthew Heislers (Bendigo University)

 

**Under 18 Women (3 x 3.1km’s) – 42.13 14th place.**

12.22 Maddison Hooke (Bendigo Harriers)

15.06 Chloe Green (Bendigo Harriers)

14.45 Emily Heislers (Eaglehawk)

 

![](https://i2.wp.com/static.wixstatic.com/media/2daf1f_c3dd35dde80c4618a1613516d006eb69~mv2.jpg/v1/fill/w_538,h_359,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_c3dd35dde80c4618a1613516d006eb69~mv2.jpg?w=474&ssl=1)

 

![](https://i1.wp.com/static.wixstatic.com/media/2daf1f_063dbde8ca5e436da1a945d95f65cfb7~mv2.jpg/v1/fill/w_538,h_359,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_063dbde8ca5e436da1a945d95f65cfb7~mv2.jpg?w=474&ssl=1)

 

 

![](https://i0.wp.com/static.wixstatic.com/media/2daf1f_7b3faa24088e431783bd99dc36415c96~mv2.jpg/v1/fill/w_538,h_359,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_7b3faa24088e431783bd99dc36415c96~mv2.jpg?w=474&ssl=1)

 

 

![](https://i2.wp.com/static.wixstatic.com/media/2daf1f_d9da1809a5a74f44b49c43a21c8e1332~mv2.jpg/v1/fill/w_538,h_363,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_d9da1809a5a74f44b49c43a21c8e1332~mv2.jpg?w=474&ssl=1)

 

![](https://i2.wp.com/static.wixstatic.com/media/2daf1f_6428c2991f9240b3a1688ebd2078cc56~mv2_d_1958_1399_s_2.jpg/v1/fill/w_538,h_384,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_6428c2991f9240b3a1688ebd2078cc56~mv2_d_1958_1399_s_2.jpg?w=474&ssl=1)

 

 

![](https://i1.wp.com/static.wixstatic.com/media/2daf1f_711609267f474ceeb6bafb863556d42a~mv2.jpg/v1/fill/w_538,h_440,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_711609267f474ceeb6bafb863556d42a~mv2.jpg?w=474&ssl=1)

 

 

 

 

 

![](https://i0.wp.com/static.wixstatic.com/media/2daf1f_2aa533d4fbf4455a8de59b0789aa271c~mv2.jpg/v1/fill/w_538,h_359,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_2aa533d4fbf4455a8de59b0789aa271c~mv2.jpg?w=474&ssl=1)

 

 

![](https://i1.wp.com/static.wixstatic.com/media/2daf1f_bc85e82a1daa49718013d085336f7b78~mv2.jpg/v1/fill/w_538,h_359,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_bc85e82a1daa49718013d085336f7b78~mv2.jpg?w=474&ssl=1)

 

![](https://i2.wp.com/static.wixstatic.com/media/2daf1f_a0f8a414aca648c799c0864d36e509ae~mv2.jpg/v1/fill/w_313,h_417,al_c,lg_1,q_80/2daf1f_a0f8a414aca648c799c0864d36e509ae~mv2.jpg?w=474&ssl=1)

![](https://i1.wp.com/static.wixstatic.com/media/2daf1f_a38773e4e1bd4703b9bc2143a87d2c8b~mv2.png/v1/fill/w_295,h_476,al_c,usm_0.66_1.00_0.01/2daf1f_a38773e4e1bd4703b9bc2143a87d2c8b~mv2.png?w=474&ssl=1)

![](https://i1.wp.com/static.wixstatic.com/media/2daf1f_35b0b97ed1b74ef48c389366c996b22c~mv2.jpg/v1/fill/w_538,h_330,al_c,q_80,usm_0.66_1.00_0.01/2daf1f_35b0b97ed1b74ef48c389366c996b22c~mv2.jpg?w=474&ssl=1)

 

 

<!-- No Footer -->



<!-- END -->