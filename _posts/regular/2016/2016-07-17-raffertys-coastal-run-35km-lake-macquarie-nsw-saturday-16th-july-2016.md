---
title: Raffertys Coastal Run 35km – Lake Macquarie NSW Saturday 16th July 2016
---

Congratulations to Craig Feuerherdt and Maree Platt on their super efforts in the Rafferty’s Coastal Run at Lake Macquarie 35km event.  The course sounds amazing, taking in the scenery of beautiful Lake Macquarie and the stunning coastline of historic Catherine Hill Bay and the Munmorah State Conservation Area.   Great work Feuerherdt family.   Might have to add this one to our “to do” list next year.

**35km Coastal Run Results**

Craig Feuerherdt 3.01.49  (6th in his age group 30-49 years)  
Maree Platt 4.34.51.

Lachlan Feuerherdt,  Luke Feuerherdt  and Josh Feuerherdt ran in the kids fun run.  No results available for this event.

More information about this run can be found at http://raffertyscoastalrun.com.au/



<!-- END -->