---
title: BUAC 2016 – Presentation Night Award Winners XC Season
---

 **Bendigo University Athletic Club**

**2016 Cross Country Season**

Congratulations to all our award winners for 2016.

| **Club Champion Awards** | | |
| **Senior Club Champion** | **1<sup>st</sup> Place** | **Ben McDermid** |
| Senior Club Champion | 2nd Place | Chris Giffin |
| Senior Club Champion | 3rd Place | Paul Lomax |
| | | |
| **Intermediate Club Champion** | **1<sup>st</sup> Place** | **Beatrice Lonsdale** |
| Intermediate Club Champion | 2nd Place | Josephine Robinson |
| Intermediate Club Champion | 3rd Place | Lynley McDonald |
| | | |
| **Junior Club Champion** | **1<sup>st</sup> Place** | **Geoffrey Robinson** |
| Junior Club Champion | 2nd Place | Avery McDermid |
| Junior Club Champion | 3rd Place | Jonty McDermid |
| | | |
| **Speed Champion Award** | | |
| **Senior Male Speed Champion** | **1<sup>st</sup> Place** | **Rik McCaig** |
| Senior Male Speed Champion | 2nd Place | Matthew Heislers |
| Senior Male Speed Champion | 3rd Place | John Robinson |
| | | |
| **Intermediate Male Speed Champion** | **1<sup>st</sup> Place** | **Will McCaig** |
| Intermediate Male Speed Champion | 2nd Place | Toby McCaig |
| Intermediate Male Speed Champion | 3rd Place | Connor Bedson |
| | | |
| **Junior Male Speed Champion** | **1<sup>st</sup> Place** | **Lachlan Feuerherdt** |
| Junior Male Speed Champion | 2nd Place | Lewis Lonsdale |
| Junior Male Speed Champion | 3rd Place | William Robertson |
| | | |
| **Senior Female Speed Champion** | **1<sup>st</sup> Place** | **Sarah Jalim** |
| Senior Female Speed Champion | 2nd Place | Rebecca Wilkinson |
| Senior Female Speed Champion | 3rd Place | Els Viester |
| | | |
| | | |
| **Intermediate Female Speed Champion** | **1<sup>st</sup> Place** | **Bridie Blake – Burrows** |
| Intermediate Female Speed Champion | 2nd Place | Tullie Rowe |
| Intermediate Female Speed Champion | 3rd Place | Melissa Douglas |
| | | |
| **Junior Female Speed Champion** | **1<sup>st</sup> Place** | **Abbey Cartner** |
| Junior Female Speed Champion | 2nd Place | Carissa Brooke |
| Junior Female Speed Champion | 3rd Place | Eden Howes |
| | | |
| **Elite Achievement Awards – 2016 National Cross Country Representatives** |
| Andrew Buchanan | | |
| Matthew Heislers | | |
| Tullie Rowe | | |
| | | |
| **Going the Distance – Attendance Award (attended every event)** | |
| Beatrice Lonsdale | | |
| Max Rowe | | |
| | | |

**XC 2016 Athletics Victoria Representation (Senior) Awards – medals**

Andrew Buchanan

James Russell

David Heislers

Craig Green

Darren Hartland

Matthew Heislers

Melissa Douglas

Ingrid Douglass

 

**Presidents Award – Andrea Smith**

 



<!-- END -->