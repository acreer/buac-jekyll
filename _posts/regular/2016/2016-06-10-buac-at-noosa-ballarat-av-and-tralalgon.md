---
title: BUAC at  – Noosa, Ballarat AV and Tralalgon
---

 **4 June 2016 – Athletics Victoria XCR Round 4, Ballarat 15.7km Road Race**   – Congratulations to Craig Green and Darren Hartland were our representatives in the division 5 team from the Bendigo Region.  Darren and Craig both had impressive runs, winning the division 5 team event with their two other team members, Anne Buckley and Matt Hooke from the Bendigo Harriers AC.    Fantastic results.

Results – Darren Hartland 100.34 and Craig Green101.38.   It is fantastic to have two of our new members competing and doing very well in this AV event.  More details are on the club Facebook page and links to Athletics Victoria results.

http://athsvic.org.au/wp-content/uploads/merged\_document\_2.pdf

**5 June 2016 – Traralgon Marathon** – two our of BUAC Ironman / Ironwoman competitors – Jac Tremayne and Russ Tremayne made their way over to the Traralgon Marathon.  Both putting in strong runs over the 42.195 km journey with Jac taking our age group honours in her category, and Russ placing 2nd in his age group category.   I’m sure this is great preparation for any future Ironman Events Jac and Russ have coming up with such fast results.  Again, showing what a diverse range of athletes our club consists of.  Great running.

Marathon Results – Jac Tremayne 3:49:22, Russ Tremayne 1:28:37

 

**28-29 May 2016 – Noosa Multi Sport Festival Fun Run, Half Marathon and Ocean Swim.**

Russ Jenkins and Jenni Jenkins made their way to sunny Noosa for the Noosa Multisport Festival.

New to BUAC this year, Jenni participated in the Swim Noosa  event as well as the 5km Fun Run doing very well in both.  Jenni placed 7th in her age group in the 1km Ocean Swim and 2nd in her age group in the 5km Fun Run.  For someone new to running, this is fabulous effort by Jenni.  We expect that Jenni can only further improve with some cross country racing this year.

Russ Jenkins lined up for the 21.1km Noosa Half Marathon with yet another age group win for the season and a super fast run.  As part of his training for the Gold Coast Marathon in July, we can only expect Russ will be in good form for the upcoming Gold Coast Marathon.  Super results.

Results – 1Km Ocean Swim – Jenni Jenkins – 20 mins

5km Fun Run – Jenni Jenkins 26.42

Noosa Half Marathon 21.1km – Russ Jenkins 1:22.28

#Unipride all round.

 

Happy to attached photos if anyone has any they would like uploaded.

 

 

 

 

 



<!-- END -->