---
title: Bendigo Park Run first birthday
---

This weekend marks Park run first birthday run at Kennington reservoir for the usual 5km event, commencing at 8 sharp.  We [noted](http://www.parkrun.com.au/bendigo/results/weeklyresults/?runSeqNumber=52) how well the BUAC crew did last weekend with some pretty good runs.  You won’t see me there this week but I will be at Notleys if I can find it!  See you there.



<!-- END -->