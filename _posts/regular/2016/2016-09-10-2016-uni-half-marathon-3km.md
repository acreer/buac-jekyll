---
title: 2016 Uni Half Marathon – 3km
---

### 3km Complete

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_3km\_Complete.csv”][/table]

### 3km Open Female

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_3km\_Female\_Open.csv”][/table]

### 3km U16 Female

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_3km\_Female\_U16.csv”][/table]

### 3km U16 Male

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_3km\_Male\_U16.csv”][/table]



<!-- END -->