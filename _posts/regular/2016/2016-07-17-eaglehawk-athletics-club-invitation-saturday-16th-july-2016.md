---
title: Eaglehawk Athletics Club Invitation – Saturday 16th July 2016
---

It was a perfect day for running at Lake Neangar on Saturday with plenty of #unipride on show and some personal bests being ran.   Thanks to the Eaglehawk Athletics Club for putting on another great Invitation event for us to enjoy and well done to all participants.

On the face of it, our BUAC ladies will be big point scorers in the Macdonald Cup with Sarah Jalim 1st, Rebecca Wilkinson 2nd, Georgie Lanyon 4th, Melissa Douglas 5th, Andrea Smith 6th and Alison Cartner 7th being within the top 7 finishers in the ladies over the line in the 5km event, in that order, and Amy Holmes producing a cracking pace of 3mins 38sec per km in the 2.5km event to take out the Open Women’s event.

In the mens 5km, our big point scorers were John Robinson in 4th, Rick McCaig in 6th and Matthew Heislers in 9th place out of all finishers.  Not sure if this is going to be enough to get BUAC over the line in the Macdonald Cup.

I’m unsure of all results, but I will do a further update when results become available, and points scores in the Macdonald Cup.

If you have any photos of your own you would like uploaded, please send them my way.

[![Amy Holmes1](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Amy-Holmes1.jpg?resize=225%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Amy-Holmes1.jpg)

[![AmyHolmes 2](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/AmyHolmes-2.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/AmyHolmes-2.jpg)

[![Mel Andrean](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Mel-Andrean.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Mel-Andrean.jpg)

[![Lake neangarn](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Lake-neangarn.jpg?resize=300%2C175)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Lake-neangarn.jpg)

PDF results provided by Eaglehawk Athletics Club are on the link below.

[Results Eaglehawk Invite 16th July 2016](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Results-Eaglehawk-Invite-16th-July-2016.pdf)



<!-- END -->