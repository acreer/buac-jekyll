---
title: 17/11/2016 Results – Race 2 Flack Advisory Distance Series 1000m Handicap event
  – Tom Flood Sports Centre
author: Mel Douglass
---

17/11/2016 Fantastic to see an increased BUAC representation on the track on Thursday night at the Flack Advisory Distance Series.   I have copied the results from the race below.

Well done to Ebony Whiley, winning tonight’s race 2 of the Flack Advisory Distance Series, ahead of Isaac Everett & Jazlin Fear!  
Results: 1000 metres  
Ebony Whiley 4.00  
Isaac Everett 3.10  
Jazlin Fear 3.43…  
Melissa Douglas 3.43  
Adam Parker 2.57  
Jasmine Santley 3.48  
Lonain Burnett 2.45 ft  
Ned Buckell 3.07  
Will McCaig 2.56  
Yvette Tuohey 3.41  
Riley Osborne 2.54  
Jake Hilson 2.51  
Luke Pagham 2.45 ft  
Amy Holmes 3.26  
Jack Hickman 2.57  
Grant Findlay 3.45  
Greg Hilson 3.08  
Terry Hicks 3.16  
Declan Osborne 3.07  
Tullie Rowe 3.39  
Bridie Blake-Burrows 3.32  
Scott Baxter 3.23  
Tim Sullivan 3.42  
George Flack 4.21  
Ross Douglas 3.44  
Connor Findlay 4.22



<!-- END -->
