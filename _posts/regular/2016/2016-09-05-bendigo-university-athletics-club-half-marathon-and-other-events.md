---
title: Bendigo University Athletics Club Half Marathon and other events
---

Don’t forget to jump online and enter the events for this Saturday 10th September 2016.  Feature event is the BUAC Half Marathon, and other events on the day include a 14km, 7km, 3km and 1km.

 

[![BUAC half](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC-half.jpg?resize=282%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC-half.jpg)

[![Shashes HMn](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/Shashes-HMn.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/Shashes-HMn.jpg)

 



<!-- END -->