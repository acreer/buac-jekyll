---
title: Congratulations to BUAC’s Amy Andrews Holmes – Race 1 Flack Advisory Distance
  Series
---

Congratulations to BUAC runner [<u><span style="color: #0066cc">Amy Andrews Holmes</span></u>](https://www.facebook.com/amyandrewsholmes) who took out Race 1 of the Flack Advisory Distance Series at Tom Flood Sports Centre last night. Amy has returned to track racing after a 20 year hiatus, this time in middle distance running rather than as a 200m/400m sprinter. I’m sure Mr Handicapper will now sort you out, but well done on your win and for flying the BUAC shirt. The only picture I have, has me in it too but hopefully when results come out there is a finish photo of Amy’s win. Not exactly sure what Amy’s time was but one thing I can say is ‘it was super fast’! Go Amy!!

Congratulations also to Terry Hicks from Eaglehawk Athletic Club who took out the second track race for the night of the Flack Advisory Distance Series.  Great run Terry and super fast.

How lucky are we in Bendigo to be able to have shared the facilities at Tom Flood Sports Centre with our Cycling community for 30 years to run the Flack Advisory Distance Series, in between the bike racing.   It is always a great night of racing for all.   This certainly doesn’t happen at all track cycling facilities around the country!

[![amy-n-mel](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/AMy-n-Mel.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/AMy-n-Mel.jpg)

[![bikes](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/bikes.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/11/bikes.jpg)



<!-- END -->