---
title: Ultra Trail Run  Australia – Katoomba NSW 11-15 May 2016
---

Congratulations to our four dedicated BUAC trail runners who made the trip to Katoomba NSW for the Ultra Trail Run Australia.  
In the 100km event Ross McPhee finished in an impressive time 11hours 52mins.21 seconds.  
In the 50km event , we had three BUAC runners and some more impressive results from John Robinson in 6hours 05mins.17sec, Brian Watson 6hours 23mins 15sec, and  Craig Feuerherdt 6hours 36mins28 sec.

Looking at the photos, clearly a good time was had by all four of you.     Thanks for the photos too.  I am sure there has been lots of hard work, long km’s and dedication gone into preparing for such events.  Our club is lucky to have such high calibre trail runners who can pass on their knowledge and some tips to those new to trail running.   Well done guys.

[![13238924_10153694348838196_5770599288120740884_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13238924_10153694348838196_5770599288120740884_n.jpg?resize=300%2C169)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13238924_10153694348838196_5770599288120740884_n.jpg)[![13281875_10208339687982978_814693883_n](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13281875_10208339687982978_814693883_n.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13281875_10208339687982978_814693883_n.jpg)[![13275258_10208339687862975_1475947_o](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13275258_10208339687862975_1475947_o.jpg?resize=300%2C113)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13275258_10208339687862975_1475947_o.jpg)[![13271434_10208339688542992_1686178553_o](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13271434_10208339688542992_1686178553_o.jpg?resize=269%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13271434_10208339688542992_1686178553_o.jpg)[![13288574_183778292017622_1490658657_o](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13288574_183778292017622_1490658657_o.jpg?resize=300%2C225)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13288574_183778292017622_1490658657_o.jpg)[![13262109_10208339687942977_147536206_o](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13262109_10208339687942977_147536206_o.jpg?resize=300%2C185)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13262109_10208339687942977_147536206_o.jpg)[![13249467_10208339758944752_498093938_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13249467_10208339758944752_498093938_n.jpg?resize=169%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13249467_10208339758944752_498093938_n.jpg)

http://www.ultratrailaustralia.com.au/



<!-- END -->