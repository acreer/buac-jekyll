---
title: Uni Invitation 2016 Cross Country
---

Good morning runners, attached is our flier outlining our events for the annual Bendigo University Cross Country Invitation on Saturday 14th May, 2016. In memory of Tracy Wilson, our committee has named the 3000m junior event in his honour. The fastest junior male and fastest junior female in the 3000m will receive the new Tracy Wilson award. Should be a great day so feel free share the invitation fliers amongst your networks.

[Invitation flyerA3](http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/Invitation-flyerA3-1.pdf)[![file-page1](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/file-page1-1.jpg?resize=1%2C1)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/file-page1-1.jpg)



<!-- END -->