---
title: 2016 Uni Half Marathon – 1km
---

### 1km Complete

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_1km\_Complete.csv”][/table]

### 1km Open Female

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_1km\_Female\_Open.csv”][/table]

### 1km Open Male

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_1km\_Male\_Open.csv”][/table]

### 1km U10 Female

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_1km\_Female\_U10.csv”][/table]

### U10 Male

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_1km\_Male\_U10.csv”][/table]



<!-- END -->