---
title: Eaglehawk YMCA Athletics Club Invitation Saturday 16th July 2016 –
---

Today’s run is the Eaglehawk Athletic Club Invitation at Lake Neangar at Eagleahawk. Extensively promoted on the BUAC Facebook page. Racing kicks off at 2pm with the 5k (two laps of the 2 lakes) followed by the 2.5km at 2.30 (1 lap of the 2 lakes), a 1k race (1 lap of Neangar at 2.50), a 500m race after that and a unique age graded 1k race to wrap up the day (your time gets ranked compared to how fast a top athlete your age could run 1k (or something like that)

Entry fees apply of $5 for the 5k, 2.5k & 1K & $3 for the 500m. If you do more than 1 event the the extra races are free.

Last year the start/finish line was on the Bendigo side of Lake Neangar just up from the YMCA.

BUAC runners just have to show up and run, no need to bring your leg tag or afternoon tea or thermoses/thermo, this is an Eaglehawk Club show.

For new members as you might imagine if you know Lake Neangar this course is very flat. There are a couple of undulations but certainly nothing resembling a hill or anything that would generate more than about 2 metres elevation on your Garmin. Good courses for getting a PB.

For those seeking further motivation to go and do it, the weather forecast is for a great day. Additionally you may well earn BUAC some vital McDonald Cup points. The McDonald Cup was only started last year and BUAC won it. Lets win it again. The exact formula for calculating cup points is apparently known only by Andrew Creer of BUAC and Ross Evans of Harriers but it is something like 1<sup><span style="font-size: small">st</span></sup> for each gender gets 20 points, 2<sup><span style="font-size: small">nd</span></sup> gets 19 points right down to 1 point. Therefore first 20 in each gender score. The 3km (or middle distance) starts at about 15 points for first so first 15 people of each gender score. Last year only the long distance event only scored but the 4 clubs voted earlier in the year to include the middle distance event as well so more people could contribute to their club score. Only the long distance and middle distance events count. The 1000m is not a points scoring race. None of the points have anything to do with being AV registered. So seeing as BUAC is almost deadlocked with Harriers on points it would be great to get as many BUAC runners (particularly men because our women have been doing an awesome job) to get along and run on Saturday.

Good luck and see you out there.  Hope we don’t have to dodge flocks of Pokémon players in today’s races.

[![Eaglehawk Invite](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Eaglehawk-Invite.jpg?resize=209%2C300)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/07/Eaglehawk-Invite.jpg)

 



<!-- END -->