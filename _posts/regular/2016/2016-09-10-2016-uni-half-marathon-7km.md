---
title: 2016 Uni Half Marathon 7km
---

### Complete 7km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_7km\_Complete.csv”][/table]

### Female 40+ 7km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_7km\_Female\_40plus.csv”][/table]

### Female 60+ 7km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_7km\_Female\_60plus.csv”][/table]

### Female Open 7km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_7km\_Female\_Open.csv”][/table]

### Male 40+ 7km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_7km\_Male\_40plus.csv”][/table]

### Male 60+ 7km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_7km\_Male\_60plus.csv”][/table]



<!-- END -->