---
title: BUAC at the Launceston 10
---

 **Launceston 10km incorporating State 10km Road Championships and Oceania 10km Championships**

Launceston 10.   Billed as Australia’s Fastest and Richest 10km.  
Andy Buchanan 29.57  (9th placed), Matt Heislers 33.32.  
Super running Andy and Matt! Congratulations.

[http://www.launcestonten.com.au/details.html](http://www.launcestonten.com.au/details.html)

<!--more-->

Brady Threlfall is Bendigo Harriers runner who now lives in Echuca. He also ran well to finish 12th in a time 30:24



<!-- END -->