---
title: BUAC at Bundoora Park – AV XRC / AV All Schools XC  Saturday18 June 2016
---

Good luck to all our BUAC members and locals running at Bundoora Park today.   Incorporating the Victorian All Schools Cross Country Championships and also serving as the selection trial for the Australian Cross Country Championships, Bundoora Park hosts round 5 of the XCR’16 series with 10km, 8km, 6km, 4km and 3km races from Open and Overage down to Under 14.

Andy Buchanan running XCR 10km –

Jim Russell, Craig Green, David Heislers, Darren Hartland make up a Division 5 Men’s team with Matt Hooke (of Bendigo Harriers) running XCR 10km and going for 3 wins a row.

Tullie Rowe, Wil McCaig, Bridie Blake-Burrows, and Luke Padgeham competing in the AV All Schools event.

We await the results from todays event.



<!-- END -->