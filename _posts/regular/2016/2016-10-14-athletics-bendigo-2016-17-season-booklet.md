---
title: Athletics Bendigo 2016-17 Season Booklet
---

A few people have mentioned it is a little hard to find the Summer Season Handbook

The actual pdf file program is [http://media.wix.com/ugd/2daf1f\_8c6f9bd950b2463e8bb2263904d8f111.pdf](http://media.wix.com/ugd/2daf1f_8c6f9bd950b2463e8bb2263904d8f111.pdf)

There is a blog post [http://www.athleticsbendigo.org.au/single-post/2016/09/28/Season-Booklet—Flack-Advisory-201617-Bendigo-Region-Athletics-Season](http://www.athleticsbendigo.org.au/single-post/2016/09/28/Season-Booklet---Flack-Advisory-201617-Bendigo-Region-Athletics-Season)

Cheers  
And happy summer running.



<!-- END -->