---
title: Rifle Range Update
---

Hi All

It looks like another muddy & wet day for running tomorrow at the BUAC ‘Rifle Range Rattle’ course off Popes Road, Junorton. For those who haven’t been before, head out McIvor Highway toward Axedale, turn left into Popes Road and follow the signs. Thanks to course markers Shane Rushan and Frances Walsh. It is a fairly flat & fast course but don’t worry the Ham Street Hustle next week will compensate for this…

Despite the inclement conditions 74 runners competed last week in the 3 races with several new members including Latrobe international student Koichi Masuda,  100m track runner who hails from Tokyo and found the running surface very novel. He ran the 3k well though and please make Koichi very welcome if you see him at an event, although he said he would not be at this week’s run.

In other news President Ross has been active mid-week and joined up a further 3 new runners including Peter King from Charlton who will easily overtake Chris and Janelle Giffin from Hunter west of Elmore as the club member making the longest commute to race each weekend.

Not much else to note – keep in mind the Race Captain job is going to be reintroduced to register race entrants on the laptop and let one of the committee know if you are not happy to have your name put down to do it…..  
Check out the blog  early on Saturday evening for all the day’s results.  
Gavin Fiedler’s results write-up in the Wednesday Bendigo Advertiser continues to be informative and comprehensive.



<!-- END -->