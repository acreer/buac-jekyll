---
title: 2016 Uni Half Marathon – 14km
---

### Complete 14km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_14km\_Complete.csv”][/table]

### 40+ Female 14km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_14km\_Female\_40plus.csv”][/table]

### Open Female 14km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_14km\_Female\_Open.csv”][/table]  
[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_14km\_Male\_40plus.csv”][/table]

### Male 50+ 14km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_14km\_Male\_50plus.csv”][/table]

### Male 60+ 14km

[table file=”http://bendigouniathsclub.org.au/blog/wp-content/uploads/2016/09/BUAC\_Half\_Marathon\_14km\_Male\_60plus.csv”][/table]



<!-- END -->