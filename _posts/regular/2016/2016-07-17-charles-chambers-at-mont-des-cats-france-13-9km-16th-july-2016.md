---
title: Charles Chambers at Mont des Cats France 13.9km 16th July 2016
---

Congratulations to Charles Chambers who  enjoyed a run over in France for the Mont des Cats 13.9km and finished 2nd in his age group.  I couldn’t find much information on this event on the internet so hopefully Charles can tell us a bit more about this when he returns.  Safe travels home and well done.

Mont des Cats. France. 13.9k.  
Charles Chambers 66.42 2nd in age group category.



<!-- END -->