---
title: School Sports Victoria State XC Championships – Bundoora Park,Thursday 14th
  July 2016
---

Congratulations to all our state finals runners who ran at Bundoora Park on Thursday 14th July in the School Sports Victoria State Cross Country Championships in the 2.8km event.

Special congratulations to Tullie Rowe who has qualified in 4th place to represent Victoria in the National Cross Country Championships in Canberra on August 19th 2016.   Great work Tullie.

2.8km Run Results

Will McCaig 10.22.  
Tullie Rowe 10.40.  (4th place)  
Bridie Blake – Burrows 10.48. (8th)  
Emily Heislers 12.46.

Carissa Brook 12.13  
Beatrice Lonsdale 12.27  
Phoebe Lonsdale DNF

A fantastic effort by all.   Hope Phoebe is ok, not sure what has happened there with the DNF.

More information and full results can be found at http://tomatotiming.racetecresults.com/Results.aspx?CId=16&RId=28503



<!-- END -->