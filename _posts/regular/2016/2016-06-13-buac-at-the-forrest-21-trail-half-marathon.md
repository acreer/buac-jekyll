---
title: BUAC at the Forrest 21 Trail Half Marathon
---

 **Run Forrest Trail Half Marathon – Otway Ranges Victoria.**

Ben McDermid 2:06:16

Danny Burgess 2.09.30.

Congratulations Ben and Danny.  Looks like a beautiful place to run.

[http://www.runforest.com.au/](http://www.runforest.com.au/)

And Ben says:

Forrest is a stunning back drop for a trail run. Offering 5km (kids & adult) 10km & 1/2 Marathon options. Small family run atmosphere. This is a must do trail run. Totally awesome essentially out and back course on groomed mountain bike / walking tracks, with sections of clay track creating an additional challenge of skating both up and down sections. With around 600m of ascending & descending the quads get a great workout & will be sure to make stairs a challenge for a few days post the event.

I’ll be back next year with both boys already declaring they’re running the 5km kids event!



<!-- END -->