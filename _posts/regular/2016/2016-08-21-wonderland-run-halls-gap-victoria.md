---
title: Wonderland Run – Halls Gap Victoria
---

# 36 Km

Ross McPhee 3.35.42,1st in cat  
John Robinson 3.42.16 3rd in cat.  
Craig Feuerherdt 3.47.45,  
Simon Austin 4.25.56  
Louise Shambrook 5.27.23,2nd in cat,  
Maree Platt 5.57.32

# 20 Km

Andrea Smith 2.52.44,  
Jac Tremayne 2.56.32,3rd in cat,  
Trinity Sanderson 3.00.13,  
Beth Lavery 3.04.14  
Leah Cripps 3.43.43,  
Lou Bray 3.58.32  
Aida Escall 3.35.48  
Helen Lees 3.40.58  
Sue Taylor 3.40.58,  
Vicki Lane 3.50.25,

# 8 Km

David Cripps 37.04  
Stephen Wainwright 40.06  
Megan McDonald 53.45

# 2 Km

Harry Cripps 16.55,  
Tully Cripps 17.09  
David Cripps 17.09  
Leah Cripps 20.27  
Poppy Wainwright 17.03  
April Wainwright 20.28  
Andrea Smith 20.27.



<!-- END -->