---
title: BUAC at Loddon Mallee Region Schools XC
---

 **31 May 2016  Loddon Mallee Region School Sports XC at St Arnaud**   Anyone missed can be added on.

**5km**.Matt Heislers1st 14.47,Tom Barkmeyer 2nd 15.12.  Hugh Schaeche 4th 15:44 (all state qualifiers)  
**3k**.Bridie Blake-Burrows 1st 12.01 (state qualifier),Tullie Rowe 1st 12.24 (state qualifier),Emily Heislers 13.46,Will McCaig 10.59 (State qualifier – team),Beatrice Lonsdale 13.17,Carissa Brook 13.34,Floyd Cartner 12.43,Phoebe A’Beckett 14.45,Phoebe Lonsdale 14.06.  
**2k.** Connor Bedson 9.22

Fantastic results and congratulations to all runners.  Stay tuned for later updates on the five runners qualifying as an individual or a team for the SSV State Finals in July, 2016.



<!-- END -->