---
title: Club Update Sandhurst Slog 2016
---

Hi BUAC members, past members, maybe future members and cross country running fans.

Another interclub event done and dusted last weekend and another haul of McDonald cup points for BUAC. We are now holding a 37 point lead over Harriers on the tally board so well done to all those who have participated so far. See the attached points summary for further details. Get well soon to Brad Russell who blew up his achilles and came back to the startline in a car

Well done also to the gang at Mt Macedon last weekend with a number of high race placing and personal milestones achieved. Also to all the junior BUAC members who competed at St. Arnaud on Tuesday just gone in the SSV cross country with a number now qualified for the state titles. More details in coming weeks.

This week is a normal club competition at the ‘Sandhurst Slog’ course with the race starting at the usual place at the corner of Kangaroo Gully Road at Read lane, Kangaroo Flat. Easiest way to find it for new members is get yourself to the east side of the railway line at Kangaroo Flat railway station and just head south onto Kangaroo Gully Road. 3km from the station you will find the start point and the eager BUAC throng milling around. 7 km long race Strava map is at the bottom of this email.

This week there are a few other general business notes.  
Handicapper Andrew Creer says he is always keen on any particular weekend to hear from people what distance their Garmin/Strava/Tomtom says the race course was so he can further refine his handicapping spreadsheet. Feel free to badger him about this at any time and even send him emails he says.  
Timelord Darren Rowe has had a couple of people finish racing recently (mostly kids I think) without running through the finish line which obviously switches off the clock as far as your racing time goes so just a reminder to be alert to that.  
Darren and the committee are keen to hear from anyone who might be interested in learning more about the timing system. Darren has it refined to the point now we will reintroduce a ‘race captain’ to take registrations each Saturday on the laptop. More on that next week.  
Photos on the website/blog. Keen members have been posting photos more often this year. If anyone has any problems with a photo of themselves or their family being on the website or blog please let one of the committee know. We assume BUAC Facebook is OK given it is a closed group.

Also racing this Saturday in the Athletics Victoria XCR series around Lake Wendouree are the speedy new BUAC members Craig Green and Darren Hartland & maybe also Andy B so good luck to that crew.

Good running Saturday.

Your dedicated BUAC committee.  
President Ross Douglas  
VP Jenny Lee  
Treasurer Ben McDermid  
Timelord Darren Rowe  
Stats Andrew Creer  
Secretary David Lonsdale



<!-- END -->