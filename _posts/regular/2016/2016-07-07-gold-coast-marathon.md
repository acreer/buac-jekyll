---
title: Gold Coast Marathon
---

Congratulations to all our Gold Coast runners in various events at the Gold Coast Marathon Festival.  Conditions were picture perfect according to my insider source and favorable to fast running, with many running a PB.

Andy Buchanan certainly put #unipride on the map with his second placing by a mere 2 seconds in the elite field in the 10km (and running under 3 minute kilometres=reasonably quick) .  Congratulations Andy, so close.

In the Marathon Russ Jenkins claimed a calf injury 22km in and soldiered on in fine form to run a 3 hour marathon.   Lots of great achievements to mention everyone, but well done to all

**Marathon** – Luke Crameri 2.58, Russ Jenkins 3.00.35, John Flood 3.18.13, Alan Buchanan 3.28.26, Jenny Buchanan 3.41.13, Brock Prime 3.55.56.

**Half Marathon**. Peter King 1.28.08, Lyndsey Bish 1.51.21, Jenna Sing 2.04.43.

**10k**. Andy Buchanan 29.25 2nd overall, Tom Barkmeyer 35.35, Lee McCullough 38.55, Melissa Douglas 44.52.

**5.7k.** Jenni Jenkins 29.08, Ruby Barkmeyer.32.20.

{I would be remiss if I didn’t tell everyone that Mel’s 10k time was very very quick, as fast as she has run in twenty years!}



<!-- END -->