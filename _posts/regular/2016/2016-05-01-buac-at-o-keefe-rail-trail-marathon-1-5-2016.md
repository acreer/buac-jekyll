---
title: BUAC at O’Keefe Rail Trail Marathon 1/5/2016
---

Congratulations to all our BUAC runners who ran in the first O’Keefe Rail Trail Marathon and associated events in cold, wet and gusty conditions.

Marathon.  
David Meade-2.53.25 3rd overall,1st age group. Andrew Creer-2.59.52.  Sarah Jalim-3.12.26.1st female. Els Viester-3.30.11.2nd female. Matt Holmes-3.47.34. Amy Holmes-3.51.21.3rd female. Aaron Anderson-3.58.44. Craig Feuerherdt-3.58.44.did the Race Pacer job. Alex Tranter-5.44.33.

Half Marathon.  
Paul Seaward-1.34.57. Lee Bray-1.36.52. Kevin Walsh-1.43.14. Andrea Smith-1.47.58. George Thomas-1.57.54. Peter Noble-1.58.18. Kevin deVries-1.59.21.

Quarter Marathon.  
Matt Heislers-37.12.1st. Gavin Fiedler-50.57. Melissa Douglas-51.11.3rd a/g . Ross Douglas-51.53. Troy Davis-57.21.

Mile.  
Tim Creer-7.08.2nd a/g. Maddy Bray-9.36.2nd a/g. Mitch Bray-9.39.

Thank you also to the race organisers and helpers for such a fantastic and well organised event.

Full results for all events can be found at:  
[http://tomatotiming.racetecresults.com/Results.aspx?CId=16&RId=28475&EId=3](http://tomatotiming.racetecresults.com/Results.aspx?CId=16&RId=28475&EId=3)



<!-- END -->