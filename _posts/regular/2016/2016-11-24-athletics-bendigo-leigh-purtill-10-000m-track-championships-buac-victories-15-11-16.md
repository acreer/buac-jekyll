---
title: Athletics Bendigo “Leigh Purtill” 10,000m Track Championships – BUAC Victories
  15/11/16
author: Mel Douglass
---

Runners were greeted with perfect conditions at the track on Tuesday 15/11/16 for the Athletics Bendigo “Leigh Purtill”  10,000m Track Championships.   The usual 3000m event was also ran at the same time for those not wanting to complete 25 laps of a running track.   BUAC runners made up one third of the runners in these events which is fantastic for our club.

Craig Green has prepared a great report on the Athletics Bendigo Website and Facebook page with all the details, click on the link below. #unipride

[http://www.athleticsbendigo.org.au/single-post/2016/11/16/Veteran-Uni-pair-cruise-to-victory-in-10km](http://www.athleticsbendigo.org.au/single-post/2016/11/16/Veteran-Uni-pair-cruise-to-victory-in-10km)

Report in the Bendigo Advertiser as well.

[http://www.bendigoadvertiser.com.au/story/4297052/creer-salutes-over-10000m/](http://www.bendigoadvertiser.com.au/story/4297052/creer-salutes-over-10000m/)



<!-- END -->
