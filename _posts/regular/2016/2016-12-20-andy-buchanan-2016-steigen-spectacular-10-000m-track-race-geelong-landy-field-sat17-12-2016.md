---
title: Andy Buchanan – 2016 Steigen Spectacular 10,000m track race Geelong (Landy
  Field)  Sat17-12-2016
author: Mel Douglass
---

Congratulations to BUAC’s Andy Buchanan who has finished 4th in the Steigen 10,000m track race on Saturday, running a personal best time of 29:17:03.   Andy is now the new Athletics Bendigo – Residential Record holder in the mens 10,000m track event.  This record was previously set by Brady Threlfall of Bendigo Harriers in 2012 with a time of 30:27:43 at Landy Field.

The Steigen 10,000m race is one of the premier long distance races in Victoria each year for Men.

For further reading, Craig Green has prepared a great report which is available of the Athletics Bendigo Webpage and Facebook Page.  Link here  [http://www.athleticsbendigo.org.au/single-post/2016/12/19/Bendigo-distances-athletes-mixing-it-with-the-states-best-with-new-records](http://www.athleticsbendigo.org.au/single-post/2016/12/19/Bendigo-distances-athletes-mixing-it-with-the-states-best-with-new-records)

Results from the Steigen 10km are available on the Atheltics Victoria Webpage.  Link here [http://www.grcc.net.au/steigen\_spectacular\_2016\_results.html](http://www.grcc.net.au/steigen_spectacular_2016_results.html)

Great work Andy.

[![](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/12/Andy-B-1.jpg?resize=300%2C200)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/12/Andy-B-1.jpg)



<!-- END -->
