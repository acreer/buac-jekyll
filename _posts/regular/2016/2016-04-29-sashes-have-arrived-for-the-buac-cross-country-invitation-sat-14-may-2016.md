---
title: Sashes have arrived for the BUAC Cross Country Invitation, Sat 14 May 2016
---

[![100_4347](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4347.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4347.jpg)[![100_4349](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4349.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4349.jpg)[![100_4357](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4357.jpg?resize=225%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4357.jpg)[![100_4344](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4344.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4344.jpg)

[![100_4343](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4343.jpg?resize=300%2C225)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/04/100_4343.jpg)

Sashes have arrived for the BUAC Cross Country invitation on Saturday 14th May and will be awarded to the following:  
Fastest Male and Fastest Female in the 7.5km  
Fastest Junior Male and Fastest Junior Female in the Tracy Wilson Memorial 3km  
Fastest Boy and Fastest Girl in the 1km  
Large ribbons for Age Group Winners in all events. …  
We also will have lots of other prizes on offer on the day, including some cash prizes. Got to be in it to win it! Should be a great day.



<!-- END -->