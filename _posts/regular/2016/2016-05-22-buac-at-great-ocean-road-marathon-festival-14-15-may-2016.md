---
title: BUAC at Great Ocean Road Marathon Festival 14-15 May 2016
---

Congratulations to all our BUAC runners who competed at Great Ocean Road Marathon Festival.  BUAC had 12 runners lining up for different events at Great Ocean Road.  Conditions on the Saturday were perfect, even slightly warm for running.   Russ Jenkins was in his usual fine form in the 14km event, placing 2nd in the 50-54 mens category and 19th overall with an average pace of 3mins 47sec.

The Kids 1.6km Gallop always attracts a large fields of kids of all ages, ones in prams, mums and dads.  Each child gets a medal for finishing which always makes them happy.

On Sunday things started off pretty much well perfect, blue clear sky, calm sea until about 14km into the half marathon and marathon.  The swell from the ocean certainly picked up, the wind change direction and picked up, then later came the rain 2 hours in.  Those in the marathon copped more of the strong winds and rain than those of us in the half.

Alan Buchanan and Jenny Buchanan lined up for their 3rd Marathon for the year, with their last one being only 4 weeks ago at Wangaratta.  Both had impressive runs yet again over the 45km distance, with Jenny placing second in her age group (55-59 women) and Alan placing third in his age group (60-64 male).

In the 23km half.  Trinity Sanderson had a great run this year, getting her PB, being the fastest amoungst us BUAC runners and 5th in the 40-45 womens category.  Great work Trinity.  Beth Lavery also had a great run getting her PB too.   Ross and I ran together for 13km, then somewhere I lost Ross and kept going. Then I ran with former BUAC (College) veteran runner Brian McCarthy, whom I hadn’t seen in years.  Much to my surprise, I finished 6th in the 40-45 womens category.  Not too far behind me, Jay Sanderson had a great run.  Ross was happy to finish and get his finishers medal,  being his first 23km event at Great Ocean Road.  Well done Rossco.  Nekti Tzouroutis joined us for our training runs so I have included Nekti in the BUAC results.

Overall a great weekend was had.  I was able to meet Steve Moneghetti, who is Russell Jenkins’ friend and coach.  Thanks Russ!  I also met Liam Adams , a top Australian Marathon runner who has qualified for the Olympic Marathon in Rio.

Some photos to come, and results are below.

Results 45km Marathon:  
Alan Buchanan 3:48:29 3rd in age group,  
Jenny Buchanan 4:04:09 2nd in age group.

23km Half Marathon:  
Brian McCarthy 1:56:00,  
Beth Lavery 2:01:29,  
Ross Douglas 2:05:39,  
Melissa Douglas 1:56:15 6th in age group,  
Trinity Sanderson 1:52:41 5th in age group,  
Jay Sanderson 1:57:16,  
Nekti Tzouroutis 2:02:03.

14km Results  
Russell Jenkins 53:07, 2nd in age group

Kids 1.6km fun run / walk  
Olivia Douglas 9:42  
Ruby Douglas 14:16

Well done to all.

 

http://www.bendigoadvertiser.com.au/story/3901621/road-test-for-club-athletes/

[![13179283_10154161972522453_8290214461965044162_n](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13179283_10154161972522453_8290214461965044162_n.jpg?resize=180%2C300)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13179283_10154161972522453_8290214461965044162_n.jpg)

[![13178046_10154159284852453_3757413598475795081_n](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13178046_10154159284852453_3757413598475795081_n.jpg?resize=300%2C180)](https://i2.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13178046_10154159284852453_3757413598475795081_n.jpg)

[![13217023_2028716677352954_606023775570329666_o](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13217023_2028716677352954_606023775570329666_o.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13217023_2028716677352954_606023775570329666_o.jpg) [![13240053_602654873235478_8125910406159927200_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13240053_602654873235478_8125910406159927200_n.jpg?resize=169%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13240053_602654873235478_8125910406159927200_n.jpg) [![13245503_10209654587109401_6155036111607600141_n](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13245503_10209654587109401_6155036111607600141_n.jpg?resize=300%2C225)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13245503_10209654587109401_6155036111607600141_n.jpg) [![13254733_603254823175483_4513778203446388189_o](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13254733_603254823175483_4513778203446388189_o.jpg?resize=300%2C200)](https://i1.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13254733_603254823175483_4513778203446388189_o.jpg)[![13198534_10154313768184155_1886726041626769817_o](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13198534_10154313768184155_1886726041626769817_o.jpg?resize=224%2C300)](https://i0.wp.com/bendigouniathsclub.org.au/blog/wp-content/uploads/2016/05/13198534_10154313768184155_1886726041626769817_o.jpg)



<!-- END -->