---
title: Race 3 Flack Advisory Distance Series 1600m at Tom Flood – at the bikes!  Thursday
  1st December 2016
author: Mel Douglass
---

Congratulations to BUAC’s young gun Will McCaig who took out 1st place in the 1600m event of the Flack Advisory Distance Series in a time of 4mins 57sec.  Congratulations also to Russ Jenkins who made a grand debut to professional track racing earning himself $5 for his 3rd placing and running a time of 5mins 36sec.

Flack Advisory Distance Series Race 3, 1600 metres: Results  
Will McCaig 4.57  
Michael Preece 4.36 F/T  
Russell Jenkins 5.36  
Craig Green 5.18…  
Oscar Fox 6.00  
Scott Baxter 5.42  
Jacob Nolan 4.59  
Rick Ermel 6.01  
Isaac Everett 5.33  
Melissa Douglas 6.15  
Connor Findlay 6.26  
Gabrielle Rusbridge 6.11  
Aaron Norton 6.22  
John Justice 7.30  
Yvette Tuohey 6.24  
Ross Douglas 6.34  
George Flack 7.30  
Tim Sullivan 6.58  
Chris Panayi 7.17

Next race is over 1000 metres on December 22nd 2016!

Will post some photos next time I get a spare 5 minutes.



<!-- END -->
