---
title: Timing System Set to Go
---

After missing out on a community grant for a 2nd time, Club treasurer Harry Terry wasn’t going to let that stop us. He tracked down a Melbourne based company called Race | Result who had exactly what we needed for almost half the price of our original option.

Suffice to say that we now have an electronic system with tags, software and a new laptop. We hope to test the system through the summer season and be ready for the Masters Championships in January.

The committee is hoping the cost of permanent tags will be able to be absorbed into membership fees with a charge for disposable bibs that covers casual race fees.

The introduction of an electronic timing system firmly stamps BUAC as the most progressive Athletics Club in Bendigo. Get excited!



<!-- END -->