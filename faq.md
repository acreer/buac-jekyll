---
title: FAQ for new members
sidebar:
  nav: admin
---

Club Member Handbook
: Yes [here]({% link assets/files/2019/BUAC-Member-Handbook.pdf %})

Is there a weekly club email update
: Yes. And you can subscribe by adding your email addess and name
[here]({% link email-signup.md %})

How do I become a member?
: Use the [BUAC Online Registration System]({{site.membershipurl}})

Do I have to register with Athletics Victoria?
: Only if you think you may compete in AV events in either the
winter XCR program or the summer track season. If you join
AV and nominate Uni as your club, we will reimburse you
for your club membership.

Do I have to become a club member?
: No, you can choose to pay a \$10/race casual race fee. This is a good option
if you think you may only make it to a few races in the season. If you are
considering joining, then your first race is free. Casual run payments will
contribute to the cost of membership, but a completed membership form
([online]({{site.membershipurl}}) or [paper]({{site.membershippdf}})) is
required.
  
What is the routine at a club run?
: Head to the registration table to get your name marked off by the race
captain. The captain will give you your start time. If you're a new runner, the
captain will write in your name, help you with an estimated handicap, and issue
a bib or tag. Warm up before your start time with a short run and some
stretches. Listen to the course description by the course marker. Wait for the
starter to call you to the line and count you down to your start. Follow the on
course signs to the finish. Recover with a warm down, sretches and a cool
drink. Enjoy some afternoon tea with the other runners and presentation of
ribbons for the day's races.

When does my run start?
: The long run starts at 2pm. The 1 km run starts after the last
runner in the long run finishes (usually around 3:15pm).
The intermediate race is the last event on the bill after
the finish of the intermediate race (around 3:30pm).

What do race winners get?  
: Ribbons are awarded to 1st, 2nd and 3rd placegetters each race.
The first three place getters in the club champion award
in each distance category are awarded trophies or medals
at the end of the year. The speedchampion in the long distance
event also wins a trophy. The President's award goes to
the club member who is deemed to have made the biggest
contribution to the club for the year.

How is my handicap calculated?  
: New members are asked for an estimate of their per km pace at
their first race. This is used to calculate the start time
but the first race for a runner is not counted in the club
champion award. The actual per km pace recorded will help
the handicapper establish a more accurate handicap for
the next race. Each race will help identify individual
trends which will suggest what adjustment to make on the
handicapper's sheet. Ribbon winners are normally handicapped
more rigorously than others for the following race.
[club champs history]({% link aggregates.md %})
[current handicaps]({% link handicaps.md %})

How are Championship Points calculated?  
: Check out the [Scoring System]({% link aggregatesScoring.md %})
for how points are allocated in both the Club Championship and the Speed Championship.

What should I bring to a race?
: Water and cordial is available but bring an electrolyte drink
if you prefer. If you intend to stay for afternoon tea,
bring something to share like a packet of biscuits, cake
or slice. Home baked goodies are much appreciated! Tea
bags and instant coffee is available if you bring a thermos
of hot water.

What should I wear?  
: Some days can be quite cold so make sure you have tracksuit
pants and a warm top to wear before and after your run.
Club hoodies are an excellent way to stay warm. The club
encourages you to wear a club singlet or T-shirt but we
won't stop you running if you haven't got one. Your running
shoes should be suited to cross country terrain.

What are the track surfaces like?
: Most courses are undulating dirt tracks but some include road
running on bitumen. Be prepared for eroded, rocky surfaces
and wet weather conditions. While all courses are safe,
most include hills and some concentration is required to
avoid tripping. Road crossings are kept to a minimum but
are a part of some long courses.

What if I break down or get injured during a run?
: Injuries or illness can sometimes happen during a run. If
you can't make it back to the start/finish line, sit tight
and wait - the club will send help after the race is over.
If other runners don't notify the timekeeper of your situation,
your absence will be noted at the finish line. If you make
it back under your own steam you can notify the timer to
put you down as a DNF (did not finish) so your handicap
is not affected.

Want to do some training during the week?
: Great! There’s a host of informal and formal weekly running
training groups. No matter what your running passion or level of ability is
there will be like-minded people for you to link up with in our club
community. Most are free or at low cost.

What are my responsibilies as a club member?
: Any sporting club, there are tasks that need doing from
time to time. Each run needs a race captain, a timekeeper
and a course marker. Volunteers are sometimes needed for
marshalling duties at Athletic Victoria or Athletics Bendigo
events. The University Invitation relies on our members
to pitch in and help out for registrations, BBQ and the
afternoon tea stall. Each week, we need somebody to simply
take home the coffee cups and wash them for the following
race. And of course, the club committee needs volunteers
to fill places at the beginning of each year.
Responsibilities are not huge or compulsory but
sharing the load ensures that everybody can enjoy their
involvement, help improve the club, and encourage more
members. Talk to a club committee member about how you
can be involved.

What club policy documents have been created.
: There are a few on the [policies]({% link policy.md %}) page.

<ul>
{% for policy in site.data.policies %}
 <li><a href="{{site.baseurl}}/{{policy.href}}">{{policy.title}}</a></li>
{% endfor %} 
</ul>
If you have
suggestions or comments please [contact]({% link about.md %}) a commitee
member.
