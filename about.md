--- 
title: About the Bendigo University Athletics Club
header:
  overlay_image: assets/promotionBanners/uniHalfRunners.png
  title: Online Membership
  excerpt: Membership year starts on March 1
  cta_label: "Join or Renew Now"
  cta_url:  https://www.eventbrite.com.au/e/bendigo-university-ac-20202021-season-memberships-tickets-127654416797
  overlay_color: white

sidebar:
  nav: admin
---

We are Bendigo University Athletics Club (BUAC). Established in 1967, we are
Bendigo’s biggest running club. [The
president](mailto:president@bendigouniathsclub.org.au) is always ready to help
new members settle in and enjoy their running.

We have a [Member Handbook]({%link  assets/files/2019/BUAC-Member-Handbook.pdf %})


The [Summer Track]({%link summerTrack.md %}) Season gets under way
around the time of Daylight Savings in October. Many Members compete in these events

In Winter, from March to September, we host timed, family-friendly running
races in the bushland surrounding Bendigo. Races are on most Saturdays from 2pm
over 4 distances.  500m for the littlies, 1km, 3km and our longer race averages
7km.  Our winter handicap system enables every participant the chance to win
the race. (Yes, that includes you.) 


# Want to learn more? 

Visit our 
[Frequently Asked Questions]({%link  faq.md %}) 
page or [email us at {{site.author.email}}](mailto:{{site.author.email}}) 

Sign up for the [official BUAC mailing list]({%link  email-signup.md %})

# Try us out!

New members are always welcome.

In the Summer from October to March, the club is part of a larger more formal [Athletics
Bendigo](https://www.athleticsbendigo.org.au/) track competition.   If you
contact the [the president](mailto:president@bendigouniathsclub.org.au)
arrangements can be quickly made to get you running in a uni singlet.

In winter your first run is free.  Our club races are a very informal affair,
just come along to one of our runs, bring your running gear and any family or
friends who might also be interested.  Check the calandar for [event
details]({%link  runs.md %}) and turn up on the day.

# Ready to join?

Visit the 
[ONLINE registration system]({{site.membershipurl}})
allowing you to sign up and pay with your credit card.  This is the preferred
method, easing the load on the committee, and smoothing the process during the
first few runs of the winter season.

It is still possible to download the 
[registration form]({{site.membershippdf}}) 
print it out, sign it and pay in the traditional ways.  Please note: Very
traditional ways.  There will be no EFTPOS machines at the early runs.

Contact [the president](mailto:president@bendigouniathsclub.org.au) or one of
the committee.

# Want to do some training during the week?  

Great! There’s a host of informal and formal weekly 
[training]({%link  training.md %}) groups. No matter what your
running passion or level of ability is, there will most likeley be like-minded
people for you to link up with in our club community. Most are free or at low
cost, and run all year round.

# Program for the fridge?

Download and print the [pdf program]({{site.programpdf}}), or bookmark [the
runs page]({%link  runs.md %}) on your smart phone.  Or print
out the 
 [A3 version]({{site.programpdf | replace: "A4", "A3" }}) 
to put up at work or school!


# Contacts

[Email](mailto:{{site.author.email}}) or phone a committee member.

| Name           | Position          | Contact      |
|----------------|-------------------|--------------|
| Ross Douglas   | President         | 0418 322 244 |
| Nigel Preston  | Vice President    | 0412 692 468 |
| David Lonsdale | Secretary         | 0429 944 009 |
| Ben McDermid   | Treasurer         | 0428 164 481 |
| Gavin Fiedler  | Publicity Officer | 03 5443 1714 |
| Janelle Giffin | Handicapper       | 0417 885 339 |
| Chris Giffin   | Handicapper       | 0429 171 049 |
| Andrew Creer   | Web Site          | 0431 831 479 |
| Jenny Lee      | Timekeeper        | 0410 448 245 |
| Andrea Smith   | Committee Member  |              |
| Darren Rowe    | Committee Member  | 0418 505 955 |
| David Heislers | Committee Member  | 0439 654 066 |
|================|===================|==============|

