---
isCurrent: false
date: Aug 9 2020 7:50am
title: City to Surf
type: other 
distances: 42.2 21.1 10
location: 
  text: Sydney
  href: https://city2surf.com.au/
setter:
  text: City to Surf
  href: https://city2surf.com.au/
--- 

History tells us that the inspiration for this great event came via a note from
Fairfax’s US correspondent, who sent a newspaper clipping in 1970 about the San
Francisco Bay to Breakers to the editor of The Sun newspaper, Jack Tier. From
that internal memorandum The Sun City2Surf was born, albeit humbly, in 1971,
with just over 1,500 entrants (only 2% of the field was female).

In the years since it has been proven time and time again that no other fun run
in the Southern Hemisphere matches the City2Surf in terms of organisation or
demographics.

For the first time in the event’s history women outnumbered men in the capacity
field of 63,451 in 2006: it is truly a community event attracting entrants from
all walks of life and all ages. Originally a 15km course, the 14km course of
today has now become one of the most loved and largest community events in the
world.

The reputation of this uniquely Sydney event has grown year on year and the
event is widely recognised as a ‘bucket-list’ item for serious runners and
adventure lovers around the world. The Telegraph listed the event as one of the
16 Great Running Races Around the World and Running Shoes Guru lists the event
as one of the Top Ten Running Races In The World. In each of these lists, the
City2Surf was the only Australian event to be featured.
