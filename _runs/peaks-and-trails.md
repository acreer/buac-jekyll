---
isCurrent: false
date: Aug 9 2020 7:50am
title: Peaks and Trails
type: other 
distances: 6.4 11 22 26 50
location: 
  text: Dunkeld, Grampians
  href: https://peaksandtrails.com.au/
setter:
  text: Peaks and Trails
  href: https://peaksandtrails.com.au/
--- 

Peaks and Trails is a challenging trail running event, starting and finishing
in the heart of picturesque Dunkeld.

The weekend offers something for all levels of trail enthusiast with 6 distance
options and the FREE Salt Creek Kids Dash.

All distances include private trails normally inaccessible to the public and
take in the beautiful Southern Grampians landscape. The longer run options also
include trails within Grampians National Park and cover sections of the newly
established Grampians Peaks Trail.  The three most Southern peaks, Mt Sturgeon,
Mt Picaninny and Mt Abrupt offer runners postcard views of this stunning area.


