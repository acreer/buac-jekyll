---
isCurrent: false
date: Sep 13 2020 17:30
title: Presentation Night
layout: single
type: social 
location: To Be Announced
distances: Taxi is easiest
setter: 
  text: The Committee
  href: http://www.bendigouniathsclub.org.au/ac/about.html#contacts
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Always a great night out to celebrate the years efforts with fellow club
members in a family friendly setting. 

Many more details coming soon.


