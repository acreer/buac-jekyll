---
isCurrent: false
date: Jul 18 2020 14:00
title: Crusoe Crusade
layout: single
type: Club Run
location:
  text: Crusoe Number 7 Park
  href: https://www.google.com/maps/dir//-36.8322778,144.2318889/@-36.8322054,144.2296746,17z?hl=en
distances: 1km, 3km, and 8km
setter: 
  text: TBA
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

Kangaroo Flat
: From town, continue past Centro Lansell on High St. Turn right into Furness
St. (after Harvey Norman) then take the first left into Granter St. Travel
1.3km looking out for BUAC signs
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}


## [Virtual Results](https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020) | [Virual Guidelines]({% link covid-spring.md %})

### Long Course Map

<iframe name="plotaroute_map_391138" src="https://www.plotaroute.com/embedmap/391138" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/391138" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Crusoe Crusade Long Course on plotaroute.com</a></p>


### Medium Course Map

<iframe name="plotaroute_map_403033" src="https://www.plotaroute.com/embedmap/403033" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/403033" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Crusoe Crusade Medium Course on plotaroute.com</a></p>


    {% include run-summary-table yaml=site.data.csv2jekyll.crusoe-crusade %}
