---
isCurrent: false
date: Aug 22 2020 
title: Australian Cross Country Champs
type: other 
distances: 10k
location: 
  text: To Be Announced
  href: https://www.athletics.com.au/aa/events/
setter:
  text: Athetics Australia
  href: https://peaksandtrails.com.au/
--- 

The peak event of Australian Cross Country running.  

Has been won [twice](https://www.runnerstribe.com/latest-news/victorias-andrew-buchanan-and-madeline-hills-win-aussie-xc-championships/) by our very own Andy Buchanan

