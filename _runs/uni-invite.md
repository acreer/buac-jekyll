---
isCurrent: false
date: May 2 2020 14:00
title: Bendigo University AC Invite 
type: Invitation
location:
  text: Student Union, La Trobe University
  href: https://www.google.com/maps/dir//-36.7795056,144.3011185/@-36.7791035,144.3006702,303m/data=!3m1!1e3!4m2!4m1!3e0?hl=en
distances: 1km, 3km and 7.5km
setter:
  - text: Shayne Rushan
  - text: Gavin Fiedler 
  - text: David Heislers
    href: https://www.strava.com/athletes/2613270
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Strathdale
: La Trobe University Bendigo Campus. Meet outside Student Union building. Google Maps link 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

 {% include run-summary-table handicap=false yaml=site.data.csv2jekyll.uni-invite noHandicap=true %}

### Long Course Map
<iframe name="plotaroute_map_404332" src="https://www.plotaroute.com/embedmap/404332" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/404332" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Invitation Long Course on plotaroute.com</a></p>
