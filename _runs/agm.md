---
isCurrent: false
date: Oct 23 2019 19:00
title: Annual General Meeting
location: 
  text: Bendigo Club Woodbury Room
  href: http://www.bendigoclub.com.au/
distances: Bendigo Club Woodbury Room
setter: 
  text: The Committee
  href: http://www.bendigouniathsclub.org.au/ac/about.html#contacts
type: social
---

The AGM is your chance to get involved in the organisation of the club.
[{{page.location.text}}]({{page.location.href}}) on {{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}


Drinks and meals are available - see attached menu.
The pre/during meal option has been quite popular the past few years.

Whilst the main purpose is to elect the office bearers for the next 12 months you can also have a say in such weighty issues as:-

- Which runs are good runs? Which not so good?
- Suggest some new runs.
- Order of club runs.
- Runs in the school holidays?
- How many runs do we need?
- Do we need more 15k runs or more 5k runs?
- When should the season start/end/break in the middle?

Or go the whole hog and organise a coup!

All welcome. See you there!


