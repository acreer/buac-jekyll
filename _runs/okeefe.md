---
isCurrent: false
title: Okeefe Trail Marathon 
date: Apr 19 2020 7:00
type: ab-run
location:
    text: Junortoun to Heathcote
    href: https://www.theokeefe.com.au/run/
setter:
    text: https://www.theokeefe.com.au/
    href: https://www.theokeefe.com.au/
distances: 5 10 21 42.2
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Follow the spectacular O’Keefe Rail Trail from Bendigo to Heathcote, as it weaves its way through bushland, open fields, farms and past Lake Eppalock.

The O’Keefe Challenge is more than just a race, fun run or bike ride – it celebrates fitness, nature and the community spirit of the Heathcote and Bendigo region.



{{ page.title}}
: [{{page.location.text}}]({{page.location.href}})

Course Setter
:{% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}


