---
isCurrent: false
date: June 20 2020 14:00
title: Eaglehawk AC Invitation
layout: single
type: Invitation
location: 
  text: Bendigo Showgrounds
  href: https://www.google.com/maps/dir//Bendigo+Showgrounds,+North+Bendigo+VIC/@-36.7371635,144.2012155,12z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x6ad759ce38f4a24b:0x9fd93d8e0c2720fb!2m2!1d144.2712559!2d-36.7373216!3e2
distances: 1km, 2.5km, and 5km
setter: 
  text: Eaglehawk AC
  href: http://athsvic.org.au/club/eaglehawk-athletic-club/
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Eaglehawk
: Parking and Race Start
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

The first race starts at 2pm so please arrive earlier to register. Entry fees apply. 

2019 prices: 18 yrs and over $6, U18 $3, Age graded 1k $3

There is usually a raffle and afternoon tea for a small charge.

This is a point scoring event for the Keith MacDonald XC Club Cup so it would be great to see lots of our members running.

BUAC runners are not required to wear your leg tag (timing chip). BUAC runners please wear club uniform at this event.
