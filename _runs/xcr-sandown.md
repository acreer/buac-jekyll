---
title: XCR-6 Sandown Road Relays
date: 2020-07-11
type: AV 
location: 
  text: Sandown Racecourse
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

Home to a number of motorsport events, Sandown Racecourse is the second relay
event for XCR’20. Held on the notoriously fast motor racing circuit, Sandown is
a venue where you can test your speed with either one or two laps of the 3.1km
circuit. So whether your testing the gears through the chicanes or going full
throttle on the main straight, Sandown will satisfy your need for speed.

