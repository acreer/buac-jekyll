---
date: July 8 2017 14:00
title: AB Combined Mandurang
layout: single
type: ab-run
location:
  text: Nankervis Rd & Fadersons Ln, Mandurang
  href: https://www.google.com.au/maps/dir//-36.8270351,144.3037764/@-36.8269364,144.3048707,18z
distances: 1km, 4km, and 7.5km
setter:
  text: BUAC
  href: https://www.strava.com/clubs/119244
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Mandurang Cricket Ground
: Travel South out Mandurang Rd, continue on to Faderson's Ln and over
Nankervis Rd. Turn right into cricket ground. See google map 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

### Start Location Map
<iframe src="https://www.google.com/maps/embed?pb=!1m17!1m8!1m3!1d1596.824368232262!2d144.3048707235123!3d-36.82693635701918!3m2!1i1024!2i768!4f13.1!4m6!3e6!4m0!4m3!3m2!1d-36.827035099999996!2d144.3037764!5e0!3m2!1sen!2sau!4v1493635187922" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
