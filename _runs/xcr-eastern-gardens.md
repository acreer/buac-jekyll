---
title: XCR-2 Eastern Gardens
date: 2020-05-09 
type: AV 
location: 
  text: Eastern Park, Geelong
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

Eastern Park returns to the XCR Calendar after 11 years. It will be a tight and
fast course with small undulations, but be prepared for some possible strong
winds with the park being closely positioned near Geelong’s Eastern Beach.
Spectators will enjoy the course layout with fantastic viewing of the races as
athletes weave their way in and out of the trees.

