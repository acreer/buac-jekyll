---
isCurrent: false
date: May 16 2020 14:00
title: South Bendigo AC Invitation
type: Invitation
location:
  text: Woodvale Recreation Reserve
  href: https://www.google.com.au/maps/dir//-36.6839937,144.2275984/@-36.6895476,144.2198926,16z/data=!4m2!4m1!3e0
distances: 1km, 3.2km, and 6.4km
setter:
  text: South Bendigo AC
  href: http://www.southbendigoac.org.au/
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Eaglehawk
: Woodvale Recreation Reserve (Tennis Club), Janaway Rd, Woodvale. See google map 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

### Long Course Map
<iframe name="plotaroute_map_408610" src="https://www.plotaroute.com/embedmap/408610" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/408610" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for South Bendigo AC Invitational Long Course on plotaroute.com</a></p>
