---
isCurrent: false
date: Jun 7 2020
title: Run Forrest
distances: 10km 21km 6km
type: other 
location: 
  text: Forrest, Otway Ranges
  href: http://www.runforest.com.au/map
setter:
  text: Run Forest
  href: http://www.runforest.com.au/
--- 

Staged in the hinterland township of Forrest, nestled in the heart of the Otway
Ranges, Run Forrest will showcase the regions world class trails and
breathtaking natural beauty – undulating hills, flowing rivers, dense fern
gullies and the cool, fresh air of the Otway Ranges.

