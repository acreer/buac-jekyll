---
isCurrent: false
date: Sep 26 2020 14:00
title: Club Mystery Run 1
layout: single
type: Club Run
location:
  text: its a mystery
  href: https://www.merriam-webster.com/dictionary/mystery
distances: Its a mystery....
setter: 
  text: Also a Mystery
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Its a mystery.........

BUT if its too much of a mystery noone will know where to go.
[{{page.location.text}}]({{page.location.href}})

This run is new and secret.  The idea 

- Nominate your own speed
- Run without time or gps devices (no watches, hour glass, satelite receivers)
- Win by being accurate to your prediction

Also a way for new runs to become favorites. 

## Please Note

These runs are all on different courses, so comparing results over the years is
not a fair comparison.  Also the aim is to run at a set speed, not fast!
Although nominating you maximum speed might be a good tactic.

    {% include run-summary-table yaml=site.data.csv2jekyll.mystery-run %}

