---
isCurrent: false
title: XCR-4 Lakeside 10
date: Jun 14 2020 8am
type: AV 
location: 
  text: Albert Park
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
--- 

A fast and flat 10km road race, starting and finishing at the Lakeside Stadium
precinct which includes two laps out and back from Lakeside Stadium to Junction
Oval on Lakeside Drive. The Albert Park 10km Road Race is the perfect
opportunity for athletes to run a fast time or to just enjoy the surrounds of
the lake.


