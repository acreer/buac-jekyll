---
isCurrent: false
date: Jul 25 2020 14:00
title: Ham Street Hustle
layout: single
type: Club Run
location:
  text: Ham Street Parking
  href: https://www.google.com.au/maps/dir//-36.7924414,144.2586475/@-36.7921624,144.2555939,17z
distances: 1km, 3km, and 6.9km
setter: 
  - text: Andrew Creer
    href: https://www.strava.com/athletes/11003459
  - text: Craig Feuerherdt
    href: https://www.strava.com/athletes/2452980
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

One of the Club Runs that everyone looks forward to for the sweet undulations.
The course setters have managed to make this run harder each year. Some hate it
but most LOVE it.

Golden Square
: From Hattam St. Golden Square turn south into McDougal Rd. Travel 1km then turn right into Ham Street. 
Travel 500m and enter the old Unity Mining carpark on the left.
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% for setter in page.setter %} {% include linkOrText.html link=setter %} {% endfor %}

    {% include run-summary-table yaml=site.data.csv2jekyll.ham-st-hustle %}

### Long Course Map
<iframe name="plotaroute_map_403642" src="https://www.plotaroute.com/embedmap/403642" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/403642" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Ham Street Hustle Long Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_403645" src="https://www.plotaroute.com/embedmap/403645" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/403645" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Ham Street Hustle Medium Course on plotaroute.com</a></p>
