---
isCurrent: false
date: Jul 5 2020 8:15am
title: Gold Coast Marathon
type: other 
distances: 42.2 21.1 10k
location: 
  text: Gold Coast
  href: https://goldcoastmarathon.com.au/
setter:
  text: Gold Coast Marathon
  href: https://goldcoastmarathon.com.au/
--- 

The world class international marathon will attract 28,000 participants of all
ages and abilities from over 50 countries across eight races including the
Village Roadshow Theme Parks Gold Coast Marathon, Wheelchair Marathon, ASICS
Half Marathon, Wheelchair 15km, Southern Cross University 10km Run, Gold Coast
Airport Fun Run, Garmin 4km Junior Dash and Garmin 2km Junior Dash.

The Village Roadshow Theme Parks Gold Coast Marathon, a World Athletics Gold
Label Road Race, is the pinnacle of road running events in Australia and during
its 41-year history has distinguished itself as one of the most prestigious
marathons in the world.

Its famous flat, fast and scenic course located alongside the city’s renowned
surf beaches and stunning broadwater plus ideal winter running conditions
featuring low humidity, little wind and mild temperatures result in 60% of
participants achieving personal best times each year.
