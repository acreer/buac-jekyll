---
title: Victorian Trail Running Championships
date: May 31 2020
type: AV 
location: 
  text: Lysterfield Lake, Lysterfield
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

The Victorian 16km Championships is the longest continuously held running
endurance event in Australia and only 2nd to Stawell Gift in history. The
expansive trail network of Lysterfield Park Lake in Melbourne’s Eastern Suburbs
will for the 3rd year host the event with 16km, 8.5km and 6km distances.

