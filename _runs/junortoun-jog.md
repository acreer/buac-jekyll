---
date: June 30 2018 14:00
title: Junortoun Jog
layout: single
type: Club Run
location:
  text: Turners Road and Braeside Drive Junortoun
  href: https://www.google.com.au/search?as_q=Turners+Road+and+Braeside+Drive+Junortoun
distances: 1km, 3km, and 8.6km
setter: Greg McBain
isCurrent: false
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Junortoun
: Heading out of Bendigo along Strathfieldsaye Road, turn left into Ryalls Lane. 
Veer left into Junortoun Rd then turn left into Turners Rd and continue to the end. 
Can also be accessed from McIvor Rd. See google map 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% include linkOrText.html link=page.setter %} 

    {% include run-summary-table yaml=site.data.csv2jekyll.junortoun-jog %}

### Long Course Map
<iframe name="plotaroute_map_403743" src="https://www.plotaroute.com/embedmap/403743" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/403743" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Junortoun Jog Long Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_439035" src="https://www.plotaroute.com/embedmap/439035" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/439035" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Junortoun Jog Medium Course on plotaroute.com</a></p>
