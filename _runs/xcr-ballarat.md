---
title: XCR-8 Ballarat Road Race
date: 2020-08-08
type: AV 
location: 
  text: Lake Wendouree
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

The traditional stomping ground of XCR legend Steve Moneghetti, Lake Wendouree
once again provides the backdrop for this fast-paced event, with open athletes
completing 15km and juniors 6km courses. After the race, stay on and enjoy the
best the city and surrounding Goldfields have to offer, including the Eureka
Centre, Sovereign Hill, the Gold Museum and the Botanical Gardens.


