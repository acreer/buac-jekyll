---
isCurrent: false
date: apr 25 2020 14:00
title: Mandurang Meander
type: Club Run
location:
  text: Nankervis Rd & Fadersons Ln, Mandurang
  href: https://www.google.com.au/maps/place/Nankervis+Rd+%26+Fadersons+Ln,+Mandurang+VIC+3551/@-36.8250934,144.3011977,17z/data=!3m1!4b1!4m5!3m4!1s0x6ad75b0284618fb9:0x13da83b4b48a6b60!8m2!3d-36.8250934!4d144.3033864
distances: 1km, 3km, and 6.5km
setter: 
  - text: Andy Buchannan
    href: https://www.strava.com/athletes/3871363
  - text: Lee McCullagh
    href: https://www.strava.com/athletes/7851212
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

This race was first run in 2009 as the end of year [Mystery Run]({% link _runs/mystery-run.md %}) and has since been a regular fixture for yearly aggregate points.

Mandurang Cricket Ground
: Travel South out Mandurang Rd, continue on to Faderson's Ln and over
Nankervis Rd. Turn right into cricket ground.  
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

## [Course Description]({% link _posts/blog/2020/2020-09-21-course-description-mandurang.md %}) | [Virtual Results](https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020) | [Virual Guidelines]({% link covid-spring.md %})


### Long Course Map
<iframe name="plotaroute_map_386542" src="https://www.plotaroute.com/embedmap/386542" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/386542" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Mandurang Meander Medium Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_402988" src="https://www.plotaroute.com/embedmap/402988" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/402988" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Mandurang Meander Medium Course on plotaroute.com</a></p>

The LONG course changed in 2013 from 6km to 6.5km

    {% include run-summary-table yaml=site.data.csv2jekyll.mandurang-meander %}
