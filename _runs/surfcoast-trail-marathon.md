---
isCurrent: false
date: Jun 27 2020 8:15am
title: Surf Coast Trail Marathon
type: other 
distances: 42.2 21.1
location: 
  text: Torquay - Fairhaven
  href: http://www.surfcoasttrailmarathon.com.au/
setter:
  text: Surf Coast Trail Marathon
  href: http://www.surfcoasttrailmarathon.com.au/
--- 

The route snakes from Australia’s Mecca of surfing, Torquay, to the beachfront
Fairhaven Surf Lifesaving Club on Victoria’s famed Surf Coast, only an hour
south west of Melbourne.

In the marathon, you can have a crack at the full distance solo, or knock off
roughly half each in teams of two. Or try the half for a taster.

In many ways, this is the perfect event for all runners eyeing the two
quintessential distances. For road runners it offers a first taste for trail,
without being intimidating as a relatively non-technical, non-remote,
non-mountainous trail. For experienced trailites, the route still features
stunning wilderness with coastal viewpoints you won’t believe (and becomes a
truly go-fast course with plenty of good twists and turns).

From iconic Bells Beach (yes you run a short stretch of this hallowed sand) to
the clifftops of Point Addis and Anglesea and on to Split Point lighthouse at
Airyes Inlet: this course has become renowned for the experience of journeying
through a unique coastal landscapes . That, and the famous live band afterparty
at the Aireys Pub. That’s pretty famous too. You can dance, yeah? Like Travolta
or a giraffe with marathon legs, doesn’t matter which - everyone’s welcome. 


