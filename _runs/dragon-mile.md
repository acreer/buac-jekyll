---
date: March 31 2018 10:30
location:
    text: Pall Mall
    href: http://www.bendigoharriers.org/dragon-mile
setter:
    href: http://www.bendigoharriers.org/
    text: Bendigo Harriers
isCurrent: false
type: AB run
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

The Bendigo Bank Dragon Mile has played a significant part in the athletics
history of Bendigo and plays a key part in the Easter Parade during this
festive period each year. It is run through the main streets of Bendigo
starting in Pall Mall. Crowds of approximately 20,000 line the streets to watch
the race.
