---
isCurrent: false
date: Jul 11 2020 14:00
title: Rifle Range Rattle
layout: single
type: Club Run
location:
  text: Wellsford Rd and Popes Track, Junortoun
  href: https://www.google.com.au/maps/place/Wellsford+Rd+%26+Pope+Track,+Longlea+VIC+3551/@-36.7541957,144.3855404,17z/data=!4m5!3m4!1s0x6ad75f0942f64b31:0x77a68295d115e73a!8m2!3d-36.7571666!4d144.386248
distances: 1km, 3.4km, and 7.9km
setter: 
  - Shayne Rushan 
  - Frances Walsh
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

Junortoun
: Travel East along McIvor Highway, turn left at Pope’s Road and follow the signs. Google Maps link 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %}{{page.setter.text}} {% endif %}


## [Course Description]({% link _posts/blog/2020/2020-09-21-course-description-rifle-range.md %}) | [Virtual Results](https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020) | [Virual Guidelines]({% link covid-spring.md %})

### Long Course Map
<iframe name="plotaroute_map_391105" src="https://www.plotaroute.com/embedmap/391105" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/391105" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Rifle Range Rattle Long Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_402859" src="https://www.plotaroute.com/embedmap/402859" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/402859" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Rifle Range Rattle Medium Course on plotaroute.com</a></p>



    {% include run-summary-table yaml=site.data.csv2jekyll.rifle-range-rattle %}
