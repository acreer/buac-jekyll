---
isCurrent: false
date: Oct 4 2020 7:00am
title: Melbourne Marathon
type: other 
distances: 42.2 21.1 10
location: 
  text: Melbourne Marathon
  href: https://melbournemarathon.com.au/
setter:
  text: Melbourne Marathon
  href: https://melbournemarathon.com.au/
--- 

Be a part of Australia’s largest marathon and race alongside marathon legends.
Circle around Albert Park Lake, Flinders Street Station, pass the St Kilda
beach foreshore and cross the finish line at the MCG to cheers from supporters,
family and friends. The Melbourne Marathon is a competitive, adrenaline-filled
race for those experienced in marathon running who want to set a PB on our fast
and flat course, and is also perfect for first-timers and less experienced
marathon runners.


