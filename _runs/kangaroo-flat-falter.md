---
isCurrent: false
date: Aug 22 2020 14:00
title: Kangaroo Flat Falter - Sporting Club Colours Day
layout: single
type: Club Run
location:
  text: Collins Street, Kangaroo Flat
  href: https://www.google.com/maps/dir//-36.7884578,144.2233338/@-36.7907057,144.231576,15.75z?hl=en-US
distances: 1km, 3km, and 8.2km
setter: 
  - text: Justin Lee
    href: https://www.strava.com/athletes/4745036
  - Jenny Lee
---
<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

[Detailed Description]({% link _posts/blog/2020/2020-11-16-course-description-kangaroo-flat-flat.md %})
: Part of the [Covid Spring Series]({% link covid-spring.md %})

Kangaroo Flat
: Head South on High St to Kangaroo Flat. Turn right into Station St then right into Olympic Pde. Take the 2nd left into Collins St and follow until dirt road. Meet at the bridge. Google Maps link 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% for setter in page.setter %} {% include linkOrText.html link=setter %} {% endfor %}



### Long Course Map
<iframe name="plotaroute_map_387729" src="https://www.plotaroute.com/embedmap/387729" frameborder="0" scrolling="no" width="640" height="425" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
<p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/387729" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Kangaroo Flat Falter Long on plotaroute.com</a></p>


    {% include run-summary-table yaml=site.data.csv2jekyll.kangaroo-flat-falter %}
