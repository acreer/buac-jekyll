---
isCurrent: false
date: Aug 15 2020 14:00
title: Glen Allen Memorial
layout: single
type: ab-run
location:
  text: Woodvale Recreation Reserve
  href: https://www.google.com.au/maps/place/Woodvale+Recreation+Reserve,+Woodvale+VIC+3556/@-36.6845566,144.2257215,17z/data=!4m5!3m4!1s0x6ad757da7eabb2bb:0xaa69bc22643ec627!8m2!3d-36.6846953!4d144.2267306
distances: 800m, 1500m, 3km, and 15km
setter:
  - text: South Bendigo AC
    href: http://www.southbendigoac.org.au/
  - text: Harriers AC
    href: http://www.bendigoharriers.org/
  - text: EagleHawk AC
    href: http://athsvic.org.au/club/eaglehawk-athletic-club/
---
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

[Athletics Bendigo Official page](https://www.athleticsbendigo.org.au/glen-allen)

Location
: [{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

