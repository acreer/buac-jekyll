---
date: 2017-08-12 14:00
title: Trotting Terrace Trundle
layout: single
type: Club Run
location:
  text: Lords Raceway, Junortoun
  href: https://www.google.com.au/maps/dir//-36.7680375,144.3337085/@-36.7704788,144.3346924,16.71z/data=!4m2!4m1!3e0
distances: 1km, 3.4km, and 9.5km
setter: 
  text: Tim Lauder
  href: https://www.strava.com/athletes/10739497
  text2: Harriers AC
  href2: http://www.bendigoharriers.org/
isCurrent: false
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

This race is held in combination with Harriers AC.

Junortoun
: Trotting Terrace (behind the trotting track). Google Maps link 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}
and {% if page.setter.href2 %}[{{page.setter.text2}}]({{page.setter.href2}}){% else %} {{page.setter.text2}} {% endif %}

### Past Handicap Winners

|Year|Long (9.5km)|Medium (3.4km)|Short (1.0km)|
|---|---|---|
|2017|Darren Hartland|Fiona Preston|Chole Beesley|
|2016|Craig Feuerherdt|Lynley McDonald|Toby McCaig|
|2015|Natalie Jacobson|Chris Giffin|Sachin Cody|

### Fastest Times - Long Course

|Year|Male|Time|Female|Time|
|---|---|---|
|2017|Craig Feuerherdt|37:03.3|Rebecca Wilkinson|43:51.2|
|2016|Rik McCaig|36:36.5|Sarah Jalim|37:34.3|
|2015|David Meade|33:43|Ingrid Douglass|43:31|
{: .tablesorter}

### Long Course Map
<iframe name="plotaroute_map_404833" src="https://www.plotaroute.com/embedmap/404833" frameborder="0" scrolling="no" width="640" height="425" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
<p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/404833" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Trotting Terrace Trundle Long on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_404834" src="https://www.plotaroute.com/embedmap/404834" frameborder="0" scrolling="no" width="640" height="425" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe>
<p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/404834" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Trotting Terrace Trundle Medium on plotaroute.com</a></p>
