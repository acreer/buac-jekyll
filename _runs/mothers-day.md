---
isCurrent: false
title: Mothers Day Classic
date: May 10 2020
type: other
location: 
  text: Beischer Park Strathdale
  href: https://www.mothersdayclassic.com.au/events/event-map/bendigo/
setter:
  text: Mothers Dat Classic
  href: https://www.mothersdayclassic.com.au/
--- 

Walk or run for breast cancer research
