---
isCurrent: false
date: Aug 29 2020 14:00
title: Rocky Rises 10
type: Club Run
distances: 1km 4km and 10km
location:
  text: Maiden Gully
  href: https://www.google.com.au/maps/dir//-36.7658388,144.2003308/@-36.7662063,144.2013808,16z/data=!4m2!4m1!3e0
setter:
    - Janelle Giffin
    - text: Chris Giffin
      href: https://www.strava.com/athletes/24258491
    - text: David Lonsdale
      href: https://www.strava.com/athletes/4125535 
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->


{{ page.title}}
From Olympic Parade Maiden Gully turn west on Rocky Rises Rd & travel for 2.5km
: [{{page.location.text}}]({{page.location.href}})

Distances
: {{ page.distances }}

Course Setter
:{% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

## [Course Description]({% link _posts/blog/2020/2020-09-21-course-description-rocky-rises.md %}) | [Virtual Results](https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020) | [Virual Guidelines]({% link covid-spring.md %})



    {% include run-summary-table yaml=site.data.csv2jekyll.rocky-rises %}
