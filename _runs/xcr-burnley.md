---
isCurrent: false
date: Aug 9 2020 8am
title: XCR-9 Victorian Half Marathon
type: AV 
location: 
  text: Burnley
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
--- 

With its low gradient course along the banks of the Yarra River, the Burnley
Half Marathon has deservedly gained a reputation as one of the fastest half
marathons in Australia. With that in mind, the race is the perfect opportunity
to shoot for a personal best or tune up for the Melbourne Marathon.



