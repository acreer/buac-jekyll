---
isCurrent: false
date: October 1 2020 14:00
title: College Classic
type: Club Run
location:
  text: Kairn Rd Strathdale
  href: https://www.google.com.au/maps/dir//Kairn+Rd,+Strathdale+VIC+3550/@-36.7826574,144.3116159,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x6ad7597d4c8d1631:0x268d2890dbf0b1e!2m2!1d144.3138046!2d-36.7826574
distances: 1km, 2km, and 6km
setter: 
 - text: Andrea Smith
   href: https://www.strava.com/athletes/12677228
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

[Detailed Course Description]({% link _posts/blog/2020/2020-11-06-course-description-college-classic.md %})
: As part of the [Covid Spring Series]({% link covid-spring.md %})

Strathdale
: [{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}


### Long Course Map

<div style="overflow:hidden;"> <div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;" /> <iframe name="plotaroute_map_569488" src="https://www.plotaroute.com/embedmap/569488" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe> </div> <p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/569488" target="_blank" title="View this route map on plotaroute.com">College Classic 5km</a> by <a href="https://www.plotaroute.com/userprofile/177223" target="_blank" title="View this person's profile on plotaroute.com">BUAC Runner</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a> </p>


### Medium Course Map

<div style="overflow:hidden;"> <div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;" /> <iframe name="plotaroute_map_569490" src="https://www.plotaroute.com/embedmap/569490" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe> </div> <p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/569490" target="_blank" title="View this route map on plotaroute.com">College Classic 2km</a> by <a href="https://www.plotaroute.com/userprofile/177223" target="_blank" title="View this person's profile on plotaroute.com">BUAC Runner</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a> </p>


### Short Course Map


<div style="overflow:hidden;"> <div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;" /> <iframe name="plotaroute_map_569492" src="https://www.plotaroute.com/embedmap/569492" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe> </div> <p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/569492" target="_blank" title="View this route map on plotaroute.com">College Classic 1km</a> by <a href="https://www.plotaroute.com/userprofile/177223" target="_blank" title="View this person's profile on plotaroute.com">BUAC Runner</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a> </p>


    {% include run-summary-table yaml=site.data.csv2jekyll.college-classic %}
