---
isCurrent: false
date: Jul 4 2020 14:00
title: Landry Lope
layout: single
type: Club Run
location:
  text: Lockwood Rd and Landry Track, Kangaroo Flat
  href: https://www.google.com.au/search?client=ubuntu&channel=fs&q=landry+track+and+lockwood+rd+kangaroo+flat&ie=utf-8&oe=utf-8&gfe_rd=cr&ei=9M3pWK38Eovr8wf_55jACQ
distances: 1km, 3km, and 6.9km
setter: 
  text: Ben McDermid
  href: https://www.strava.com/athletes/434687
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Lockwood South
: Take High St. to Kangaroo Flat. Turn right into Lockwood Rd. Drive approx
4.5km to Landry Track on Right – look for club signs. Google Maps link.
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

    {% include run-summary-table yaml=site.data.csv2jekyll.landry-lope %}

### Long Course Map
<iframe name="plotaroute_map_391114" src="https://www.plotaroute.com/embedmap/391114" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/391114" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Landry Lope Long Course on plotaroute.com</a></p>

### Intermediate Course Map
<iframe name="plotaroute_map_402967" src="https://www.plotaroute.com/embedmap/402967" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/402967" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Landry Lope Medium Course on plotaroute.com</a></p>
