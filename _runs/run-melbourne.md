---
isCurrent: false
date: Jul 26 2020 8:00am
title: Run Melbourne
type: other 
distances: 42.2 21.1 10
location: 
  text: Run Melbourne
  href: https://events.solemotive.com/run-melbourne/
setter:
  text: Run Melbourne
  href: https://events.solemotive.com/run-melbourne/
--- 

Run Melbourne isn’t your average run. Over the last 12 years it has grown into
a movement that has changed the culture of what it means to run through the
city streets.

With closed roads and a fast, flat course, more than 20,000 runners descend on
the streets, heading past some of Melbourne’s most famous landmarks. Cheer
zones and epic vibes make this a race weekend unlike any other. Join the
movement and let’s inspire our city to run.
