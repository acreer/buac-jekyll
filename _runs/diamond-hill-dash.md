---
title: Diamond Hill Dash
date: 2015-01-01
type: Club Run
isCurrent: false
--- 

This run is resting.  Good long hill, but 100km/hr bitumen.  Not much
carparking at the start.

 {% include run-summary-table yaml=site.data.csv2jekyll.diamond-hill-dash %}
