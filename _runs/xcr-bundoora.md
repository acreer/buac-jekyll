---
title: XCR-7 Bundoora
date: 2020-07-25
type: AV 
location: 
  text: Bundoora Park
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

Note: Selection Trial for the Australian Cross Country Championships.

This bumper day of cross country racing incorporates the Victorian All Schools
Cross Country Championships and also serves as the selection trial for the
Australian Cross Country Championships. Both the men and women races cover 10km
of the gruelling slopes of Bundoora Park, while juniors race over 3km, 4km, 6km
or 8km.

U20 athletes who have not entered the Victorian All Schools Cross Country (must
be currently attending school to do so), must purchase an individual entry to
participate in this event. There are no events for U14, U16 and U18 athletes
for XCR’20 Round 3, all junior athletes attending school should enter the
Victorian All Schools Cross Country Championships. 

Bundoora Park is not offered as part of the Junior XCR/Max Package in 2020.


