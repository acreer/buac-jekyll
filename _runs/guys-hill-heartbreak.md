---
title: Guys Hill Heartbreak
date: 2010-05-23
type: Club Run
isCurrent: false
distances: 1km, 3km, 7.7km
setter:
   - text: Nigel Preston
     href: https://www.strava.com/athletes/1080178
location:
   text: Guys Hill Rd
   href: https://www.google.com.au/maps/place/Strathfieldsaye/@-36.7953333,144.3271967,18z/data=!3m1!4b1!4m5!3m4!1s0x6ad75be6f772ef17:0x74bdf3cf7816f010!8m2!3d-36.7953333!4d144.3282911

--- 

NEW COURSE: Guys Hill Rd to Edwards Rd Return.


Detailed description coming soon
: Part of the [Covid Spring Series]({% link covid-spring.md %})

Race Distances 
: {{page.distances}}

Course Setter
: {% for setter in page.setter %} {% include linkOrText.html link=setter %} {% endfor %}

Strathdale
: [{{page.location.text}}]({{page.location.href}})

## Map
<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_1332711" src="https://www.plotaroute.com/embedmap/1332711?units=km&hills=show" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/1332711?units=km" target="_blank" title="View this route map on plotaroute.com">BUAC - Heartbreak Return </a> by <a href="https://www.plotaroute.com/userprofile/147126" target="_blank" title="View this person's profile on plotaroute.com">Nigel Preston</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

The previous version of this run is resting.  Guys Hill Rd is a bit busy on a Saturday Afternoon.

 {% include run-summary-table yaml=site.data.csv2jekyll.guys-hill-heartbreak %}
