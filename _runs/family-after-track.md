---
isCurrent: false
date: Mar 28 2020 11:00am
title: Family Fun Day
location: 
  text: The Track, Retreat Rd Flora Hill
  href: https://www.athleticsbendigo.org.au/the-complex-lubac
distances: BBQ, Games and More
setter: 
  text: The Committee
  href: http://www.bendigouniathsclub.org.au/ac/about.html#contacts
type: social
---

__This will need to wait till next year.__

Come to the track from 11am till 3pm

Celebrate the end of the summer season, and the build up to winter. Catch up with the Handicappers and mention lack of training injuries etc.

Members please bring a plate of afternoon tea

Members and non members welcome.
