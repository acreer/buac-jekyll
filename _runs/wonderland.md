---
isCurrent: false
date: Aug 23 2020 6:00am
title: Peaks and Trails
type: other 
distances: 60 36 20 8 2
location: 
  text: Halls Gap, Grampians
  href: http://www.wonderlandrun.com.au/
setter:
  text: Wonderland Run
  href: http://www.wonderlandrun.com.au/
--- 

Sunday 6am  60km Run. At 5am buses set out to Beehive Falls car park, Roses Gap
for 6am start. Run back to town along the Mount Difficult range and flow into
the 36km course.  Age limit:18+

Sunday 7am 36km Run. Set out with the 20kers, but get the magic of Mount Rosea
too.  Age limit:18+

Full Impact 20 km run on the Sunday 7am.  See major icons such as the
Elephant's Hide, Grand Canyon, The Pinnacle and descend around Sundial Peak.
Look at the elevation profile if you think this is just another half marathon
effort.  Age limit:18+

Flatter, shorter option at 2pm Saturday. Mixture of trail and path. Gives you
time to drive out, get set up and get a run in. Open to all ages.


Flat-ish, shortest option at 1pm Saturday. Open to all ages. Prizes for kids.
(Adults, please don't win it.)
