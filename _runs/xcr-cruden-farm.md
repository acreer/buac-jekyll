---
isCurrent: false
date: May 23 2020 12pm
title: XCR-3 Cruden Farm
type: AV 
location: 
  text: Cruden Farm
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
--- 

Cruden Farm will host the 12km heritage round. The grounds of Cruden Farm offer
athletes a tough and testing course with a mixture of different terrains to
accompany the beautiful grounds surrounding the farm. Cruden Farm is a historic
and inspirational venue and has been a great addition to the XCR calendar. The
2020 event is a great opportunity for Clubs to showcase their heritage and
history and we encourage Clubs to do so again this year.


