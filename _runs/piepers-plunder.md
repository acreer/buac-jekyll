---
title: Piepers Plunder
date: July 7 2018 14:00
layout: single
type: Club Run
location:
  text: Wildflower Drive
  href: https://www.google.com.au/maps/dir//-36.7901966,144.3379397/@-36.7923213,144.3313317,15.5z/data=!4m2!4m1!3e2
distances: 1km, 4km, and 8km
setter:
  - text: David Lonsdale
    href: https://www.strava.com/athletes/4125535
  - Alan East
isCurrent: false
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

2017 sees the return of a run last held in 2010. 

Strathfieldsaye
: Heading out of Strathdale along Strathfieldsaye Road, Turn left into Wildflower Drive 1.5km after passing Guys Hill Road - look out for the club signs.
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% for setter in page.setter %} {% include linkOrText.html link=setter %} {% endfor %}


    {% include run-summary-table yaml=site.data.csv2jekyll.piepers-plunder %}



### Long Course Map
<iframe name="plotaroute_map_404308" src="https://www.plotaroute.com/embedmap/404308" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/404308" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Piepers Long Course (2 x laps) on plotaroute.com</a></p>

### Medium Course Map
Single loop of that shown in Long Course Map above
