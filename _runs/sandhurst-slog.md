---
isCurrent: false
date: Aug 8  2020 14:00
title: Sandhurst Slog
layout: single
type: Club Run
location:
  text: Read Lane, Kangraroo Flat
  href: https://www.google.com.au/maps/dir/-36.8208001,144.2587206//@-36.8218736,144.25827,17z/data=!4m2!4m1!3e0
distances: 1km, 3km, and 7km
setter:
  - text: David Heislers
    href: https://www.strava.com/athletes/2613270
  - text: Matthew Heislers
    href: https://www.strava.com/athletes/2554855
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
--> 


This race was first run in 2012 as the end of year Mystery Run and has since been a regular fixture for yearly aggregate points.


[Detailed Description]({%link _posts/blog/2020/2020-11-16-course-description-sandhurst-slog.md %})
: Part of the [Covid Spring Series]({% link covid-spring.md %})

Kangaroo Flat
: From Hattam St. Golden Square travel south on Woodward Road then Diamond Hill Road. Turn right into Kangaroo Gully Road and continue 500m to Read Lane on the left. 
From Kangaroo Flat, heading along Allingham Street (changes to Kangaroo Gully Road), continue 2.5km south from intersection of Chapel Street to Read Lane on the right.
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% for setter in page.setter %} {% include linkOrText.html link=setter %} {% endfor %}



### Long Course Map
<iframe name="plotaroute_map_391120" src="https://www.plotaroute.com/embedmap/391120" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/391120" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Sandhurst Slog Long Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_402958" src="https://www.plotaroute.com/embedmap/402958" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/402958" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Sandhurst Slog Medium Course on plotaroute.com</a></p>


    {% include run-summary-table yaml=site.data.csv2jekyll.sandhurst-slog %}
