---
title: Cedar Drive Dash
date: 2015-01-01
type: Club Run
isCurrent: false
--- 

This run is resting.  It was based around a shed in a club members front yard.  
It did get run in opposite directions in alterate years.

 {% include run-summary-table yaml=site.data.csv2jekyll.cedar-drive-dash %}
