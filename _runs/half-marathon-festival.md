---
date: Sep 12 2020 14:00
title: POSTPONED Half Marathon Festival
isCurrent: false
layout: single
type: Invitation
location:
  text: Golf Course Road, Epsom (2km East of Midland Hwy)
  href: https://www.google.com.au/maps/dir///-36.7158379,144.3348403/@-36.7270754,144.3477068,14.5z/data=!4m2!4m1!3e2
distances: 1km, 3km, 7km, 14km and 21.1km
setter:
  text: Alan East
header:
  overlay_image: assets/promotionBanners/uniHalfRunners.png
  overlay_color: white
  title: Bendigo University Athletics Club Half Marathon Festival
  excerpt: POSTPONED - Date to be announced
  XXcta_label: Enter Online
  XXcta_url: https://my1.raceresult.com/128637/registration?lang=en&mode=1&contest=-1
---

With the current State of Disaster impacting our original date for our annual
half marathon we are postponing the event until such time that we can lock in
a new date. Please be assured that will do everything within our control and
are still planning to hold our annual half marathon in 2020 subject to Aths Vic
approval and current local restrictions with date to be confirmed once we have
clarity.

In the meantime please continue to enjoy safe training in line with current
restrictions etc…

Date POSTPONED FROM. New date Unknown
: {{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Age/Distance categories
: Ribbons will be awarded in each of the following distance gender and age
combinations.

Entry Fees

Online entry is encouraged, as it makes life easier for the organisers on the
day. It it cheaper as well

|  Entry Fee   | Online | On the day |
| :----------: | :----: | :--------: |
|    Adults    |  \$10  |    \$15    |
| Children <16 |  \$5   |    \$8     |
|   Students   |  \$5   |    \$8     |
| BUAC members |  FREE  |  As above  |

Distances are measured with wheel by old school enngineer types. The distance
is right despite what your Garmin might say

| Distance |     Gender      |        Categories        |
| :------: | :-------------: | :----------------------: |
| 21.1 km  | Male and Female | Open, 40+, 50+, 60+, 70+ |
|  14 km   | Male and Female | Open, 40+, 50+, 60+, 70+ |
|   7 km   | Male and Female | Open, 40+, 50+, 60+, 70+ |
|   3 km   | Male and Female |      Open, U16, U12      |
|   1 km   | Male and Female |    Open, U10, U8, U6     |

World Famous in Bendigo and the greatest trail half in the Universe...

Getting there
: From Bendigo, head north on Midland Hwy. Turn right into Taylor Street then right into Golf Course Road. Google Maps link
[{{page.location.text}}]({{page.location.href}})

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

### Course Map

The 7km 14km and 21.1km courses run 1 2 or 3 laps of the course. All Entrants can
change the number of laps in their own race, while they are on the course.
These changes MUST be communicated to the Timing Officials

<iframe name="plotaroute_map_404848" src="https://www.plotaroute.com/embedmap/404848" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/404848" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Invitation Long Course on plotaroute.com</a></p>

### Past Winners

| Year                                                                                     | Half Marathon (21.1km) | Time      | Club     |
| ---------------------------------------------------------------------------------------- | ---------------------- | --------- | -------- |
| [2019]({% link _raceData/2019/2019-09-14-half-marathon-festival-long-results.md %})      | Sarah Jalim            | 1:23:52.4 | UNI      |
| [2018]({% link _raceData/2018/2018-09-08-half-marathon-long-results.md %})               | Jamie Cooke            | 1:14:41.3 | HARRIERS |
| [2017]({% link _raceData/2017/2017-09-09-buac-half-marathon-festival-long-results.md %}) | Jason Sim              | 1:19:33.8 | UNI      |
| [2016]({% link _raceData/2016/2016-09-10-buac-half-marathon-long-results.md %})          | Ben Stolz              | 1:20:54.4 | HARRIERS |
| [2015]({% link _raceData/2010/2010-09-11-half-marathon-long-results.md %})               | Fraser Walsh           | 1:13:48.3 | UNI      |
| [2014]({% link _raceData/2014/2014-09-13-half-marathon-long-results.md %})               | Fraser Walsh           | 1:15:38.3 | UNI      |
| [2013]({% link _raceData/2013/2013-09-14-half-marathon-long-results.md %})               | Michael Bieleny        | 1:19:01   | UNI      |
| [2012]({% link _raceData/2012/2012-09-08-half-marathon-festival-long-results.md %})      | Richard Gleisner       | 1:17:12   | UNI      |
| [2011]({% link _raceData/2011/2011-09-10-buac-half-marathon-long-results.md %})          | Pat Kenny              | 1:19:50   | UNI      |
| [2010]({% link _raceData/2010/2010-09-11-half-marathon-long-results.md %})               | Pat Kenny              | 1:18:20   | UNI      |
| [2009]({% link _raceData/2009/2009-09-12-half-marathon-long-results.md %})               | Pat Kenny              | 1:20:50   | UNI      |

| Year | Longer (14km)   | Time    | Club     |
| ---- | --------------- | ------- | -------- |
| 2019 | Luke Preston    | 54:10.5 | UNI      |
| 2018 | Sarah Jalim     | 56:59.7 | UNI      |
| 2017 | Nigel Preston   | 51:56.2 | UNI      |
| 2016 | Darren Hartland | 55:50.9 | UNI      |
| 2015 | Tim Lauder      | 57:41.4 | UNI      |
| 2014 | Jamie Cook      | 50:59.5 | HARRIERS |
| 2013 | Andrew Hosking  | 56:49   | UNI      |
| 2012 | Trevor Kelly    | 52:52   | EGWK     |
| 2011 | Peter Rice      | 49:58   | BYM      |
| 2010 | Kaine Leech     | 55:12   | EGHK     |
| 2009 | Michael Bieleny | 57:41   | UNI      |

| Year | Long (7km)     | Time    | Club     |
| ---- | -------------- | ------- | -------- |
| 2019 | Ian Wellard    | 30:56.6 | HARRIERS |
| 2018 | Max Rowe       | 30:23.8 | UNI      |
| 2017 | Jacob Nolan    | 24:53.5 | UNI      |
| 2016 | Ian Wellard    | 27:34.9 | HARRIERS |
| 2015 | Michael Preece | 25:56.5 | OTHER    |
| 2014 | Trevor Kelly   | 25:35.3 | EGWK     |
| 2013 | Andy Buchanan  | 23:32   | UNI      |
| 2012 | Peter Cowell   | 28:09   | HARRIERS |
| 2011 | Tom Savage     | 27:04   | EGWK     |
| 2010 | David Birch    | 25:49   | BYM      |
| 2009 | Rick Ermel     | 26:36   | BYM      |

| Year | Medium (3km)  | Time    | Club |
| ---- | ------------- | ------- | ---- |
| 2019 | Harrison Boyd | 10:54.5 | HAR  |
| 2018 | Logan Tickell | 11:26.0 | SBO  |
| 2017 | Logan Tickell | 11:49.3 | SBO  |
| 2016 | Wil McCaig    | 11:51.4 | UNI  |
| 2015 | Wil McCaig    | 11:49.4 | UNI  |
| 2014 | Spencer Evans | 11:49.6 | HAR  |
| 2013 | Jack Meade    | 12:41   | UNI  |
| 2012 | Callum Tucker | 14:10   | UNI  |
| 2011 | Nathan Green  | 11:46   | BYM  |
| 2010 | Nathan Green  | 12:35   | BYM  |
| 2009 | Kaine Leech   | 10:10   | EGWK |

| Year | Short (1km)    | Time   | Club  |
| ---- | -------------- | ------ | ----- |
| 2019 | Mason Woodward | 3:57.2 | UNI   |
| 2018 | Will de Vries  | 4:40.4 | UNI   |
| 2017 | Mason Woodward | 4:18.9 | UNI   |
| 2016 | Toby McCaig    | 3:31.9 | UNI   |
| 2015 | Eli Pearce     | 3:52   | SBO   |
| 2014 | Floyd Cartner  | 3:51.2 | UNI   |
| 2013 | Floyd Cartner  | 4:05   | UNI   |
| 2012 | Floyd Cartner  | 4:12   | UNI   |
| 2011 | Matilda Moore  | 4:22   | OTHER |
| 2010 | Tom Coleman    | 5:29   | UNI   |
| 2009 | Harry Jackel   | 4:32   | UNI   |

# Work in progress AUTO generated table...

{% include run-summary-table handicap=false yaml=site.data.csv2jekyll.half-marathon-festival noHandicap=true %}
