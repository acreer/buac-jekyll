---
isCurrent: false
date: Aug 15 2020 14:00
title: Hornets Hideaway 
setter:
    text: Craig Green
    href: https://www.strava.com/athletes/7537001
location:
    text: Crusoe No 7 Park
    href: https://www.google.com.au/maps/dir//-36.8315381,144.2330999/@-36.8247533,144.2263244,15z/data=!4m2!4m1!3e2
distances: 9.5km, 4.2km, 1km
type: Club Run
---
<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

[Detailed Description]({%link _posts/blog/2020/2020-11-06-course-description-hornets-hideaway.md %})
: Part of the [Covid Spring Series]({% link covid-spring.md %})

Kangaroo Flat
: Travel south on the Calder Hwy, turn right into Furness St (Harvey Norman) then take the first left into Granter St.  Continue on Granter St for 1.3 km.
[{{page.location.text}}]({{page.location.href}})

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

## Long Course Map
<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_497917" src="https://www.plotaroute.com/embedmap/497917?units=km&hills=show" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/497917?units=km" target="_blank" title="View this route map on plotaroute.com">Bendigo University Athletics Club Mystery Run 2017</a> by <a href="https://www.plotaroute.com/userprofile/57073" target="_blank" title="View this person's profile on plotaroute.com">Craig Green</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

## Medium Course Map
<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_497964" src="https://www.plotaroute.com/embedmap/497964?units=km&hills=show" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/497964?units=km" target="_blank" title="View this route map on plotaroute.com">BUAC Mystery Run 4.1km 2017</a> by <a href="https://www.plotaroute.com/userprofile/155150" target="_blank" title="View this person's profile on plotaroute.com">Darren Rowe</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

    {% include run-summary-table yaml=site.data.csv2jekyll.hornets-hideaway %}

