---
title: Apollo Hill Attack
date: 2015-01-01
type: Club Run
isCurrent: false
--- 

This run is resting.  There is a section with blind corners on an increasingly
busy 100km/hr road.

 {% include run-summary-table yaml=site.data.csv2jekyll.apollo-hill-attack %}
