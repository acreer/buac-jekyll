---
title: XCR-10 Tan Relays
date: 2020-09-19
type: AV 
location: 
  text: The Tan Track, Melbourne Botanical Gardens
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

XCR’20 returns to Melbourne’s spiritual home of running, the Tan track in the
heart of the city’s sporting precinct. With a host of local and international
distance-running stars having made their mark on the hallowed course, this is
your chance to test yourself both against the greats and against the clock. All
competitors complete one lap of the 3.8km circuit, which winds its way around
the iconic Botanical Gardens and Kings Domain.
