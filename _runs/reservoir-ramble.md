---
date: May 26 2018 14:00
title: Reservoir Ramble
layout: single
type: Club Run
location:
  text: Sandhurst Reservoir Road
  href: https://www.google.com.au/maps/dir//-36.840465,144.2443422/@-36.8385828,144.2384581,15.25z
distances: 1km, 3km, and 7km
setter: 
  text: David Lonsdale
  href: https://www.strava.com/athletes/4125535
isCurrent: false
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

2017 sees a new club run that was first introduced as the 2016 Mystery Run.

Kangaroo Flat
: Travel south on Calder Highway, turn left into Phillis Street, follow the bitumen over the railway line to the Sandhurst Reservoir gates
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

    {% include run-summary-table yaml=site.data.csv2jekyll.reservoir-ramble %}

### Long Course Map
<iframe name="plotaroute_map_433382" src="https://www.plotaroute.com/embedmap/433382" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/433382" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Reservoir Ramble Long Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_433380" src="https://www.plotaroute.com/embedmap/433380" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/433380" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Reservoir Ramble Medium Course on plotaroute.com</a></p>
