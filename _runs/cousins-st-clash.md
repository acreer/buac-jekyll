---
title: Cousins St Clash
date: Aug 25 2018 14:00
type: Club Run
setter: Gavin Fielder
location: Cousins St
isCurrent: false
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

    {% include run-summary-table yaml=site.data.csv2jekyll.cousins-st-clash %}
