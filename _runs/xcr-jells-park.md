---
title: XCR-1 Jells Park
date: 2020-04-18
type: AV 
location: 
  text: Jells Park
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

XCR Racing once again kicks off in the beautiful surrounds of Jells Park in
Melbourne’s south east with the Victorian Cross Country Relay Championships.
The challenging and undulating 3km course traverses both open grassland and
wooded forest areas, with open and overage competitors completing two laps per
leg and junior competitors one lap per leg.


