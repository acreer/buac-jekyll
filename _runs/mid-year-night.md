---
title: Mid Year Social Night
date: June 16 2018 18:30
location: 
  text: To be advised
distances: An exciting night for all the family
setter: 
  text: Andrea Smith
  href: https://www.strava.com/athletes/12677228
isCurrent: false
type: social
---

A great night for the whole family.

The date is {{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

![Trivia NIght]({% link assets/files/2018/TriviaNightPoster.jpeg %})

Kids music rounds, Some athletics questions...



