---
title: XCR-5 St Anne's – Ekiden Relays
date: 2020-06-27
type: AV 
location: 
  text: St Anne's Winery, Myrniong
  href: http://athsvic.org.au/events/competitions/avcompetitions/xcr/
setter:
  text: Athletics Victoria
  href: http://athsvic.org.au/
isCurrent: false
--- 

St Anne’s Winery in Myrniong returns for XCR’20 but this year it will hold the
Ekiden Relay. Ekiden is a Japanese term which refers to a long-distance relay
running race over varied distances and terrains, meaning you pick your team to
suit the various legs. Traditionally held over the marathon distance of
42.195km, the AV Ekiden offers a range of distances to suit our junior, open
and masters aged runners. But don’t just come for the run, stay and enjoy the
local produce in St Anne’s breathtaking grounds once you have finished your
leg.


