---
isCurrent: false
date: Aug 1 2020 14:00
title: Harriers AC Invitation
layout: single
type: Invitation
location:
  text: Mandurang South
  href: https://www.google.com.au/maps/dir//-36.8459323,144.2759268/@-36.8468424,144.2694251,16z
distances: 1km, 3km, and 8km
setter:
  text: Harriers AC
  href: http://www.bendigoharriers.org/
---
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Mandurang South
: Mandurang South Recreation Reserve (Tennis Club), Hannans Road, Mandurang South.
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

### Long Course Map
<iframe name="plotaroute_map_404353" src="https://www.plotaroute.com/embedmap/404353" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/404353" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for Harriers AC Invitational Long Course on plotaroute.com</a></p>
