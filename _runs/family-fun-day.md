---
title: Family Fun Day
date: April 6 2019 11:00am
location: 
  text: Latrobe University Athletics Track
  href: https://www.athleticsbendigo.org.au/the-complex-lubac
distances: Until 3pm
setter: 
  text: The Committee
  href: http://www.bendigouniathsclub.org.au/ac/about.html#contacts
isCurrent: false
type: social
---

<img src="http://bendigouniathsclub.org.au/blog/wp-content/uploads/2019/03/Slide1.jpg">


[{{page.location.text}}]({{page.location.href}}) on {{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
