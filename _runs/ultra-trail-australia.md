---
isCurrent: false
date: May 17 2020
title: Ultra Trail Australia
type: other 
location: 
  text: Katoomba
  href: https://www.ultratrailaustralia.com.au/uta/about-the-blue-mountains
setter:
  text: UTA
  href: http://ultratrailaustralia.com.au/
--- 

The breathtaking trails located in the World Heritage listed Blue Mountains
National Park of Australia was the obvious choice. Starting in 2008 as The
North Face 100 and Marathon Pairs, the 50km was introduced in 2013. 2016 saw an
expansion of the event and a name change to Ultra-Trail Australia with a
substantial increase in the entrant numbers in the 100km and 50km and the
introduction of the UTA22, Scenic World UTA951 (Furber Stairs Time Trial) and
Event Expo. UTA11 was introduced in 2019. Ultra-Trail Australia has captivated
runners locally and internationally and It has quickly grown to become one of
the most talked about endurance events in Australian history.




