---
isCurrent: false
date: Aug 1 2020 14:00
title: Pearces Road Rally 
layout: single
type: Club Run
location:
  text: Pearces Road, Mandurang
  href: https://www.google.com.au/maps/dir//-36.840644,144.3082778/@-36.8420373,144.2990109,16z/data=!4m2!4m1!3e2
distances: 1km, 3.6km, and 7.5km
setter: 
  text: Ben McDermid
  href: https://www.strava.com/athletes/434687
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->

[Detailed Description]({% link _posts/blog/2020/2020-11-06-course-description-pearces-road-rally.md %})
: Part of the [Covid Spring Series]({% link covid-spring.md %})

Mandurang
: Travel out Mandurang Rd, turn right into Nankervis Rd then left into Pearces Rd meeting at intersection with Diggers Road. Google Maps link 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

Mass starts for each race at the usual times.  Handicaps still apply for points, but everyone starts together, so ribbons are not for first across the line.

Pearces Road Rally returns after missing out on a spot in 2016. The return sees a new route of 7.6km rather than 7.5km with an additional 3m of elevation gain with some lovely single track replacing the Pearces Road section. Check out the new routes using the links below.

## MAPS Covid variations - No single track

### Long Course Map
<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_404800" src="https://www.plotaroute.com/embedmap/404800?units=km&hills=show" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/404800?units=km" target="_blank" title="View this route map on plotaroute.com">Pearces Road Rally</a> by <a href="https://www.plotaroute.com/userprofile/131757" target="_blank" title="View this person's profile on plotaroute.com">John Robinson</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

### Medium Course Map
<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_404804" src="https://www.plotaroute.com/embedmap/404804?units=km&hills=show" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/404804?units=km" target="_blank" title="View this route map on plotaroute.com">Pearces Road Rally Medium</a> by <a href="https://www.plotaroute.com/userprofile/131757" target="_blank" title="View this person's profile on plotaroute.com">John Robinson</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

## Regular season maps - With single track

### Long Course Map
<iframe name="plotaroute_map_457701" src="https://www.plotaroute.com/embedmap/457701" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/457701" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Pearces Road Long Course on plotaroute.com</a></p>

### Medium Course Map
<iframe name="plotaroute_map_457707" src="https://www.plotaroute.com/embedmap/457707" frameborder="0" scrolling="no" width="525" height="350" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe><p style="margin-top:8px;"><a href="https://www.plotaroute.com/route/457707" target="_blank" title="View this route map on plotaroute.com" style="font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">View route map for BUAC Pearces Road Medium Course on plotaroute.com</a></p>


    {% include run-summary-table yaml=site.data.csv2jekyll.pearces-road-rally %}
