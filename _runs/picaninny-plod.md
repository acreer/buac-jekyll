---
title: Picaninny Plod
date: 2015-01-01
type: Club Run
isCurrent: false
--- 

This run is resting.  There were too many blind hills, and a few cars going quite fast.

 {% include run-summary-table yaml=site.data.csv2jekyll.picaninny-plod %}
