---
isCurrent: false
date: Sep 5 2020 14:00
title: Notleys Noodle
layout: single
type: Club Run
location:
  text: Notley's Reserve, Whipstick
  href: https://www.google.com.au/maps/dir//Whipstick+campground/@-36.6553326,144.2657146,15.88z/data=!4m8!4m7!1m0!1m5!1m1!1s0x0:0xa07ed2959001b757!2m2!1d144.2632294!2d-36.6501666
distances: 1km, 4.8km, and 11km
setter: 
  text: Ross Douglas
  href: https://www.strava.com/athletes/2532558
---

<!--
{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}
-->


Whipstick
: Notley’s Reserve, Whipstick: head north on Eaglehawk-Neilborough Road for approximately 10km. Turn left into Notley Road follow signs. Google Maps link 
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %}{{page.setter.text}} {% endif %}

## [Course Description]({% link _posts/blog/2020/2020-09-21-course-description-notleys.md %}) | [Virtual Results](https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020) | [Virual Guidelines]({% link covid-spring.md %})

### Long Course Map

<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_1288203" src="https://www.plotaroute.com/embedmap/1288203?units=km" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/1288203?units=km" target="_blank" title="View this route map on plotaroute.com">Notleys Noodle - Long BUAC</a> by <a href="https://www.plotaroute.com/userprofile/147126" target="_blank" title="View this person's profile on plotaroute.com">Nigel Preston</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

### Medium Course Map

<div style="overflow:hidden;position:relative;"><div style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe name="plotaroute_map_1294788" src="https://www.plotaroute.com/embedmap/1294788?units=km" style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%; height:100%;" frameborder="0" scrolling="no" allowfullscreen webkitallowfullscreen mozallowfullscreen oallowfullscreen msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica Neue,Helvetica,arial;font-size:11px;">Route map for <a href="https://www.plotaroute.com/route/1294788?units=km" target="_blank" title="View this route map on plotaroute.com">Notleys Noodle - Medium BUAC</a> by <a href="https://www.plotaroute.com/userprofile/147126" target="_blank" title="View this person's profile on plotaroute.com">Nigel Preston</a> on <a href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free route planner for walking, running, cycling and more">plotaroute.com</a></p>

### Short Course Map

<div style="overflow:hidden;position:relative;"><div
style="position:relative;width:100%;padding-top:56.25%;overflow:visible;"/><iframe
name="plotaroute_map_1294789"
src="https://www.plotaroute.com/embedmap/1294789?units=km"
style="position:absolute;top:0;left:0;bottom:0;right:0;width:100%;
height:100%;" frameborder="0" scrolling="no" allowfullscreen
webkitallowfullscreen mozallowfullscreen oallowfullscreen
msallowfullscreen></iframe></div><p style="margin-top:8px;font-family:Helvetica
Neue,Helvetica,arial;font-size:11px;">Route map for <a
href="https://www.plotaroute.com/route/1294789?units=km" target="_blank"
title="View this route map on plotaroute.com">Notleys Noodle - Short BUAC</a>
by <a href="https://www.plotaroute.com/userprofile/147126" target="_blank"
title="View this person's profile on plotaroute.com">Nigel Preston</a> on <a
href="https://www.plotaroute.com" target="_blank" title="plotaroute.com - free
route planner for walking, running, cycling and
more">plotaroute.com</a></p>


    {% include run-summary-table yaml=site.data.csv2jekyll.notleys-noodle %}
