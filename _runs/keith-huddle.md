---
isCurrent: false
date: Jul 18 2020 14:00
title: Keith Huddle Memorial
layout: single
type: ab-run
location:
  text: Quarry Hill Recreation Reserve 
  href: https://www.google.com.au/maps/dir//Ken+Wust+Oval,+51+Hamlet+St,+Quarry+Hill+VIC+3550/@-36.7744099,144.2745422,17z/
distances: 1km, 3km, and 6km
setter:
  text: Harriers AC
  href: http://www.bendigoharriers.org/
---

{{ page.date| date: '%A %-d %B %Y at %l:%M %P' }}

Quarry
: Quarry Hill Recreation Reserve Ken Wust Oval Hamlet Street Quarry Hill.
[{{page.location.text}}]({{page.location.href}})

Race Distances
: {{page.distances}}

Course Setter
: {% if page.setter.href %}[{{page.setter.text}}]({{page.setter.href}}){% else %} {{page.setter.text}} {% endif %}

### Long Course Map
Soon
