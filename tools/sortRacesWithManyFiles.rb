require 'optparse'
require 'pp'
require 'fileutils'

optparser = OptParse.new{|p|
  p.banner= <<-EOD

SO.....

Darren sends us some files that he will generate for the
Invite Or Half Marathon. We save them all into one BIG FLAT
folder from within the email client.  These gnerally have
the data from the previous year and so we DON'T want to
publish them until the actual data comes in.

  - BUT we dont know which files are LONG MEDIUM or SHORT 
  - AND we want to tweak the default sort oder of the
  tablesorters on the web site 
  - AND we dont want to make mistakes on RACE DAY night by
  phaffing with files again after running a PB...
  - AND we dont want to accidently publish the OLD practice
  files onto the live site...

SO

# BEFORE RACE DAY

The Invite LONG race files all seem to have 7.5 in filename.
So MANUALLY with a filemanager, create a folder structure
that will work for us.

      2017/08_BuacInvite/results/
      ├── long
      │   ├── 03
      │   ├── 05
      │   └── 07
      ├── medium
      │   ├── 03
      │   ├── 05
      │   └── 07
      └── short

The 'gaps' are just so I have somewhere to go before between
03 and 05 without having to use 03.1.1.2 BUT that WOULD
work....

Here, the SHORT files sort well automagically, so no changes
are needed.

Then (with the file manager) arrange the files so that:
 - the ALL and Fastest are in 03 (ALL sorts before FASTEST)
 - then the opens (IN 05 otherwise OPEN comes after 40)
 - then the age groups... (which sort nicely numerically)

like this

    2017/08_BuacInvite/
    ├── frontMatter.yaml
    └── results
        ├── long
        │   ├── 03
        │   │   ├── 2017-Uni-Invite-7.5km-ALL.csv
        │   │   ├── 2017-Uni-Invite-7.5km-ALL-Female.csv
        │   │   ├── 2017-Uni-Invite-7.5km-ALL-Male.csv
        │   │   ├── 2017-Uni-Invite-7.5km-FASTEST-Female.csv
        │   │   └── 2017-Uni-Invite-7.5km-FASTEST-Male.csv
        │   ├── 05
        │   │   ├── 2017-Uni-Invite-7.5km-OPEN-Female.csv
        │   │   └── 2017-Uni-Invite-7.5km-OPEN-Male.csv
        │   └── 07
        │       ├── 2017-Uni-Invite-7.5km-40plus-Female.csv
        │       ├── 2017-Uni-Invite-7.5km-40plus-Male.csv
        │       ├── 2017-Uni-Invite-7.5km-50plus-Female.csv
        │       ├── 2017-Uni-Invite-7.5km-50plus-Male.csv
        │       └── 2017-Uni-Invite-7.5km-60plus-Male.csv

    
So this script will copy any files in some
FLAT_SAVE_AS_FOLDER  over the top of any files with the same
basename that it finds already nicely sorted in the YEAR
folder.

It will also warn if  
 - you have the same file in 2 folders,
 - any files in from folder do NOT have a place in the
 SORTED tree
 - any files in SORTED tree do NOT have a replacement in the
 Flat folder

ALSO in the 'preparation' stage you might want to commit
these files.  AND you are worried they MIGHT accidently get
published to main site SO make them all empty (File Size of
                                               0 bytes)

They wont be processed (by csv2jekyll.rb) if they are sero
size.  On my system I can ``` find 2017/08_BuacInvite/ -name
*csv -exec cp /dev/null {} \; ``` to make all the files
empty. YMMV

# ON RACE DAY 

 - delete ALL the tempory practice files from the SAVEAS
 folder
 - Save the NEW GOOD data in there
 - Run this script with the FROM and TO folders 
 - then process into jekyll with tools/csv2jekyll.rb


Usage: #{$0} FROM_DARREN SORTED_TOP

EOD
}

optparser.parse!

if ARGV.length <2 
  puts optparser
  exit
else
  flat_folder = ARGV[0]
  to_folder = ARGV[1]
  flatsFiles = { }
  Dir["#{flat_folder}**/*csv"].each{|fn| flatsFiles[File.basename(fn)]=fn}
  
  sortedFiles = Hash.new {|h,k| h[k]=Array.new}
  Dir["#{to_folder}**/*csv"].each{|fn| 
    sortedFiles[File.basename(fn)] << fn
  }
  
  stopOnMultipleSorted = false
  sortedFiles.each{|bn,list|
    if list.length>1
      puts "WARNING #{bn} appears #{list.length} "+list.pretty_inspect
      stopOnMultipleSorted = true
    end
  }
  if stopOnMultipleSorted
    puts "NOT COPYING: too many versions of same file"
    exit
  end

  # puts "#{flatsFiles.keys.length} Flat files from Timing System: "+ flatsFiles.pretty_inspect
  # puts "#{sortedFiles.keys.length} Sorted files from #{to_folder}: "+ sortedFiles.pretty_inspect

  copied = []
  flatsFiles.each{|bn,from_fn|
    if(sortedFiles.member? bn)
      sortedFiles[bn].each {|dest_fn|
        # puts "Copying #{from_fn} to #{dest_fn}"
        FileUtils.cp(from_fn,dest_fn)
      }
      copied<<bn
    else
      puts "WARNING: #{bn} is NOT in the sorted list. Put it in manually now."
    end
  }
  sortedFiles.each{|bn,sorted_fn|
    if (copied.member? bn)
      # puts "Its OK #{bn} was copied to #{sorted_fn}"
    else
      puts "WARNING: #{sorted_fn} exists but there is no version in #{flat_folder}"
    end
  }
  puts "#{flatsFiles.keys.length} Flat files in Timing System folder #{flat_folder}"
  puts "#{sortedFiles.keys.length} Sorted files in #{to_folder}"
  
end

