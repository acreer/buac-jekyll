require 'find'

Find.find('raceDataCSV'){|path|
  next unless path =~ /\.csv$/
  content = File.read(path)
  if content.sub!(/\+ or -,\+ or -/ , "\+ or -,Amount")
    puts "changed #{path}"
    File.write(path,content)
  end
}
