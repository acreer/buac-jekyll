require 'yaml'
require 'pp'
require 'active_support/core_ext/hash/keys.rb'
require 'chronic'
require 'slugify'
require 'fileutils'
require 'optparse'



optparser = OptParse.new{|p|
  p.banner= <<-EOD

So, you need a tree structure like

raceDataCSV/2018/
├── 01_college-classic
├── 02_mandurang-meander
├── 03_landry-lope
├── 04_rifle-range-rattle
├── 05_crusoe-crusade
├── 06_ham-street-hustle
├── 07_reservoir-ramble
├── 08_sandhurst-slog
├── 09_pearces-road-rally
├── 10_bendigo-university-ac-invite
├── 11_junortoun-jog
├── 12_piepers-plunder
├── 13_hornets-hideaway
├── 14_notleys-noodle
├── 15_kangaroo-flat-falter
├── 16_rocky-rises
└── 17_club-mystery-run

To save the handicap files and results files to generate the website.

Each folder needs a 
frontMatter.yaml 

    ---
    :number: 1
    :date: March 17 2018
    :name: College Classic
    :runMD: college-classic.md

that says 'this folder needs processing'

This script scans the _runs/*md files, 
- sorting by date
- keeping type:club runs and the *uni-invite*.md or half-marathon
- and counts them and makes the folders and the frontmatter.yaml file.

Usage: ruby #{$0} DOIT

EOD
}

class KeywordStruct < Struct
  def initialize(**kwargs)
    super(kwargs.keys)
    kwargs.each { |k, v| self[k] = v }
  end
end


class FileWithYaml <  KeywordStruct.new(:filename, :front_matter)
end

optparser.parse!

if ARGV.length < 1
  puts optparser
  exit
end

runs_folder = './_runs'
race_data_folder = './raceDataCSV'

filesWithYAML = Dir["#{runs_folder}/*md"].map!{|md_fn|
    front_matter = YAML.load(File.read(md_fn))
    FileWithYaml.new(
      filename:md_fn,
      front_matter: front_matter.deep_symbolize_keys!
    ) 
}
.sort_by {|o| 
  puts "HERE #{o.filename} #{o.front_matter[:date]}"
  str = o.front_matter[:data]
  dt = Chronic.parse(o.front_matter[:date])
  raise "Can't parse date '#{str}' in #{o.filename}: "+
        o.front_matter.pretty_inspect unless dt;
  o.front_matter[:chronic] = dt
  # puts dt.strftime('%s')
  dt.strftime('%s')
}
.keep_if{|o| o.front_matter[:isCurrent] }
.keep_if{|o| o.front_matter[:type].downcase == 'club run' || 
      o.filename =~ /uni-invite/ ||
      o.filename =~ /half-marathon/ 
}

count = 0
filesWithYAML.each{|fwy|
  count+=1
  year = fwy.front_matter[:chronic].strftime('%Y')
  f_count =  '%02d' %count
  # pp fwy
  name_slug = fwy.front_matter[:title].slugify
  fn = File.join(race_data_folder,year,"#{f_count}_#{File.basename(fwy.filename,'.md')}",'frontMatter.yaml')
  yaml_hash ={
    number: count,
    date: fwy.front_matter[:chronic].strftime('%F'),
    name: fwy.front_matter[:title],
    runMD: File.basename(fwy.filename),
  }
  if !File.exists? fn
    puts "#{year}/#{f_count} #{fwy.front_matter[:date]} #{fwy.front_matter[:chronic]} #{fwy.filename}"
    puts "Making #{fn} with \n#{yaml_hash.to_yaml}"
    FileUtils.mkdir_p(File.dirname(fn))
    File.open(fn,'w'){|fh| fh.puts yaml_hash.to_yaml }
  else
    puts "File #{fn} already exists"
  end
};


