
#######
# 
# Just paste these ruby commands below into a power shell in order till until
# one works...  You can tell it works if it prints out a home folder name that
# you can find
# 
# 
# ruby -r etc -e 'puts Etc.getpwuid'
# 
# ruby -e "puts ENV['HOME']"
# 
# ruby -e "puts ENV['USERPROFILE']"
# 
# ruby -e "puts ENV['HOMEPATH']"
# 
# ruby -e "puts ENV['HOMEDRIVE']+ENV['HOMEDIR']"
# 
# 
# THEN make a new text file in sublime that looks like
# 
# machine ftp.bendigouniathsclub.org.au
#   login  webmaster@bendigouniathsclub.org.au
#   password NOT_PUTTING_PASSWORD_HERE
# 
# and save it into the folder that home folder
# 
# Then the upload script will basically replace haveing to use filezilla It
# knows where to find the _site/ac files and where to put them, andit will
# replace any that have a different size.
# 
# It can be a little slow at the start, as it has to check all the folders
# first.
#
# It prints out any file that it uploads, along with the local and remote size.
#
######


require 'pp'
require 'net/netrc'
require 'net/ftp'

local_files = Dir.glob('_site/ac/**/*').sort
if ARGV.length> 0
    local_files.keep_if{|f| f== ARGV[0]}
    puts "YO" + local_files.pretty_inspect
end

machine = 'ftp.bendigouniathsclub.org.au'
rc = Net::Netrc.locate(machine) or 
  raise "No .netrc containing '#{machine}' creds. Looking in" + [
    ENV['HOME'],
    ENV['USERPROFILE'],
    ENV['HOMEPATH'],
    ENV['HOMEDRIVE'].to_s + ENV['HOMEDIR'].to_s
].pretty_inspect
puts "login to #{rc.machine} with #{rc.login} in #{Net::Netrc.rcname}"

remote_root = 'ac/'

Net::FTP.open(rc.machine, rc.login, rc.password) do |ftp|
  local_files.each do |name|
    remote = name.sub('_site/','')
    if File::directory? name
      # puts "Dir #{name} #{remote}"
      begin
        ftp.mkdir remote
      rescue Net::FTPPermError => e
        if e.message =~ /File exists/
        else
          raise e
        end
      end
    else
      
      begin 
        remote_size = ftp.size(remote)
      rescue Net::FTPPermError => e
        if e.message =~ /No such file or directory/
          remote_size = 0
        end
      end
      local_size = File.size(name)
      if local_size == remote_size
        # puts "file #{name}:#{local_size} remote #{remote}:#{remote_size}"
      else
        puts "Upload #{local_size}:#{remote_size} #{name}"
        File.open(name) { |file| ftp.putbinaryfile(file, remote) }
      end
    end
  end
end
# pp local_files
