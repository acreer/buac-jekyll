# AB Handbook has pdf table, but the data is in columns not rows.
# If you paste it....

require 'pp'
require 'yaml'
require 'time'

races = %(
RACE 1
RACE 2
RACE 3
RACE 4
RACE 5
RACE 6
RACE 7
RACE 8
RACE 9
RACE 10
RACE 11
RACE 12
FINAL
).split("\n").reject {|i| i.empty?}

dates = %(
OCTOBER 25, 2018
NOVEMBER 8, 2018
NOVEMBER 22, 2018
DECEMBER 6, 2018
DECEMBER 20, 2018
DECEMBER 28, 2018
JANUARY 10, 2019
JANUARY 17, 2019
JANUARY 31, 2019
FEBRUARY 7, 2019
FEBRUARY 21, 2019
FEBRUARY 28, 2019
MARCH 8, 2019
).split("\n").reject {|i| i.empty?}

distances = %(
800m
1000m
1600m
1000m
800m
1000m
800m
1000m
1600m
1000m
1000m
800m
1000m
).split("\n").reject {|i| i.empty?}

# puts "distances #{distances.length}" + distances.pretty_inspect

raise "Bad numbers" unless distances.length == dates.length && dates.length == races.length

table = distances.each_with_index.map{|d,i| {distance: d, date: dates[i], race: races[i] }}

table.each{|week|
  race = week[:race]
  date = Time.parse("#{week[:date]} 19:15:00")
  distance = week[:distance]
  fn = "_runs/summer_thursday_#{week[:race]}.md".gsub(' ','_').downcase
  front_matter = {
    'title'=> "Flack Advisory #{distance} Thursday",
    'distances'=> distance,
    'date'=> date.strftime('%F %T'),
    'isCurrent'=>true,
    'type'=>'AB run',
    'location'=>{
      'text'=>'Tom Flood Sports Centre',
      'href'=>'https://www.bendigo.vic.gov.au/Things-To-Do/sports-venues/tom-flood-sports-centre'
     },
    'setter'=>{
      'text'=>'Athletics Bendigo',
      'href'=>'https://www.athleticsbendigo.org.au/'
     },
  }

  content = [
    YAML.dump(front_matter),
    '---',
    '',
    "{{ page.date |date: '%A %-d %B %Y' }}",
    '',
    "[{{page.location.text}}]({{page.location.href}})",
    '',
    "Distance: #{distance}",
    '',
    %(
The intended start time of the Flack Advisory Distance Series event is 7.15pm. 

$30.00 is given to the winner of each heat, $10.00 for 2nd place & $5.00 for
third. $5.00 for fastest time of the night!

Points are awarded on each night; 8 for 1st, 7 for 2nd, 6 for 3rd & all other
finishers get 5 points! Double points awarded for the Xmas Carnival 1000 metres
on 28th December! A season aggregate is conducted with the winner being the
person with the most points from all races, excluding the final.

The top fifteen point’s scorers then go through to the series final over 1000 metres at 7.15pm on the Friday 8th
March, 2019. Athletes must finish a minimum 6 races to be considered for the final. (*unless organizers feel the need
to reduce this number). The winner of the final receives a sash & trophy, with 2nd & 3rd a trophy. Prize money is also
available in the final.

The John Burke trophy is given to the person considered the Most Consistent athlete! All athletes must be 14yo+,
unless AV registered! All athletes must wear a singlet/shirt under there given race number!

*Organizers reserve the right to increase the number of runners in the final if they feel the need!

*For insurance purposes, all athletes must be Aths.Victoria or Bendigo Athletics Club members!

Enquiries to: Kel Pell 54433911 or Greg Hilson 0418-590804

Sponsor [Flack Advisory](http://www.fasg.com.au/)
)


  ].join("\n")

  File.write(fn,content)
}
