require 'nokogiri'
require 'pp'
require 'yaml'
require 'fileutils'
require 'csv'

def snakecase(str)
  str
    .strip
    .gsub(/[^0-9a-z ]/i,'')
    .gsub(/\s/, '-')
    .downcase
end

def cleancell(str)
  str
    .delete("\t")
    .delete("\n")
    .gsub(/\s+/,' ')
end

def rows2md(rows)
  count = 0
  lines = []
  rows.each do |r|
    lines << r.join('|')
    lines << r.map { |_c| '-------' }.join('|') if count == 0
    count += 1
  end
  lines
end

base = 'https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020/events/'

tmp = '/tmp/virtual'
FileUtils.mkdir_p(tmp)

puts "cd #{tmp}"
puts 'for e in $(seq 1 34);do echo Event $e;curl  -sO https://www.multisportaustralia.com.au/races/bendigo-uni-athletics-club-2020-spring-series-2020/events/$e ;done'
puts "cd #{ENV['PWD']}"

(1..34).each do |e|
  fn = File.join(tmp, e.to_s)
  puts "Event #{e} #{fn}"

  document = Nokogiri::HTML.parse(File.read(fn))
  title =  document.title.split('-')
  # puts title.pretty_inspect
  # Event 16 is not a race....
  next if title[0] =~ /server error/i

  kms = title[1][/(\d+)/, 1].to_i
  len = case kms
        when 1
          'SHORT'
        when 2..5
          'MEDIUM'
        else
          'LONG'
        end
  csvfn = File.join(
    'raceDataCSV',
    '2020',
    snakecase(title[0]),
    "#{snakecase(title[0])}_results_#{len}.csv"
  )

  rows = document.search('tr').each.to_a.map do |row|
    row.search('td, th').map { |c| cleancell(c.text.strip) }
  end.reject { |r| r[0] == 'NYS' }

  FileUtils.mkdir_p(File.dirname(csvfn))
  File.write(csvfn, rows.map(&:to_csv).join('')) if rows.length > 1

  racedate = case csvfn
             when /notley/
               '2020-09-01'
             when /crusoe-crusade/
               '2020-09-02'
             when /mandurang/
               '2020-09-03'
             when /rocky/
               '2020-09-04'
             when /rifle/
               '2020-09-05'
             when /college/
               '2020-09-06'
             when /sandhurst/
               '2020-09-07'
             when /kangaroo/
               '2020-09-08'
             when /hornet/
               '2020-09-09'
             when /pioneer/
               '2020-09-10'
             else
               puts "WARNING #{csvfn} needs an unique date"
               '2020-09-05'
             end

  runMD = "#{snakecase(title[0])}.md"
          .tr('’', '')
          .tr("'", '')
          .sub('-kennington', '')
          .sub('-crusoe', '')
          .sub(/-kangaroo-flat.md$/, '.md')
          .sub(/.*pioneer.*/, 'guys-hill-heartbreak.md')
          .sub(/-mandurang/, '')

  raise "No md '_run/#{runMD}'" unless File.exist? File.join("_runs/#{runMD}")

  File.write(
    File.join(File.dirname(csvfn), 'frontMatter.yaml'),
    [
      YAML.dump(
        ':number' => 1,
        ':date' => racedate,
        ':name' => title[0].strip.tr('’', "'"),
        ':runMD' => runMD
      ),
      '---'
    ].join("\n").delete('"')
  )
end
