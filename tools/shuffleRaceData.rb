require 'optparse';
require 'pp'
require 'fileutils'
require 'naturally'
require 'yaml'

class PositionInfo
  def initialize(folder)
    folder_fixed = folder.sub(%r{/$},'')
    matches = File.basename(folder_fixed).scan(/(\d+)_(.*)/)
    raise "cannot get number and race from #{folder}" if matches.length ==0
    @folder = folder_fixed
    @position = matches[0][0].to_i
    @race_name =  matches[0][1]
  end
  def shuffle_down(new_position=nil)
    new_position = position -1 if new_position.nil?
    return File.join File.dirname(@folder),sprintf('%02d',new_position)+"_#{race_name}"
  end
  attr_reader :folder,:position,:race_name
end

opts = Hash.new
optparser = OptParse.new{|p|
  p.banner= <<-EOD

So in 2012, the folder numbers are all screwed up

  01_CollegeClassic
  02_ApolloHillAttack
  03_CedarDriveDash
  04_DiamondHillDash
  05_LandryLope
  06_RifleRangeRattle
  07_JunortounJog
  08_PearcesRoadRally
  09_AbChamps
  10_MandurangMeander
  11_PicaninnyPlod
  12_CrusoeCrusade
  13_KangarooFlatFalter
  14_CousinsStreetClash
  15_BuacHalfMarathon
  16_ClubMysteryRun
  17_Buacinvitation


The club invite at 17 should have been after the CedarDriveDash do all the
numbers need to increase.



EOD
  p.on('-m','--move folder','The folder to move'){|o|opts[:move] = o}
  p.on('-a','--after folder','The folder to insert after'){|o|opts[:after] = o}
}

optparser.parse!

puts optparser unless opts[:move] && opts[:after]

raise "Must be in same folder" unless File.dirname(opts[:move]) == File.dirname(opts[:after])

after = PositionInfo.new opts[:after]
move = PositionInfo.new opts[:move]

pp move

orig = Naturally.sort(Dir["#{File.dirname(move.folder)}/**"])
  .collect{|folder| PositionInfo.new(folder) }
  .each{|this_folder|
    if this_folder.position <= after.position
      puts "# Leave #{File.basename this_folder.folder}"
    else
        new_pos = this_folder.folder == move.folder ? after.position+1 : this_folder.position+1
        yaml_fn = File.join(this_folder.folder,'frontMatter.yaml')
        raise "Can't find yaml file #{yaml_fn}" unless File.exists? yaml_fn
        yaml = YAML.load(File.read yaml_fn)
        yaml[:number] = new_pos
        File.write(yaml_fn,yaml.to_yaml)
        cmd =  "git mv #{this_folder.folder} #{this_folder.shuffle_down(new_pos)}"
        puts cmd
        system cmd
    end
}





