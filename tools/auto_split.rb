
require 'pp'
require 'csv'

def underscore(camel_cased_word)
     camel_cased_word.to_s.gsub(/::/, '/').
       gsub(/([A-Z]+)([A-Z][a-z])/,'\1_\2').
       gsub(/([a-z\d])([A-Z])/,'\1_\2').
       tr("-", "_").
       downcase
end

def save_lines(lines,filename)
  puts "save #{filename}\n"+lines.pretty_inspect
  CSV.open(filename,'w'){|csv|
    lines.each{|l| csv<<l}
  }
end

fn = ARGV.shift

new_bn = underscore(ARGV.join '-')



collecting = []
new_fn = ''
CSV.foreach(fn){|r|
  next if r[0].strip == ''
  next if r[0].ord ==160 # nbsp?

  if r[0]==r[1]
    save_lines(collecting,new_fn) if collecting.length>0
    collecting=[]
    # puts "split '#{r[0]}' '#{r[0].ord}'"

    fn_ex = underscore(r[0]
      .gsub(/\s/,'-')
    )

    new_fn = File.join(
      File.dirname(fn),
      underscore(
        "#{new_bn}-#{fn_ex}.csv"
        .gsub('+','-plus')
        .gsub(/[-_]+/,'_')
        .gsub('women','female')
        .sub(/^_/,'')

      )
    )
    # puts "cp #{fn} #{new_fn}"
  else
    collecting << r
  end

}


save_lines(collecting,new_fn)
