require 'yaml'
require 'pp'
require 'time'

def bad_array_length(reference,subject,name)
  if reference.length != subject.length
    puts "The number of #{name}(#{subject.length}) is not the same as the number of dates(#{reference.length})" 
  exit
  end
end

dates =%(
Oct 6 2018
oct 13 2018
Oct 20 2018
Oct 27 2018
Nov 10 2018
Nov 17 2018
Dec 1 2018
Dec 15 2018
Jan 11 2019
Jan 19 2019
Feb 2 2019
Feb 9 2019
Feb 23 2019
).split("\n").reject {|i| i.empty? }

rounds=(1..12).map{|i| "Round #{i}"}.push("AV Sheild Final")

programs =(1..12).map{|i| "Program #{(i % 2) +1}"}.push("Program 2")

distances = [

%(
1500m/3000m Walk 
2x2 400m Relay 
Pole Vault
).split("\n").reject {|i| i.empty? },

%(
1500m/3000m Walk 
3000m/5000m
Non Scoring Relays Swedish
).split("\n").reject {|i| i.empty? },

%(
1500m/3000m Walk 
4x400m Relay 
).split("\n").reject {|i| i.empty? },

%(
2000m/5000m Walk 
2000m/3000m Steeple
4x100m Relay
).split("\n").reject {|i| i.empty? },


%(
1500m/3000m Walk 
4x200m Relay 
).split("\n").reject {|i| i.empty? },

%(
1500m/3000m Walk 
3000m/5000m
4x100m Relay
).split("\n").reject {|i| i.empty? },

%(
1500m/3000m Walk 
Pole Vault
Non Scoring Relays Medley
).split("\n").reject {|i| i.empty? },


%(
2000m/5000m Walk 
4x100m Relay
2000m/3000m Steeple
).split("\n").reject {|i| i.empty? },

%(
1500m/3000m Walk 
4x200m Relay
Ern Hammer 3000m Handicap
).split("\n").reject {|i| i.empty? },


%(
1500m/3000m Walk 
3000m/5000m
4x100m Relay
).split("\n").reject {|i| i.empty? },


%(
1500m/3000m Walk 
4x400m Relay
Pole Vault
).split("\n").reject {|i| i.empty? },


%(
1500m/3000m Walk 
4x400m Relay
2000m/3000m Steeple
).split("\n").reject {|i| i.empty? },

[]
]

bad_array_length(dates,rounds,"Rounds")
bad_array_length(dates,programs,"Programs")
bad_array_length(dates,distances,"Distances")


table = dates.each_with_index.map {|d,i|
  {
    date: d,
    round: rounds[i],
    distance: distances[i]
  }
}

# pp table

table.each_with_index{|week,i|
  round = week[:round]
  date = Time.parse("#{week[:date]} 14:00:00")
  race = i+1
  distance = week[:distance]
  fn = "_runs/summer_saturday_#{race}.md".gsub(' ','_').downcase
    location = {
      'text'=>'The Track, Retreat Rd Flora Hill',
      'href'=>'https://www.athleticsbendigo.org.au/the-complex-lubac'
     },
    setter = {
      'text'=>'Athletics Bendigo',
      'href'=>'https://www.athleticsbendigo.org.au/'
     },
     if i==12
         location = 'Lakeside Stadium, Aughtie Drive Albert Park'
         setter={'text'=>'Athletics Victoria','href'=>'http://athsvic.org.au/'}
     end
  
  front_matter = {
    'title'=> "Bendigo Saturday Athletics #{round}",
    'distances'=> distance.join(' '),
    'date'=> date.strftime('%F %T'),
    'isCurrent'=>true,
    'type'=>'AB run',
    'location'=>location,
    'setter'=>setter,
  }

  content = [
    YAML.dump(front_matter),
    '---',
    '',
    "{{ page.date |date: '%A %-d %B %Y' }}",
    '',
    "[{{page.location.text}}]({{page.location.href}})",
    '',
    "Feature races:",

    distance.map{|d| "* #{d}"},

    "","Check [Aths Bendigo](https://www.athleticsbendigo.org.au/) for more details"


  ].join("\n")

  File.write(fn,content)
}
