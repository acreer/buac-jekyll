# encoding: ISO-8859-1

require 'pp'
require 'find'
require 'yaml'
require 'csv'
require 'fileutils'
require 'naturally'
require 'front_matter_parser'
require 'gender_detector'
require 'chronic_duration'

@gender_detect = GenderDetector.new(case_sensitive: false)
@known_male = [
  'Tracy Wilson',
  'Tracey Tucker',
  'Callum Tucker',
  'Kaidyn Tucker',
  'Floyd Cartner'
]
@known_female = [
  'Tarli Bird',
  'Georgie Lanyon',
  'Ivie Safstrom',
  'Ivie Safstrom (#239)'
]

# From error messages below.  Watch out if you change id_str
@known_no_female_races = [
  'run page for Mystery Run (Mandurang) 2009 results medium csvs 1',
  'run page for Half Marathon Festival 2015 results medium csvs 2'
]

# Standalone method
def stringify(obj)
  if obj.is_a? Hash
    return obj.reduce({}) do |memo, (k, v)|
      memo.tap { |m| m[k.to_s] = stringify(v) }
    end
  end

  if obj.is_a? Array
    return obj.each_with_object([]) do |v, memo|
      memo << stringify(v)
    end
  end

  obj
end

def csv2md(fn)
  count = 0
  lines = []
  CSV.foreach(fn, encoding: 'ISO-8859-1') do |r|
    lines << "|#{r.collect { |i| i }.join('|')}|"
    lines << "|#{r.collect { |_i| '------' }.join('|')}|" if count == 0
    count += 1
  end
  lines
end

def get_key(table, column, fn)
  column_key = nil
  trys =
    case column
    when :gender_name
      %i[firstname first_name first name]
    when :first
      %i[first firstname first_name]
    when :last
      %i[last lastname last_name surname]
    when :time
      %i[time net_time net gun_time]
    else
      raise "Cannot deal with finding column '#{column}' in #{fn}"
    end

  trys.each do |try|
    return column_key = try if table.headers.member? try
  end
  raise "Could not get #{column} using #{trys} from #{table.headers} in #{fn}"
end

# lots of combinations of name first firstname etc.
# make each row in table have a :gender :fullname :time
def csvStandard(fn)
  list = CSV.read(fn, encoding: 'ISO-8859-1', header_converters: :symbol, headers: true)
  gender_key = get_key(list, :gender_name, fn)
  fullname_keys = case gender_key
                  when :name
                    [:name]
                  else
                    [get_key(list, :first, fn), get_key(list, :last, fn)]
  end
  time_key = get_key(list, :time, fn)
  list.map do |r|
    gen_name = r[gender_key].strip
    gen_name = gen_name.split(/\s/)[0].strip if gender_key == :name
    gen = @gender_detect.get_gender(gen_name, :great_britain)
    # puts  "Gender #{gen} for '#{gen_name}' from '#{r[gender_key]}' in #{fn}"
    # puts  "Could not decide about '#{gen_name}' from '#{r[gender_key]}' in #{fn}" if gen==:andy
    r[:gender] = gen
    r[:fullname] = fullname_keys.map { |k| r[k].strip }.join(' ')
    r[:gender] = :male if @known_male.member? r[:fullname]
    r[:gender] = :female if @known_female.member? r[:fullname]
    r[:chronicDuration] = ChronicDuration.parse(r[time_key])
    r[:time] = r[time_key].gsub(/^0/, '').gsub(/^:/, '')
  end
  # puts "Read #{fn} into " + list.to_csv
  list
end

def overWriteIfChanged(fn, content, opts = { quiet: false })
  existing_content = nil
  existing_content = File.read(fn) if File.exist? fn

  # Alter the yaml to remove quotes from buacRaceNum: '08'
  # content.sub!(/^buacRaceNum:(\s+)'(\d+)'$/,%q{buacRaceNum:\1\2})

  # Alter the yaml to add in the quotes
  content.sub!(/^buacRaceNum:(\s+)(\d+)$/, %q(buacRaceNum:\1'\2'))

  # Remove 160.chr (non breaking space.)
  # content.gsub!(160.chr,' ');
  if content == existing_content
    # puts "No change in '#{fn}'"
  else
    verb = 'Creating'
    verb = 'Updating' if File.exist? fn
    msg = "#{verb} '#{fn}'" unless opts[:quiet]
    FileUtils.mkdir_p(File.dirname(fn))
    File.open(fn, 'w') { |f| f.write(content) }
    return msg
  end
end

output_top = nil

# output_top ||= ARGV[0]
# unless output_top
#   %W{
#     ../uni-newsite/uni-jekyll/
#     ../buac-jekyll
#   }.each{|try| output_top = try if File.exists? try}
# end

output_top = './' if output_top.nil?

# This started with trying to decipher filenames from Brads Website
# Now that Darren sends us files like 05_Crusoe_Crusade_handicaps_LONG.csv
# we can tell what is the race number, what is the distance and what is the type.
# so we dont need to use the tree structure so much.
#
winnersList = Hash.new { |h, k| h[k] = [] }

Find.find('./raceDataCSV/') do |p|
  next unless p =~ /frontMatter.yaml/

  # next unless p =~ /2015.*Half/
  # next unless p =~ /2020/
  # puts "YAML LOAD #{p} #{ File.read(p) }"
  yaml = YAML.load(File.read(p))
  race_f = File.dirname(p)
  # puts "In race #{race_f}"+yaml.pretty_inspect

  races_with_front_matter = Hash.new { |h, k| h[k] = {} }
  %w[results handicaps].each do |type|
    %w[long medium short].each  do |length|
      csvs = []
      mds = []
      old_style_folder = File.join(race_f, type, length)
      # puts "Checking for old style/complicated #{type} folder '#{old_style_folder}'"
      if File.exist?(old_style_folder)
        # puts "Looking in #{race_f}/#{type}/#{length} folders."
        auto_glob = File.join(race_f, type, length, '**/*csv')
        fixed_glob = File.join(race_f, type, length, '**/*_FIXED_*csv')
        fixed_csvs = Dir[fixed_glob]
        auto_csvs = Dir[auto_glob]
        csvs = auto_csvs
        csvs = fixed_csvs unless fixed_csvs.empty?
        # puts "Found these csv files "+csvs.pretty_inspect
        mds = Dir[File.join(race_f, type, length, '**/*md')]
      else
        # puts "After new website settles, look for sensible #{type} names from Darren"
        glob = File.join race_f, "**/*#{type}_#{length.upcase}.csv"
        # puts "GLOB #{glob}" if race_f =~/2020/
        csvs = Dir[glob]
        # pp csvs
        mds = Dir[File.join race_f, "**/*#{type}_#{length.upcase}.md"]
        # puts "glob #{glob}"+csvs.pretty_inspect
      end

      csvs.delete_if { |fn| File.size(fn) == 0 } # Zero length place holders.

      # puts "Type #{type} length#{length} csvs" + csvs.pretty_inspect
      next unless !csvs.empty? || !mds.empty?

      csvs = Naturally.sort(csvs)
      date = Date.parse(yaml[:date])
      if type == 'handicaps'
        date -= 5 # days
      end
      bns = [
        date.strftime('%F'),
        # date.strftime('%Y'),
        yaml[:name].gsub(/\s+/, '-'),
        length.capitalize,
        type.capitalize
      ]
      races_with_front_matter[type][length] = { csvs: csvs, bns: bns, mds: mds }
    end
  end

  races_with_front_matter.each do |type, lengths|
    lengths.each do |length, post_stuff|
      title = post_stuff[:bns].join('-').downcase
      fn = File.join(race_f, title + '.md')
      # https://mmistakes.github.io/minimal-mistakes/docs/layouts/#custom-sidebar-navigation-menu

      date = Date.parse(yaml[:date])
      if type == 'handicaps'
        date -= 5 # days
      end

      post_front = {
        'title' => yaml[:name],
        'buacYear' => date.strftime('%Y').to_s,
        'buacRaceNum' => format('%02d', yaml[:number]).to_s,
        'buacSlug' => yaml[:name].gsub(/\s+/, '-').downcase,
        'buacLength' => length,
        'buacType' => type,
        #         'sidebar'=>[
        #           {
        #             'title'=> "Other #{type.capitalize}",
        #             'text'=>"[theage](http://www.theage.com.au)|#{link}"
        #           }
        #         ]
      }
      post_front['runMD'] = yaml[:runMD] if yaml[:runMD]

      titles = {}
      post_stuff[:csvs].each do |csv_fn|
        titles[csv_fn] = File.basename(csv_fn)
                             .sub(/_#{type}_#{length}/i, '')
                             .sub(/\.csv/, '')
                             .sub(/_auto__tb\d+_\d+/, '')
                             .split(/_|-/)
                             .collect(&:capitalize)
                             .join(' ')
      end

      md_top_stuff = ''
      md_rest_stuff = ''
      Naturally.sort(post_stuff[:mds]).each do |mdfn|
        if File.basename(mdfn) =~ /top/i
          md_top_stuff += "\n\n#{File.read(mdfn)}\n\n"
        else
          md_rest_stuff += "\n\n#{File.read(mdfn)}\n\n"
        end
      end

      description = ''
      description = "\n <br>[Course Description and Results Summary]({% link _runs/#{yaml[:runMD]} %})" if yaml[:runMD]

      jekyll_fn = File.join(output_top, '_raceData', date.strftime('%Y').to_s, File.basename(fn))
      # puts jekyll_fn
      content = [
        YAML.dump(post_front),
        "---\n\n",
        '<h3>{{ page.buacLength | capitalize }} {{ page.buacType }}</h3>',
        "\n",
        "{{ page.date |date: '%A %-d %B %Y' }}",
        description,
        "\n",
        '{% include raceDataLikeThis.html %}',
        "\n",
        md_top_stuff,
        "\n\n",
        post_stuff[:csvs].collect do |csv|
          [
            (post_stuff[:csvs].length > 1 ? "\n\n##### #{titles[csv]}\n\n" : ''),
            csv2md(csv).join("\n"),
            "\n" + '{: .tablesorter}',
            "\n\n"
          ].join
        end,
        "\n\n",
        md_rest_stuff,
        '{% include raceDataLikeThis.html  %}',
        "\n\n"
      ].join('')

      FileUtils.mkdir_p(File.dirname(jekyll_fn))
      overWriteIfChanged(jekyll_fn, content)

      # Now make a data file
      next unless type == 'results'

      year = date.strftime('%Y')
      run_md_id_str = "run page for #{yaml[:name]} #{year} #{type} #{length} csvs #{post_stuff[:csvs].length}"
      if md = yaml[:runMD]
        # puts "Data for #{run_md_id_str}"
        csv_fn = nil
        if post_stuff[:csvs].length == 1
          csv_fn = post_stuff[:csvs][0]
        else
          # Special lookups
          list = post_stuff[:csvs]
          if md == 'uni-invite.md'
            # puts "INV #{yaml[:runMD]} " + list.pretty_inspect
            csv_fn = if list[0] =~ /__tb\d+_/ # 03Invitation4_auto__tb13_1.csv
                       list.max_by do |fn|
                         m = fn =~ /__tb(\d+)_/
                         m[1].to_i
                       end
                     else
                       list.select { |fn| fn =~ /all/i }
                           .min_by(&:length)
                       # puts "AHHHHH #{csv_fn}"
                     end
          elsif md == 'half-marathon-festival.md'
            # puts "HMF "+list.pretty_inspect
            csv_fn = list.select { |fn| fn =~ /all|complete|open/i }
                         .min_by(&:length)
            if length == 'long'
              csv_fn = list.select { |fn| fn =~ /all|complete/i }
                           .select { |fn| fn =~ /21/ }
                           .min_by(&:length)
              csv_fn = nil if csv_fn =~ /MALE/i
            end
            # puts "HMF CHOOSE #{length} #{csv_fn}" if csv_fn
          end
        end

        if csv_fn
          gen_list = csvStandard(csv_fn)
          # puts gen_list.to_csv
          sorted_list = gen_list.sort_by { |i| i[:chronicDuration] }
          male_list = sorted_list
                      .select { |l| l[:gender] == :male || l[:gender] == :mostly_male }
          raise "No males in #{csv_fn} for #{run_md_id_str}" if male_list.empty?

          female_list = sorted_list
                        .select { |l| l[:gender] == :female || l[:gender] == :mostly_female }
          raise "No females in for '#{run_md_id_str}' #{p}" if female_list.empty? &&
                                                               male_list.length != gen_list.length && !@known_no_female_races.member?(run_md_id_str)

          winnersList[yaml[:runMD]].push(
            runMD: yaml[:runMD],
            runnerCount: gen_list.length,
            year: year.to_i,
            type: type.gsub(/^0/, '').gsub(/^:/, ''),
            length: length,
            winner: gen_list[0].to_hash,
            fast_male: male_list[0]
              .to_hash,
            fast_female: !female_list.empty? ? female_list[0]
              .to_hash : {}
          )
        else
          # puts "Can't deal with #{run_md_id_str}"
        end

      else
        # puts "No runMD frontMatter page for #{yaml[:name]} #{year} #{type} #{length}"
      end
    end
  end
end

# Also similar but different process for aggregates.
# we need to update 'current' aggregates for the current year
# And put in the front matter similar to above.
#
# AND we need to sort them naturally so early races dont overwrite newer ones.

yamls = []
Find.find('./raceDataCSV') do |p|
  next unless p =~ /frontMatter.yaml/

  yamls << p
end
# puts "Found some yaml"+yamls.pretty_inspect

used_verbs = {}
Naturally.sort(yamls).each do |p|
  # puts "yaml read #{p}"
  yaml = YAML.load(File.read(p))
  race_f = File.dirname(p)
  # pp yaml
  date = Date.parse(yaml[:date])
  # puts "In race #{race_f}"+yaml.pretty_inspect
  test_string = " touch 02_club_champs_LONG.csv 01_club_champs_LONG.csv club_champs_LONG.csv adj_points_LONG.csv another_adj_points_LONG.csv ruby -e 'puts Dir[%q[*{adj_points_,club_champs_}LONG.csv]]'
"
  {
    speed: {
      glob: '{net_points_,speed_champs,speed_points_}'
    },
    club: {
      glob: '{adj_points_,club_champs,agg_points_}'
    }
  }.each do |type, type_info|
    %w[long medium short].each do |length|
      glob = File.join race_f, "#{type_info[:glob]}#{length.upcase}*.csv"
      csvs = Naturally.sort(Dir[glob])
      # puts "glob #{glob} "+ csvs.pretty_inspect  if csvs.length>0
      next if csvs.empty?

      markdown_fn = File.join output_top,
                              '_aggregates',
                              date.strftime('%Y'),
                              "#{type}-#{length}.md"
      # puts "The file we overwrite/create #{markdown_fn}"
      dfmt = date.strftime('%Y')
      md_yaml = {
        'title' => "#{dfmt} BUAC #{type.capitalize}" \
                   " Champs #{length.capitalize}",
        'buacYear' => dfmt,
        'buacRaceNum' => format('%02d', yaml[:number]),
        'buacSlug' => yaml[:name].gsub(/\s+/, '-').downcase,
        'buacLength' => length,
        'buacType' => type.to_s
      }
      content = [
        YAML.dump(md_yaml),
        "---\n\n",
        '{% include aggregatesLikeThis.html %}',
        "\n\n",
        csvs.collect do |csv|
          [
            csv2md(csv).join("\n"),
            "\n" + '{: .tablesorter}',
            "\n\n"
          ].join
        end,
        "\n\n",
        '{% include aggregatesLikeThis.html %}',
        "\n\n"
      ]
      msg = overWriteIfChanged(markdown_fn, content.join(''), quiet: false)
      puts msg if msg =~ /creat/i
    end
  end
end

puts "Checked RaceData files in #{File.join output_top, '_raceData'}"
puts "Checked Aggregate files in #{File.join output_top, '_aggregates'}"

# puts 'Winners YAML' + winnersList.pretty_inspect

winnersList.each do |race_md, list|
  # puts "race_md #{race_md}"
  fn = File
       .join(
         '_data',
         'csv2jekyll',
         "#{File.basename(race_md, '.md')}.yml"
       )
  # pp by_year
  overWriteIfChanged(fn, YAML.dump(stringify(list)), quiet: false)
end
