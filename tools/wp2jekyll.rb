require 'json'
require 'pp'
require 'date'
require 'htmlentities'
require 'optparse'
require 'chronic'
require 'yaml'
require 'reverse_markdown'
require 'fileutils'
require 'open-uri'
require 'naturally'

# pp posts

options = {}
op = OptionParser.new do |parser|
	parser.banner="Usage: #{$0} [options]"
	parser.on('-a','--after SINCE',String,"Upload anything newer than"){|o| 
		options[:since] = Chronic.parse(o).to_date
		raise "Bad upload date #{o} " if options[:since].nil?
	}
end
op.parse!

unless options[:since]
	existing_posts = Naturally.sort Dir["_posts/*md"]
	if existing_posts.length>0
		options[:since] =  Date.parse(File.basename( existing_posts[-1]))
		puts "Using --since #{options[:since]} after #{existing_posts[-1]}"
	end
end

def decode(text)
	return HTMLEntities.new.decode text
end

def overWriteIfChanged(fn,content)
	existing_content = nil
	existing_content = File.read(fn) if File.exists? fn
	if content==existing_content
		# puts "No change in '#{fn}'"
	else
		verb = 'Creating'
		verb = 'Updating' if File.exists? fn
		puts "#{verb} '#{fn}'"
		FileUtils.mkdir_p(File.dirname(fn))
		File.open(fn,'w'){|f|f.write(content)}
	end
end

def self.sluggify(title)
	title.gsub(/[^[:alnum:]]+/, '-').downcase
end

def cache_json(type,url,id=nil)

	cache_fn =  File.join 'tmp',"CACHE_type#{type}"
	cache_fn = File.join cache_fn,"type-#{id}" if id

	if File.exists?(cache_fn) && (Time.now-File.mtime(cache_fn)) < 2 *60*60
		# if File.exists?(cache_fn)
		puts "Reading #{type} json from #{cache_fn}" if type == 'posts'
		return File.read(cache_fn)
	else
		puts "Getting #{type} data from #{url} into #{cache_fn}" if type=='posts'
		# puts "Getting #{type} data from #{url} into #{cache_fn}" 
		content = nil
    begin
      open(url) {|f|
        content = f.readlines.join('')
      }
      if (content)
        overWriteIfChanged(cache_fn,content)
        return content
      else
        puts "Could not get anything from #{url}"
        exit
      end
    rescue
      puts "Could not get #{type} data from #{url} into #{cache_fn}"
      puts $! if $!
      if File.exists? cache_fn
        puts "Using old json"
		    return File.read(cache_fn)
      else
      return {}.to_json
      end
    end
	end

end

def cache_media(id)
	return cache_json('media',"http://bendigouniathsclub.org.au/blog/wp-json/wp/v2/media/#{id}",id)
end

def cache_posts()
	# return cache_json 'posts',"http://bendigouniathsclub.org.au/blog/wp-json/wp/v2/posts"
	return cache_json 'posts',"http://bendigouniathsclub.org.au/blog/wp-json/wp/v2/posts?per_page=100&_embed=true"
end

def cache_user(id)
	return cache_json 'user',"http://bendigouniathsclub.org.au/blog/wp-json/wp/v2/users/#{id}",id
end

posts_json = cache_posts()

JSON.parse(posts_json).each{|post|
	date = Date.parse(post["date_gmt"])
	title = decode post["title"]['rendered']
	if options[:since] && date < options[:since]
		puts "Skipping (after #{options[:since]}) Post #{title} date #{date.strftime('%F')}"
		next
	end

	fn = File.join '_posts','blog',date.strftime('%Y'),sluggify("#{date.strftime('%F')} #{title}")+'.md'

	# Generate the content
	puts "Post #{title} date #{date.strftime('%F')}"
	yaml = {
		'title'=>title
	}
	if post['featured_media']>0
		media = JSON.parse(cache_media(post['featured_media']))
		# pp media
		# puts media['source_url']
		yaml['header']||={}
		yaml['header']['teaser']=media['source_url']
	end

	if post['author']
    yaml['author'] = post['_embedded']['author'][0]['name']
  else
    puts "No Author"
	end
	content = [
		YAML.dump(yaml),
		"---\n\n",
		ReverseMarkdown.convert( post['content']['rendered'])
    .gsub('&nbsp;',' ')
    .gsub(/<iframe/i,"\n\n<iframe"),
		"\n\n<!-- END -->\n"
	].join('')

	# puts "rm #{fn} # NOT saving #{fn} as it exists.  delete it if you want."
# 	if File.exists?(fn)
# 		if content.strip == File.read(fn).strip
# 			# puts "Unchanged content in #{fn}"
# 		else
# 			# puts "Would change Changed content in #{fn}"
# 			puts "rm #{fn} # if you want to rewrite this file as per the blog"
# 		end
# 	else
# 		overWriteIfChanged(fn,content)
# 	end

 		overWriteIfChanged(fn,content)

}
