require 'yaml'
require 'pp'
require 'time'

dates =%(
2nd October 2018
9th October 2018
16th October 2018
23rd October 2018
30th October 2018
6th November 2018
13th November 2018
20th November 2018
27th November 2018
4th December 2018
11th December 2018
18th December 2018
8th January 2019
15th January 2019
22nd January 2019 
29th January 2019
5th February 2019
12th February 2019
19th February 2019
26th February 2019
5th March 2019
12th March 2019
19th March 2019
26th March 2019
).split("\n").reject {|i| i.empty? }

distances=%(
3k
3k
3k
5k
3k
Melbourne Cup 3200m Handicap
Track Champs 10k
3k
3k
5k
3k
3k
3k
5k
3k
3k
Track Champs 5k
3k
3k
5k
3k
3k
5k
3k
).split("\n").reject {|i| i.empty? }

duties =%(
BYM
SBE
EH
UNI
Aths Bgo
BYM
SBE
EH
UNI
BYM
SBE
EH
BYM
SBE
EH
UNI
BYM
SBE
EH
UNI
BYM
SBE
EH
UNI
).split("\n").reject {|i| i.empty? }

raise "Count mis match" unless dates.length == distances.length && dates.length==duties.length

table = dates.each_with_index.map {|d,i|
  {
    date: d,
    duty: duties[i],
    distance: distances[i]
  }
}

# pp table
now =Time.now

table.each_with_index{|week,i|
  duty = week[:duty]
  date = Time.parse("#{week[:date]} 19:00:00")
  race = i+1
  distance = week[:distance]
  fn = "_runs/summer_tuesday_#{race}.md".gsub(' ','_').downcase
  front_matter = {
    'title'=> "Parker Electrical #{distance} Tuesday",
    'distances'=> distance,
    'date'=> date.strftime('%F %T'),
    'isCurrent'=>date>now,
    'type'=>'AB run',
    'location'=>{
      'text'=>'The Track, Retreat Rd Flora Hill',
      'href'=>'https://www.athleticsbendigo.org.au/the-complex-lubac'
     },
    'setter'=>{
      'text'=>'Athletics Bendigo',
      'href'=>'https://www.athleticsbendigo.org.au/'
     },
  }

  content = [
    YAML.dump(front_matter),
    '---',
    '',
    "{{ page.date |date: '%A %-d %B %Y' }}",
    '',
    "[{{page.location.text}}]({{page.location.href}})",
    '',
    "Distance: #{distance}",
    '',
    %(
STANDARD TUESDAY
COMPETITION DAILY FEES

Athletics Victoria Registered
(Track Package) - All ages $2

Non-Athletics Victoria Registered
$5, U18 $3

Competition Manager:
Peter Barrett 0413 635 387

Sponsor [Adam Parker Electrical](https://www.facebook.com/ALparkerelec/)
      )

  ].join("\n")

  File.write(fn,content)
}
