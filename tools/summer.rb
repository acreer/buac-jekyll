require 'pp'
require 'csv'
require 'time'
require 'yaml'

now = Time.now

CSV.foreach(
  './SummerTrack-19-20.csv',
  converters: :all,
  header_converters: :symbol,
  headers: true
) do |r|
  pp r

  distance = case r[:day]
          when 'Saturday','Sunday'
            sh = "Shield #{r[:shield]}" if r[:shield]
            nosh = "Non Shield #{r[:non_shield]}" if r[:non_shield]
            "#{sh} #{r[:type]} #{nosh}"
          else
            r[:type]
          end
  title = case r[:day]
          when 'Tuesday'
            "#{r[:day]} Parker Electrical #{distance}"
          when 'Thursday'
            "#{r[:day]} Flack Advisory #{distance}"
          when 'Saturday','Sunday'
            "#{r[:day]} Athletics"
          end
  day = r[:date].split('/').reverse.join('/')
  date = Time.parse(
    case r[:day]
    when 'Tuesday'
      "#{day} 19:00:00"
    when 'Thursday'
      "#{day} 19:00:00"
    when 'Saturday'
      time='14:00:00'
      time='10:00:00' if r[:type]=~/Nitro/i
      "#{day} 14:00:00"
    else
      day
    end
  )

  location = {
    'text' => 'The Track, Retreat Rd Flora Hill',
    'href' => 'https://www.athleticsbendigo.org.au/the-complex-lubac'
  }
  if r[:day] == 'Thursday'
    location = {
      'text' => 'Tom Flood Sports Centre',
      'href' => 'https://www.bendigo.vic.gov.au/Things-To-Do/sports-venues/tom-flood-sports-centre'
    }
  end

  front_matter = {
    'title' => title,
    'date' => date,
    'isCurrent' => date > now,
    'type' => 'ab-run',
    'location' => location,
    'distances'=>distance,
    'setter' => {
      'text' => 'Athletics Bendigo',
      'href' => 'https://www.athleticsbendigo.org.au/'
    }
  }
  content = [
    YAML.dump(front_matter),
    '---',
    "{{ page.date |date: '%A %-d %B %Y' }}",
    '',
    "Distance: #{distance}",
    '',
    '[{{page.location.text}}]({{page.location.href}})',
    '',
    ''
  ].join("\n")
  pp front_matter
  a=''
  a= '-a' if r[:type]=~/nitro/i 
  fn = "_runs/summer-#{date.strftime('%F')}#{a}.md"
  File.write(fn, content)
end
