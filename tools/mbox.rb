require 'pp'

# From the parse mbox in https://ruby-doc.org/core-2.2.6/Enumerable.html

def parse_mbox(mbox_fn)
    messages = [];

    open(mbox_fn) { |f|
      f.slice_before { |line|
        # puts "YO #{line}"
        line.start_with? "From "
      }.each { |mail|
        unix_from = mail.shift
        puts "mail #{mail.pretty_inspect}"
        i = mail.index("\n")
        puts "Found i #{i}"
        header = mail[0...i]
        body = mail[(i+1)..-1]
        body.pop if body.last == "\n"
        fields = header.slice_before { |line| !" \t".include?(line[0]) }.to_a
        p unix_from
        pp fields
        pp body
      }
    }
end

if __FILE__ == $0
    my_inbox='/Users/andrewcreer/Library/Thunderbird/Profiles/cvxclv7g.default/ImapMail/mail.messagingengine.com/INBOX'
    parse_mbox(my_inbox)
end
