---
title: Club and Speed Champion Scoring System
layout: single
---

The following information explains how the points are allocated in the two main championship events held each year.

### Club Champions

The Club Champion award is designed to reward dedication and steady improvement throughout the year. Because of the handicapping system, the Club Champion is not necessarily the fastest runner in the club, or the one who runs the most kilometres in a week, or even the one that gets the most ribbons throughout the year, it normally goes to the person who trains steadily, attends the majority of club runs and achieves consistent top twenty placings. Here’s how it works:

| |Long|Medium|Short|
|---|---|---|
|Place|Points|Points|Points|
|1st|60|40|40|
|2nd|55|35|35|
|3rd|50|30|30|
|4th+|50 minus your position|30 minus your position|30 minus your position|

Your first run for the season this year is counted in the aggregates if you are
a returning member with a handicap. If it is your first race at a new distance,
your are ineligible for points until the following race when a handicap will be
established.

Only the best 10 of all eligible races are counted. So if you have a shocker or
two, or can’t make it to every race, you have a chance to redeem yourself. Only
runners that have competed more than once will appear on the leader board.

### Speed Champions

The Speed Champion aggregate recognises the fastest runners at the club over
the year. Although the club’s purpose is not necessarily to train runners to an
elite level, it’s good to see who is running fast regardless of the
handicapper’s time penalties. Here’s how the aggregate system works:

| |Long|Medium|Short|
|---|---|---|
|Place|Points|Points|Points|
|1st|20|20|20|
|2nd|19|19|19|
|3rd|18etc.|18etc.|18etc.|
|21st+|0|0|0|

The Speed Aggregate is based on points and it’s your best 7 races as far as
points that count, not necessarily your seven fastest times.
