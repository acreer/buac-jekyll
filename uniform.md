---
title: BUAC club uniform
sidebar:
  nav: admin
---

## Official Club Merchandise

### Run Singlets

Available in Mens, Womens regular (wide back) and Womens action back (narrow
back). Kids sizes are coming!

These custom designed official club singlets are made in Australia from
lightweight, moisture wicking polyester. The fully sublimated design looks
great and is AV approved.

![singlet-front]({{site.baseurl}}/assets/files/Uniforms/MensSingletfront.jpg)
![singlet-back]({{site.baseurl}}/assets/files/Uniforms/MensSingletback.jpg)


### Run T-Shirts

Available in Mens and Womens styles. Kids sizes are now here! Available in both short and long sleeves....we will add some photos of the long sleeves soon.

![shirt-front]({{site.baseurl}}/assets/files/Uniforms/MensT-Shirtback.jpg)
![shirt-back]({{site.baseurl}}/assets/files/Uniforms/MensT-Shirtfront.jpg)


### Crop Tops

If you want to show off your flat stomach, maximise your ventilation or look
like a 70's roller skater this option is coming soon!

![crop-front]({{site.baseurl}}/assets/files/Uniforms/Crop-front.jpg)
![crop-back]({{site.baseurl}}/assets/files/Uniforms/Crop-back.jpg)


### Hoodies

Keep warm on those cold Saturdays before your run or wear it proud to your next
interclub or long distance event. The Bendigo Uni hoody is stylish and
remarkably good value for money.

Made from 370gsm 80% cotton 20% poly, this super heavy fleece features a
kangaroo pocket with rib banding and a fully lined hood with 'natural' cord.
There are a limited number of hoodies available at club runs for cash purchase. 
If we don't have your size, simply add your details to the order form and
we'll let you know when your top arrives. 

![hoodie-front]({{site.baseurl}}/assets/files/Uniforms/Club-Hoody.jpg)


### Garment Sizes

|Uniform Item|Sizes|
|---|---|
|Technical Run Singlet|Mens: XS, S, M, L, XL|
|Technical Run Singlet|Womens: 6, 8, 10, 12, 14|
|Technical Run Singlet|Kids: 6, 8, 10, 12, 14|
|Technical Run T-Shirt|Mens: XS, S, M, L, XL|
|Technical Run T-Shirt|Womens: 6, 8, 10, 12, 14|
|Technical Run T-Shirt|Kids: 6, 8, 10, 12, 14|
|Technical Run Long Sleeve T-Shirt|Unisex: XS, S, M, L, XL|
|Technical Run Crop Top|Womens: 8, 10, 12|
|Casual Fleece Hoodie|Mens: XS, S, M, L, XL|
|Casual Fleece Hoodie|Womens/Kids: 6, 8, 10, 12, 14|


### Prices

|Uniform Item|Price|
|---|---|
|Technical Run Singlet|$30|
|Technical Run T-Shirt|$35|
|Technical Run Long Sleeve T-Shirt|$50|
|Technical Run Crop Top|$45|
|Casual Fleece Hoodie|$50|


### Ordering Details

Run tops are available at club runs for cash purchase. If we don't have your
size, complete an order form and we'll let you know when your top arrives. 

#### Banking Details:

Account Name: 	BGO University Athletics Club Inc.

BSB Number: 633-000

Account No: 104183603

