---
title: Winter Season Results History
layout: single
---

Online winter season results history over the years. There are
[handicap history]({%link  handicapsHistory.md %}) and
[aggregate results]({%link aggregates.md %}) available also

{% assign byYear = site.raceData | where: "buacType","results" | group_by: "buacYear" %}
{% assign byYearReverse = byYear | reverse %}

<nav class="nav-across">
<ul>
<li><small>Jump to year:</small></li>
{% for yr in byYearReverse  %}
<li><a href="#{{yr.name}}"><small>{{yr.name}}</small></a></li>
{% endfor %}
</ul>
</nav>

{% for yrGrp in byYear reversed %}

<table id="{{yrGrp.name}}">
 <thead>
  <tr>
   <th style="width:20%">{{ yrGrp.name }}</th>
   <th style="width:50%">Race</th>
   <th>Results</th>
  </tr>
 </thead>
 <tbody>
  {% assign byRaceNum = yrGrp.items |  group_by: "date" %}
  {% for raceNumGrp in byRaceNum %}
   <tr>
    <td>
     {{ raceNumGrp.items.first.date | date: '%B %-d'}}
    </td>
    <td>
      {% assign race = raceNumGrp.items.first %}
      {% if race.runMD %}
        <a id="{{yrGrp.name}}-{{race.runMD}}" href="{{site.baseurl}}/runs/{{ race.runMD | replace: ".md",".html" }}">
          {{ race.title }}
        </a>
      {% else %}
        {{ race.title }} 
      {% endif %}
      <!--{{ race.runMD }} -->
    </td>
    <td>
     {% assign sortedPosts = raceNumGrp.items | sort: "buacLength"  %}
     {% for post in sortedPosts %}
      <a href="{{site.baseurl}}{{post.url}}">{{ post.buacLength | capitalize }}</a>
     {% endfor %}
    </td>
   </tr>
  {% endfor %}
 </tbody>
</table>
{% endfor %}

<nav class="nav-across">
 <ul>
   <li><small>Jump to year:</small></li>
   {% for yr in byYearReverse  %}
    <li><a href="#{{yr.name}}"><small>{{yr.name}}</small></a></li>
   {% endfor %}
 </ul>
</nav>

<!--End -->
