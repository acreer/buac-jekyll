---
title: Sign up for the BUAC email list
---

The Official BUAC Mailing list is generally used once a week to send updates
and news to club members and intersted others.

If you do not currently recieve the weekly updates, you can remedy this
situation by filling in thisthis form, with your email address and name. 

Please note that existing members and those that sign up using the
[online membership system]({{site.membershipurl}}) do not need to use this
form.

You can also unsubscribe by clicking the UNSUBSCRIBE link in any email you
receive.

<div id="mc_embed_signup">
  <form action="https://bendigouniathsclub.us17.list-manage.com/subscribe/post?u=30247a265d32468a4848fb3f2&amp;id=fe4dfc9a4e"
  method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form"
  class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
      <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
      <div class="mc-field-group">
        <label for="mce-EMAIL">Email Address <span class="asterisk">*</span>

        </label>
        <input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
      </div>
      <div class="mc-field-group">
        <label for="mce-FNAME">First Name <span class="asterisk">*</span>

        </label>
        <input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
      </div>
      <div class="mc-field-group">
        <label for="mce-LNAME">Last Name <span class="asterisk">*</span>

        </label>
        <input type="text" value="" name="LNAME" class="required" id="mce-LNAME">
      </div>
      <div id="mce-responses" class="clear">
        <div class="response" id="mce-error-response" style="display:none"></div>
        <div class="response" id="mce-success-response" style="display:none"></div>
      </div>
      <!-- real people should not fill this in and expect good things - do not
      remove this or risk form bot signups-->
      <div style="position: absolute; left: -5000px;" aria-hidden="true">
        <input type="text" name="b_30247a265d32468a4848fb3f2_fe4dfc9a4e" tabindex="-1"
        value="">
      </div>
      <div class="clear">
        <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe"
        class="button">
      </div>
    </div>
  </form>
</div>

<!--End mc_embed_signup-->
