---
title: ReadMe
---

A repository for the 
[Bendigo University Athletics Club website](http://bendigouniathsclub.org.au/)

# Overview

This repository is a system for building a static website from a bunch of data
files. A static website being one where no processing is done on the server.
It can be compared with a typical blog like wordpress, where every time someone
loads a page, the navigation and menus are rebuilt from some sort of database.

We found that the wordpress blog worked well for a while, but after a few years
of results and handicaps posts and some stuff on each page to link to other
pages (long results need to find the short and medium results for the same
run), that the speed of page loads had dropped below some level where it was
becoming noticeable. Also the speed response of the admin pages in the
wordpress blog were painfully slow. 

Although these problems could probably have been solved with a higher level of
web hosting, they were not. So here we are.

The central features of the site are to present

* standard welcome/promotion/information type pages about the club.
 
* A stream of current news stories about current events.  These are
  characterised by being important at the time, but less so with the passing of
  time.  These include the weekly update email texts

* an organised view of handicap and race result data for all the club races

# Details

The processing of the site is done with [jekyll](http://jekyllrb.com/) using a
theme based on [minimal
mistakes](https://github.com/mmistakes/minimal-mistakes). The theme is all
modern and cool at the time of writing, but hopefully when someone want to make
a new version of this, the data will be structured enough to make it a fairly
painless transition.

The jekyll system is relatively well known and well supported in tech circles.
Github documentation pages are all generated with jekyll. It is relatively easy
to drive, with good flexibility and extensibility.  And if it is used by
github, then it will be maintained and updated, or they will choose something
else, in which case we can also.

Jekyll works by processing
[Markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
into html, and also processing any [Liquid
Templates](https://shopify.github.io/liquid/) it finds in the markdown.

Jekyll will also process html files for when you really need.

# Race and Handicap Data files.

Our race/handicap data files are CSV files that come from the timing system.
These are checked into this repository in a folder structure like this

```
raceDataCSV/
├── 2009
│   ├── 01_CollegeClassic
│   │   └── results
│   │       ├── long
│   │       ├── medium
│   │       └── short
│   ├── 02_ApolloHillAttack
│   │   └── results
│   │       ├── long
│   │       ├── medium
│   │       └── short
│   ├── 03_CedarDriveDash
│   │   └── results
│   │       ├── long
│   │       ├── medium
│   │       └── short
└── 2017
    ├── 01_MandurangMeander
    ├── 02_LandryLope
    ├── 03_RifleRangeRattle
    ├── 04_SandhurstSlog
    ├── 05_CrusoeCrusade
    ├── 06_HamStreetHustle
    ├── 07_PiepersPlunder
    ├── 08_BuacInvite
    │   └── results
    │       ├── long
    │       │   ├── 03
    │       │   ├── 05
    │       │   └── 07
    │       ├── medium
    │       │   ├── 03
    │       │   └── 05
    │       └── short
    ├── 09_ReservoirRamble
    ├── 10_JunortounJog
    ├── 11_AbCombinedMandurang
    ├── 12_PearcesRoadRally
    ├── 13_KangarooFlatFalter
    ├── 14_TrottingTerraceTrundle
    ├── 15_Notleys
    ├── 16_BuacHalfMarathonFestival
    └── 17_ClubMysteryRun
```

These [csv files](https://en.wikipedia.org/wiki/Comma-separated_values) are
converted into a [jekyll collection](https://jekyllrb.com/docs/collections/) by
a helper script `npm run csv`.  The output is not checked in to the repository.
They appear in `_raceData` folder.

# Wordpress blog news creation source

We have kept the old blog, as a way of allowing more people to have a pain free
way of contributing to the website.  That is to be able to write race reports
and other news articles and have them appear on the website.  Creating content
in Wordpress is pretty well documented. 

There is a helper script that will login to the worpress api and create
markdown files, that are then processed by jekyll `npm run wp` does this.

The md files made by this script are not checked into the repository.
Wordpress is seen as the primary source of truth for the content of the news
articles on the site.

## Feature Images
If you upload media to the wordpress site, and then set a 'featured image' for
a post that you right, the image will appear on the homepage automagically 

# Installation

To use this repository:

## Mac or linux like system

Install [ruby](https://www.ruby-lang.org/en/) and [node](https://nodejs.org/en/) and [git](https://git-scm.com/)
 
```
git clone git@bitbucket.org:acreer/buac-jekyll.git
npm install
gem install bundler
bundle install
```

## Windows

John Robinson uses
* [TortoiseGit](https://tortoisegit.org/)

* [Power Shell](https://en.wikipedia.org/wiki/PowerShell)

* [Ruby Installer for windows](https://rubyinstaller.org/) click the 'add ruby
  to the path' option during the install so that in the power shell you can
  type (paste) `ruby --version` and it just works

* [Sublime Text](https://www.sublimetext.com/3) as a text editor. It has colour
   syntax highlighting and makes life much easier than using notepad

# Continuous Integration

When you are editing/experimenting/making changes to the site, you can have a
local version running on your own machine and you can make a change to a file
and see the result on your own private version of the site without the risk of
breaking everything in a very public way.  Like this [Video](https://www.youtube.com/watch?v=Gxpg6xlxKVg)

On windows, you open 2 power shell windows, and run

1.  Start the local web sever. There is a helper script to start a local server `npm run serve` or `npm run sv`

2.  Process your changes. There are couple of options
  
  - `npm run fb` fb is for full build will process every md file and build the whole site
  
  - `npm run jk` runs jekyll in watch mode. Will see when you save a file and rebuild that page



To process the website `npm run fb` to see a local copy `npm run sv` and follow
the instructions. Usually browse to http://localhost:3000/

# Jekyll

The folder structure is standard jekyll

    .
    ├── assets
    ├── _data
    ├── _drafts
    ├── _includes
    ├── _layouts
    ├── _plugins
    ├── _posts
    ├── _results
    ├── _runs
    ├── _sass
    ├── _site

And some additions stuff

    ├── raceDataCSV
    └── tools




# Tools

There are some tools that process files from Darren into Files that jekyll can use.

## csv2jekyll.rb

```
ruby tools\csv2jekyll.rb
```

Looks inside the `raceDataCSV` folder.

Output into `_raceData` folder for futher prcessing by jekyll

Only folders with a frontMatter.yaml get into the raceData

3 results and 3 handicaps can be made for each race with a frontMatter.yaml file.

Each CSV inside folder ends up as a 'tablesorter' table in the file.


## Complex races like the Invite and Half Marathon

There are some DOCS about INVITES and HALF MARATHON files when you run

``` 
ruby tools/sortRacesWithManyFiles.rb
```

## Priming for the next year

### Once the committee sets the calender.

Edit all the file in `_runs` Set the date: frontmatter and the isCurrent to
rest or re-instate any runs.

Generate the website and check that the Calendar is OK.

###  Generate the raceDataCSV files for the year.

```
ruby tools/runs2frontmatter.rb
```

Will spit out some documentation.


