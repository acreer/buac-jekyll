---
title: Life Members
sidebar:
  nav: admin
---

Life membership is the highest award that Bendigo University Athletics Club
(BUAC) can bestow upon members. Conferring life membership represents the clubs
appreciation of an outstanding contribution to the club over a significant
period of time.

The current list of Life Members is

<b>
<br/>Max MaKay *
<br/>David Keith
<br/>Alan East
<br/>Gary Crouch
<br/>Gavin Fiedler
</b>

<p>* deceased </p>

The club would be much less without the contributions of these people.

# Nomination details

See the [Life Membership Guidelines]({% link assets/policy/BUACLifeMemberGuidelines.pdf %}) for details
