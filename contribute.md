---
title: Contributions
sidebar:
  nav: admin
---

Our club runs on the efforts of many volunteers

There are many ways for YOU to help out.

## Write a something for the web site

We celebrate just as hard for 29:07 in a 3km, a parkrun, or the Zatopek
Australian 10k Championship. 

Mail a race report or an interesting story to
[race.report@bendigouniathsclub.org.au](mailto:race.report@bendigouniathsclub.org.au)
and we will add to the website.

## Suggest a location for a Club Race

Got ideas for somewhere to hold a club run? Let someone on 
[the committee]({% link about.md %}#contacts)
 know.

## Site Source

This site is kept in a [public repository](https://bitbucket.org/acreer/buac-jekyll/src/master/) you can build your own copy and contact the [maintainers](mailto:blog@bendigouniathsclub.org.au) with suggestions or a pull request.
