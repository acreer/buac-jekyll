---
title: Introducing the BUAC Spring Series - Round 2
sidebar:
  nav: admin
---

We invite YOU to participate in the second round of our COVID Series: test
yourself on some of our classic club run courses until Sunday 13 December 2020.
Upload your results online using Strava, .gpx files or screenshots.

Similar to many ‘virtual runs’, participants are invited to run any course at
any time during the period and submit your results online.


# The Series has been designed to:

* enable COVID-Safe participation,
* create a sense of direct comparison with a runners’ own past performances on
  our traditional cross-country courses,
* offer a wide variety of distances for all ages and abilities, and
* take in a range of safe and interesting running locations from across Bendigo.

# COVID Spring Series - Round 2

* [ College Classic, Kennington ]({% link _runs/college-classic.md %}) 1km, 2km, and 6km
* [ Sandhurst Slog, Kangaroo Flat ]({% link _runs/sandhurst-slog.md %}) 1km, 3km, and 7km
* [ Pearce's Road Rally, Mandurang ]({% link _runs/pearces-road-rally.md %}) 1km, 3.6km, 7.3km 
* [ Kangaroo Flat Falter, Kangaroo Flat ]({% link _runs/kangaroo-flat-falter.md %}) 1km, 3km, and 8.2km
* [ Hornet's Hideaway, Crusoe ]({% link _runs/hornets-hideaway.md %}) 1km, 4.1km, 9.5km
* BONUS ROUND: [ Pioneer Heartbreak ]({% link _runs/guys-hill-heartbreak.md %}) NEW COURSE: Guys Hill Rd to Edwards Rd Return. 1km, 3km, 7.7km 


# COVID Spring Series – Round 1

* [ Notley’s Noodle (Virtual) ]({% link _runs/notleys-noodle.md %}) 1km, 4.8km, 11km 
* [Rifle Range Rattle (Virtual)]({% link _runs/rifle-range-rattle.md %}) 1km, 3.4km, and 7.9km 
* [Mandurang Meander (Virtual)]({% link _runs/mandurang-meander.md %}) 1km, 3km, and 6.5km 
* [Rocky Rises  (Virtual)]({% link _runs/rocky-rises.md %}) 1km 4km and 10km 
* [Crusoe Crusade (Virtual)]({% link _runs/crusoe-crusade.md %}) 1km, 3km, and 8km


# COVID-SAFE AND GENERAL SAFETY MEASURES

* Participants enter at their own risk.
* Participants agree to run as safely as possible and strictly adhere to all Government regulations, including current COVID-19 Government restrictions and traffic rules.
* Participants are discouraged from lingering at the start line for longer than necessary before or after their runs.
* No pre-booking of run times is required. However, if a participant happens to arrive at the same start line at the same time as another person, they will need to wait for them to start or leave the area and re-schedule their run for another time.
* Club members are encouraged to post a short message to the Bendigo University Athletics Club Facebook group to let others know when they are intending to run a certain course – to help avoid congestion.
* If any more participants are encountered ‘on course’ than the COVID guidelines, then an adequate social distancing will be expected when passing.
* Cross country courses typically allow for a wide berth for overtaking, if at all required.

# THE COURSES

* Course maps will be available via the club website and PlotARoute.
* Following the correct course will be the responsibility of the participant.
* Courses will not have any signage or have any course markings.
* Photos will be posted on the club website of the Start and Finish location of
  each course.
* A written description of the courses will be posted on the club website to
  assist with navigation.
* Participants may request a ‘run buddy’ who knows the course to assist with
  course navigation.
* Participants are strongly encouraged to take a mobile phone with them on
  course if they are unfamiliar with the course/location/route.
* All courses are on unsealed bush tracks, so it is unlikely that participants
  will encounter cars. However, if they do, they will be expected to proceed
  with caution and obey traffic rules.

 

# RECORDING AND SUBMITTING TIMES:

* Participants must have a GPS watch, smartphone or app to track their run or
  walk.
* Although designed for club members, non-members will be invited to
  participate to encourage healthy physical activity.
* Participants can run the courses as many times as they want.
* Times submitted should be elapsed time (i.e. participants should not pause
  their timing device mid-run or walk.)
* All entries must be completed on foot, so no bikes or scooters, etc are
  permitted.
* Entrants will need to upload their time via a link sent to them via email.
  They will need to provide publicly accessible ‘proof’ (Strava or similar
platform).
* Participants’ ‘proof’ will be reviewed for their course accuracy and for run
  times to be verified by the club committee members.
* If a navigation error has occurred, the participant will be advised and they
  can try again.
* A handicap may be applied to each of the runs after the series is completed,
  depending on overall participation levels.
* The honesty policy will apply to all entries.

