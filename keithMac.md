---
title: Keith MacDonald Championship Club
sidebar:
  nav: admin
---

This initiative pits Bendigo Athletic Clubs against one another in each of the
clubs' Invitation races together with the 
[Keith Huddle Memorial](https://www.athleticsbendigo.org.au/keith-huddle-memorial)
and the 
[Glen Allen Memorial](https://www.athleticsbendigo.org.au/glen-allen).

The first 25 placegetters in the Open and Intermediate categories for both
genders earn points. First home gets 25 points, second - 24, third - 2 etc. 

The points are tallied at each Invitation and totalled at the end of the year
to establish the Champion Club in Bendigo. 

The winning club wins a trophy ![trophy](https://static.wixstatic.com/media/2daf1f_829a6640cee94e8ab04d3b7915dbc30a~mv2.jpg/v1/fill/w_843,h_562,al_c,q_90,usm_0.66_1.00_0.01/2daf1f_829a6640cee94e8ab04d3b7915dbc30a~mv2.jpg)

Latest results are posted on the 
[Athletics Bendigo Page](https://www.athleticsbendigo.org.au/keithmacdonaldcup)

# Previous Winners

| Year | 1<sup>st</sup> Place | 2<sup>nd</sup> Place | 3<sup>rd</sup> Place | 4<sup>th</sup> Place |
|:----:|:---:|:---:|:---:|:---:|
| 2017 | Bendigo University |  Bendigo Harriers | South Bendigo | EagleHawk |
| 2016 [Results](https://docs.wixstatic.com/ugd/2daf1f_a7d9cd6724f440f3b37f7486580b714f.pdf) | Bendigo University (1969&nbsp;points) | Bendigo Harriers (1913&nbsp;points) | South Bendigo (759&nbsp;points) | EagleHawk (294&nbsp;points) |
| 2015 | Bendigo University | Bendigo Harriers | South Bendigo | EagleHawk |

&nbsp;
