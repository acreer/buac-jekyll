---
title: Club Policy
sidebar:
  nav: admin
---

As an Athletics Victoria affiliated club BUAC has a number of policies and
operating guidelines. 


<ul>
{% for policy in site.data.policies %}
  <li><a href="{{site.baseurl}}/{{policy.href}}">{{policy.title}}</a></li>
{% endfor %}
</ul>  

If you have
suggestions or comments please [contact]({% link about.md %}) a commitee
member.


&nbsp;
